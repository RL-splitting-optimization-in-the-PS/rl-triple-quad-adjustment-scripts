from RL_splitting_adjustment_scripts.splitting_agent_base import SplittingAgentBase
from argparse import ArgumentParser
from longitudinal_tomography.utils import tomo_input as tomoin
import pyjapc
import time
import sys
import numpy as np

"""
Instructions to run automatic optimisation tri/quad functions:

1. Open a tomoscope on your beam.
2. Open the splitting reference of your interest (triple or quad, 72bs/48b_bcms)
3. Make sure the acquisition is centered on the splitting of one bunch.
4. Check which tomoscope is being used (57/58) and use the correct name in the SplittingAgentBase init.
5. Make sure the correct settings are given to SplittingAgentBase:
    user, splitting, tomoscope, and beam type (72b_25ns or 48b_BCMS).
6. Use the approproate function for you:
    splitting_class.start_automatic_optimisation_tri
    splitting_class.start_automatic_optimisation_quad

    with your settings of choice.
"""


if __name__ == '__main__':

    # Parsing arguments
    parser = ArgumentParser()
    parser.add_argument('input', default='stdin', type=str,
                        help='Path to input .dat file, or use "stdin" for '
                             'taking input from stdin.')
    args = parser.parse_args()

    # Reading data and building base tomo objects (based on longitudinal_tomography.utils.tomo_run.run function)
    raw_params, raw_data = tomoin.get_user_input(args.input)
    # print(raw_params)
    # print(type(raw_data))
    raw_data = np.reshape(raw_data, (150,400))
    # print(np.shape(raw_data))
    # print(raw_data)
    
    user_beam_param = raw_params[0]
    print(f'Detected user / beam: {user_beam_param}')

    split = user_beam_param.split('=')
    if 'BCMS' in user_beam_param:
        print('Detected beam type bcms beam.')
        beam_type = '48b_BCMS'
    else:
        print('Detected beam type 72b beam.')
        beam_type = '72b_25ns'

    user_name_from_tomo = split[0]


    ########## General settings ##########

    user = 'CPS.USER.' + user_name_from_tomo
    japc = pyjapc.PyJapc(selector=user)

    # Unclear which tomoscope is being used and not included in raw params, check 57 and 58 and see which matches:
    tomoscope_57 = 'PR.SCOPE57.CH01/Acquisition'
    tomoscope_58 = 'PR.SCOPE58.CH01/Acquisition'

    acq_57 = japc.getParam(tomoscope_57)
    acq_58 = japc.getParam(tomoscope_58)

    print(acq_58['value'])
    print(type(acq_58['value']))
    print(np.shape(acq_58['value']))
    # Check which tomoscope has the same values as the raw_data from the std input
    # Check correct shape (number of datapoints)
    tomo_57_shape = False
    tomo_58_shape = False

    if len(acq_57['value'].flatten()) == len(raw_data.flatten()):
        print('57 has right shape...')
        tomo_57_shape = True
    if len(acq_58['value'].flatten()) == len(raw_data.flatten()):
        print('58 has right shape...')
        tomo_58_shape = True
    
    if tomo_57_shape and tomo_58_shape:
        print('Both tomo acquisition of right size, unsure which tomo is being used, exiting...')
        sys.exit()
    elif tomo_57_shape:
        active_tomoscope = tomoscope_57
        print('Only 57 has correct shape, assuming this is the \
active tomoscope, subscribing...')

    elif tomo_58_shape:
        active_tomoscope = tomoscope_58
        print('Only 58 has correct shape, assuming this is the \
active tomoscope, subscribing...')

    elif not tomo_57_shape and not tomo_58_shape:
        print('Neither tomo acquisition of right size, exiting...')
        sys.exit()

    # With correct user, tomo, and beam type one has enough information to run
    # the automatic optimisation script.
    print(f'User: {user}')
    print(f'Active tomo: {active_tomoscope}')
    print(f'Beam type {beam_type}')
    ###############################################
    
    ########## Triple split optimisation ##########


    splitting_class = SplittingAgentBase(user=user, 
                                        splitting='tri', 
                                        tomoscope=active_tomoscope, 
                                        beam_type=beam_type)

    splitting_class.start_automatic_optimisation_tri(initial_feat_guess=True, 
                                        max_steps=30, 
                                        render_opt=True,
                                        phase_criterion=0.0008,
                                        volt_criterion = 0.0005,
                                        )
  