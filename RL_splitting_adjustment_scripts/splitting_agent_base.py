from distutils.log import error
import json
import pyjapc
import matplotlib.pyplot as plt
import numpy as np
import sys
import torch
import torch.nn as nn
import torch.nn.functional as F
#from pytorch_fitmodule import FitModule
from torch.autograd import Variable
import numpy as np
from .neural_networks.dataloader import QuadsplitDataset, ToTensor, Normalize
from torchvision import transforms, utils
from torch.utils.data import Dataset, DataLoader
from .neural_networks.CNN import CNN
from .neural_networks.CNN_tri import CNN_tri
from .neural_networks.CNN_tri_new import CNN_tri_new
import torch.optim as optim
import matplotlib.pyplot as plt
from .vpc_functions import load_model, isolate_bunches_from_dm_profile_tri, \
                        loss_function_tri, process_tomoscope_acquisition, render, render_quad, \
                        tri_phase_loss, isolate_bunches_from_dm_profile, loss_function_two#, correct_dm_tri #, FWHM
from .vpc_ajdust_phases import offset_GSRPB, \
                                offset_GSRPC, \
                                update_voltage_program_GSVMOD7, \
                                offset_GSRPB_BCMS, \
                                offset_GSRPC_BCMS, \
                                update_voltage_program_GSVMOD7_BCMS, \
                                offset_C20, \
                                offset_C40
import time
import datetime
from stable_baselines3 import SAC
import os
from pathlib import Path
import pickle

### MLP imports ###
import mlp_rf_splittings
from mlp_model_api import INPUTS, OUTPUTS
from mlp_client import Client, Profile, AUTO

parent_path = str(Path(__file__).parent.parent) # Save parent dir of this script to allow loading of CNN/RL models.


"""
Class with functions to get new data, take actions etc. To run in conjunction with script/main function.
"""




class SplittingAgentBase():


    def __init__(self, user='CPS.USER.MD1', 
                splitting='tri', 
                tomoscope='PR.SCOPE57.CH01/Acquisition', 
                beam_type='72b_25ns'):

        #print(japc.getUsers('CPS'))

        ### Attributes for saving visited positions, profiles, etc.
        self.step=0
        self.phase_set_memory = []
        self.action_memory = []
        self.phase_set_memory = [] # Doubles as labels for dm:s in dm_memory. May need to convert them to resemble labels in original dataset.
        self.dm_memory = []
        self.profile_criterion_memory = []
        self.profile_memory = []
        self.fwhms_memory = []
        self.intensities_memory = []
        self.rel_phase_volt_set = [[0,0,0]]

        ### Trying to save things to make future online RL possible.
        self.phase_obs_memory = []
        self.phase_agent_action_memory = []
        self.phase_agent_reward_memory = [] # Simply using -loss for now

        self.volt_obs_memory = []
        self.volt_agent_action_memory = []
        self.volt_agent_reward_memory = []

        print('Initializing agents and CNNs...')
        self.tomo = tomoscope
        self.user = user
        self.japc = pyjapc.PyJapc(selector=self.user)
        self.tomo_values = None
        self.profile = None
        self.bunches = None
        self.intensities = None
        self.fwhms = None
        self.splitting = splitting
        if beam_type != '72b_25ns' and beam_type != '48b_BCMS':
            print(f'Unrecognized beam type input {beam_type}. \n \
                    Two accepted types are \'72b_25ns\' or \'48b_BCMS\'. \n \
                    WARNING: Make sure you run the correct beam setting for your beam or the agent will change the wrong settings!')
            sys.exit('Input error: beam_type input unrecognized.')
        self.beam_type = beam_type


        ### Load feat extr.
        client = Client(Profile.PRO)
        self.feature_extractor = client.create_model(
            model_class = mlp_rf_splittings.FeatExtrModelTri,
            params_name = "FeatExtrModelTri_default",
            params_version = AUTO # latest compatible version will be taken
            )
        #self.model = CNN_tri_new(trim_edges=True)
        #feat_model_name = 'cnn_tri_Trim_Move_Injection_PV_59521_ref_voltageJul_11_2022_09-22-53_e43' # Trim edges version
        #self.model_quad = CNN() 
        #feat_model_name_quad = 'cnn_quad_ms_ADD_NOISE_Move_injectione_46_FINETUNE_LIVEMay_05_2022_15-41-33_e11'# 

        # print(f'Fetching model {feat_model_name}...')
        # path = parent_path + r"/CNN_models/{}.pth".format(feat_model_name)
        # #path = r"C:\Users\jwulff\cernbox\workspaces\RL-007-RFinPS\optimising-rf-manipulations-in-the-ps-through-rl\saved_models\{}.pth".format(model_name)
        # self.model = load_model(path,self.model)
        # #optimizer.load_state_dict(checkpoint['optimizer_state_dict'])
        # print(f'Weights loaded...')
        # self.model.eval() ### IMPORTANT! If not in eval mode, batchnorm layers will not work correctly.
        # print(f'Fetching model {feat_model_name_quad}...')
        # path = parent_path + r"/CNN_models/{}.pth".format(feat_model_name_quad)
        # self.model_quad = load_model(path,self.model_quad)
        # print(f'Weights loaded...')
        # self.model_quad.eval() ### IMPORTANT! If not in eval mode, batchnorm layers will not work correctly.

        ### Load RL agents

        # # Tri
        #client = Client(Profile.PRO)
        self.agent_volt_tri = client.create_model(
            model_class = mlp_rf_splittings.SacVoltTriModel,    
            params_name = "SacVoltTriModel_default",
            params_version = AUTO # latest compatible version will be taken
            )
        self.phase_agent_tri = client.create_model(
            model_class = mlp_rf_splittings.SacPhaseTriModel,    
            params_name = "SacPhaseTriModel_default",
            params_version = AUTO # latest compatible version will be taken
            )

        
        # model_name_phase = r"SAC-RelBLIntPhase-phase-loss-profile-step10-59521-ref-NoiseMoveInjPV-diff-00009"
        # self.agent_tri = SAC.load(parent_path + "/RL_models/tri/{}".format(model_name_phase))

        # volt_model_name = r"SAC-RelBLIntVolt-voltloss-step01-profile-59521-ref-NoiseMoveInjPV-diff-00010"
        # self.agent_volt_tri = SAC.load(parent_path + "/RL_models/tri/{}".format(volt_model_name))

        ref_voltage_h14 = np.load(parent_path + '/v14_settings_max_step_01.npy') # Load v14 max step. Calculated as 0.1*reference setting of the six points to change in v14 program.
        self.max_step_volt = 0.1 # Hardcoded through training of agent.
        self.voltage_max_step = ref_voltage_h14
        self.max_step_size=10 # Hardcoded through training of agent.


        # Load agents quad

        # self.agent42 = client.create_model(
        #     model_class = mlp_rf_splittings.SacP42QuadModel(),
        #     params_name = "SacP42QuadModel_default",
        #     params_version = AUTO # latest compatible version will be taken
        #     )

        # self.agent84 = client.create_model(
        #     model_class = mlp_rf_splittings.SacP84QuadModel(),
        #     params_name = "SacP84QuadModel_default",
        #     params_version = AUTO # latest compatible version will be taken
        #     )

        # model_name_42 = r"SAC-Simple-Profile-relBLInt42-step20-diff-00003"
        # self.agent_42 = SAC.load(parent_path + "/RL_models/quad/{}".format(model_name_42))
        # model_name_84 = r"SAC-Simple-Profile-dist8-relBLInt84-step20-diff-00003"
        # self.agent_84 = SAC.load(parent_path + "/RL_models/quad/{}".format(model_name_84))

        self.max_step_size_quad=20 # Hardcoded through training of agent. Already accounted for in MLP agents.

        

        self.japc.subscribeParam(self.tomo, self.checkIfNewDataReceivedCallback)
        self.japc.startSubscriptions()

    def checkIfNewDataReceivedCallback(self, parameterName, newValue):
            self.new_values_received=True
            print(f"New value for {parameterName}.")

    def wait_for_acquisition(self):
        print('Waiting for new acquisition...')
        self.new_values_received=False
        while not self.new_values_received:
            time.sleep(1)
        return
    
    def use_feature_extractor_non_mlp(self):
        sample_trim = {'image': self.tomo_values, 'labels': np.ndarray([0])}
        transform = transforms.Compose([
            #Normalize(),
            ToTensor(),
            ])
        inputs_dict_trim = transform(sample_trim)
        image_trim = inputs_dict_trim['image'].unsqueeze(dim=0)
        outputs = self.model(image_trim).detach().numpy()/100
        outputs[0][2] = outputs[0][2]/10
        print(f'Feat extractor predicted phase offsets: {outputs[0][:2]}')
        outputs[0][2] = 0
        return outputs[0]

    def get_and_process_acquisition(self, mode='tri'):
        mode = self.splitting

        data_dictionary = self.japc.getParam(self.tomo)
        values = data_dictionary['value'] # Matrix if using tomoscope
        self.unaltered_tomo_values = values
        if mode=='tri':
            try:
                assert np.shape(values) == (150,400)
            except AssertionError as e:
                print(f"Unexpected tomoscope acq. shape {np.shape(values)}. Exiting...")
                return None
            values_centered_norm = process_tomoscope_acquisition(values, mode='tri', remove_tails=True, correct_dm=True)
            values_centered_norm_trimmed = values_centered_norm[:,40:360]
            self.tomo_values = values_centered_norm_trimmed
            profile = values_centered_norm_trimmed[-1,:]
            self.profile = profile.copy()
            #corr = correct_profile_tri(profile)
            #################
            # CALCULATE LOSS
            #################
            bunches=[]
            bunches, fwhms, intensities = isolate_bunches_from_dm_profile_tri(profile, intensities=True, rel=True)
            self.bunches = bunches
            # Norm intensities: Try only relative intensities/fwhms
            intensities = intensities / np.max(intensities) # Same as in sim. #TODO
            intensities = intensities - np.mean(intensities)

            fwhms = fwhms - np.mean(fwhms)
            self.fwhms = fwhms
            self.intensities=intensities


            b1 = bunches[0]
            b2 = bunches[1]
            b3 = bunches[2]
            #loss = loss_function_tri(b1,b2,b3)
            phase_loss = tri_phase_loss(b1, b3)
            volt_loss = loss_function_tri(b1,b2,b3)
            #self.loss_memory.append(loss)
            phase_obs = [fwhms[0], fwhms[2], intensities[0], intensities[2]]
            volt_obs = np.append(fwhms,intensities)
            return phase_obs, phase_loss, volt_obs, volt_loss
        elif mode=='quad':
            try:
                assert np.shape(values) == (150,200)
            except AssertionError as e:
                print(f"Unexpected tomoscope acq. shape {np.shape(values)}. Exiting...")
                return None
            values_centered_norm = process_tomoscope_acquisition(values, mode='quad')
            profile_84 = values_centered_norm[-2,:]
            profile_42 = values_centered_norm[99,:]
            self.tomo_values = values_centered_norm
            self.profile_84 = profile_84.copy()
            self.profile_42 = profile_42.copy()
            #################
            # CALCULATE LOSS
            #################
            bunches, fwhms, intensities = isolate_bunches_from_dm_profile(profile_84, intensities=True, bunch_width=8, rel=True, plot_found_bunches=False, distance=20)

            self.bunches = bunches

            ### Process for 84 agent
            # Four bunches found, take average of 1,3 and 2,4 respectively
            bunches_84 = [(bunches[0] + bunches[2])/2, (bunches[1]+bunches[3])/2]
            fwhms_84 = [(fwhms[0] + fwhms[2])/2, (fwhms[1]+fwhms[3])/2]
            intensities_84 = [(intensities[0] + intensities[2])/2, (intensities[1]+intensities[3])/2]
            fwhms_84 = fwhms_84 -np.mean(fwhms_84)
            intensities_84 = intensities_84 / max(intensities_84) #
            intensities_84 = intensities_84 - np.mean(intensities_84)
            bls_and_intensities_84 = np.append(fwhms_84, intensities_84)

            intensities = intensities / np.max(intensities) # Same as in sim. #TODO
            intensities = intensities - np.mean(intensities)

            fwhms = fwhms - np.mean(fwhms)
            self.fwhms = fwhms
            self.intensities=intensities

            ### Process for 42 agent
            bunches_42, fwhms_42, intensities_42 = isolate_bunches_from_dm_profile(profile_42, intensities=True, rel=True, plot_found_bunches=False)
            fwhms_42 = fwhms_42 -np.mean(fwhms_42)
            # How to normalize intensities to be in range [0,1]? For now, divide by constant.
            intensities_42 = intensities_42 / max(intensities_42) # 
            intensities_42 = intensities_42 - np.mean(intensities_42)

            bls_and_intensities_42 = np.append(fwhms_42, intensities_42)

            b1 = bunches_84[0]
            b2 = bunches_84[1]

            phase_84_loss = loss_function_two(b1, b2) #
            phase_42_loss = loss_function_two(bunches_42[0], bunches_42[1])

            phase_42_obs = bls_and_intensities_42
            phase_84_obs = bls_and_intensities_84
            return phase_42_obs, phase_42_loss, phase_84_obs, phase_84_loss
        else:
            print('Unknown \'mode\' parameter when trying to get and process acquisition: {mode}. \n \
            Please insert either mode = \'tri\' or \'quad\'.')
            return None

    def start_automatic_optimisation_tri(self, 
                                        initial_feat_guess=True, 
                                        max_steps=30, 
                                        render_opt=False,
                                        phase_criterion=0.0008,
                                        volt_criterion = 0.0005,
                                        save_dir = None,
                                        block = True):

        """ 
        Main function to run to run a complete episode of triple split optimisation.
        """
        phase_done = False
        volt_done=False

        s1,a1,s2,r1 = None, None, None, None

        phase_step = 0
        volt_step = 0
        tot_steps = 0
        phase_set_memory = []
        action_memory = []
        phase_set_memory = [] # Doubles as labels for dm:s in dm_memory. May need to convert them to resemble labels in original dataset.
        dm_memory = []
        phase_loss_memory = []
        volt_loss_memory = []
        profile_memory = []
        fwhms_memory = []
        intensities_memory = []
        rel_phase_volt_set = [[0,0,0]]

        ### Phase and volt criterion - decides what loss they will accept and terminate with.
        phase_criterion = phase_criterion
        volt_criterion = volt_criterion

        ### Max opt steps allowed - How many steps are the phase/voltage agents allowed to take before terminating run.
        max_total_opt_steps = max_steps

        ### Trying to save things to make future online RL possible.
        phase_obs_memory = []
        phase_agent_tuples = []

        volt_obs_memory = []
        volt_agent_tuples = []
        
        ### Save directory. If not input, will not save anything.
        save_dir=save_dir
        
        print(f'Starting automatic optimisation of triple split...')
        self.wait_for_acquisition()
        phase_obs, phase_loss, volt_obs, volt_loss = self.get_and_process_acquisition()
        ### Store obervations for plotting/saving
        phase_obs_memory.append([phase_obs])
        phase_loss_memory.append([phase_loss])
        dm_memory.append(self.tomo_values) # Append tomoscope values to allow for saving.
        profile_memory.append(self.tomo_values[-1,:])
        fwhms_memory.append([self.fwhms])
        intensities_memory.append([self.intensities])

        if render_opt:
            plt.figure('Phase optimisation')
            render((-20,20), (-20,20), rel_phase_volt_set, self.profile, 
                    phase_loss, phase_loss_memory, self.tomo_values, 
                    self.bunches, self.fwhms, self.intensities, 
                    criterion=phase_criterion, step=phase_step)

        if initial_feat_guess:
            # Initial feat extractor step
            #initial_guess = self.use_feature_extractor()
            inputs_dict = {INPUTS: self.unaltered_tomo_values}
            outputs_dict = self.feature_extractor.predict(inputs_dict)
            initial_guess = outputs_dict[OUTPUTS]
            initial_guess[2] = 0 # Disregard voltage feature
            converted_action = -initial_guess
            print(f'Taking initial guess from feat extractor: Moving {converted_action}')
            # Track correction
            corr=converted_action
            # h14_offset += converted_action[0]
            # h21_offset += converted_action[1]
            # v14_offset += converted_action[2]
            rel_phase_volt_set.append(rel_phase_volt_set[-1]+corr)
            h14_input = converted_action[0]
            h21_input = converted_action[1]
            #v14_input = converted_action[2]
            print(f'Current relative phase from start: {rel_phase_volt_set[0]}, {rel_phase_volt_set[-1]}')
            print(f'Taking Action: {converted_action})')
            print(f'Profile-based loss: {phase_loss}')
            if self.beam_type == '48b_BCMS':
                #print('Not doing anything, exiting before init guess')
                #sys.exit()
                offset_GSRPC_BCMS(self.japc, phase_offset=h14_input)
                offset_GSRPB_BCMS(self.japc, phase_offset=h21_input)
            elif self.beam_type == '72b_25ns':
                offset_GSRPC(self.japc, phase_offset=h14_input)
                offset_GSRPB(self.japc, phase_offset=h21_input)
            action_memory.append([converted_action])
            phase_step += 1
            tot_steps += 1
        
        #######################################################
        ### Start of phase optimisation using SAC-agent. ######
        
        while not phase_done:
            if tot_steps > max_total_opt_steps:
                print(f'Optimisation not finished in less than {max_total_opt_steps}, exiting...')
                phase_done=True
                volt_done=True
            self.wait_for_acquisition()
            phase_obs, phase_loss, volt_obs, volt_loss = self.get_and_process_acquisition()

            if s1 != None:
                s2 = phase_obs
                r1 = -phase_loss
                step_tuple = (s1,a1,s2,r1)
                phase_agent_tuples.append(step_tuple)

            ### Store obervations for plotting/saving
            phase_obs_memory.append([phase_obs])
            phase_loss_memory.append([phase_loss])
            dm_memory.append(self.tomo_values) # Append tomoscope values to allow for saving.
            profile_memory.append(self.profile)
            fwhms_memory.append([self.fwhms])
            intensities_memory.append([self.intensities])

            if render_opt:
                plt.figure('Phase optimisation')
                render((-20,20), (-20,20), rel_phase_volt_set, self.profile, 
                        phase_loss, phase_loss_memory, self.tomo_values, 
                        self.bunches, self.fwhms, self.intensities, 
                        criterion=phase_criterion, step=phase_step)

            if phase_loss < phase_criterion:
                print(f'Phase criterion reached in {phase_step} steps. Phase optimisation finished, checking voltage...')
                plt.figure('Phase optimisation')
                render((-20,20), (-20,20), 
                        rel_phase_volt_set, 
                        self.profile, 
                        phase_loss, 
                        phase_loss_memory, 
                        self.tomo_values, 
                        self.bunches, 
                        self.fwhms, 
                        self.intensities, 
                        criterion=phase_criterion, 
                        done = True, 
                        step=phase_step)
                #plt.savefig('{}Ep{}_optimisation.png'.format(SAVE_DIR, episode))
                phase_done=True
                break
            else: #phase not yet optimised, take opt. step
                print(f'Phase loss not optimal, taking step {phase_step}...')
                inputs_dict = {INPUTS: phase_obs}
                outputs_dict = self.phase_agent_tri.predict(inputs_dict)
                converted_action = outputs_dict[OUTPUTS]
                converted_action = np.append(converted_action,0)
                #converted_action = self.predict_phase_opt_step(phase_obs)
                corr=converted_action
                # h14_offset += converted_action[0]
                # h21_offset += converted_action[1]
                # v14_offset += 0
                rel_phase_volt_set.append(rel_phase_volt_set[-1]+corr)
                h14_input = converted_action[0]
                h21_input = converted_action[1]
                v14_input = converted_action[2]

                ### Saving tuples of s1,a1,s2,r1

                s1 = phase_obs.copy()
                a1 = converted_action[:2].copy()
            

                print(f'Current relative phase from start: {rel_phase_volt_set[0]}, {rel_phase_volt_set[-1]}')
                print(f'Taking Action: {converted_action})')
                print(f'Profile-based loss: {phase_loss}')
                if self.beam_type == '48b_BCMS':
                    offset_GSRPC_BCMS(self.japc, phase_offset=h14_input)
                    offset_GSRPB_BCMS(self.japc, phase_offset=h21_input)
                elif self.beam_type == '72b_25ns':
                    offset_GSRPC(self.japc, phase_offset=h14_input)
                    offset_GSRPB(self.japc, phase_offset=h21_input)
                action_memory.append([converted_action])
                phase_step += 1
                tot_steps += 1
        
        ####################################################################################################
        ##### Start of Voltage optimisation. Done after phase loss is below criterion (at least once) ######
        ####################################################################################################
        
        max_step_volt = 0.1
        # Wait for next acquisition
        #plt.figure('Voltage optimisation')

        ### Check if voltage already below criterion...
        if volt_loss < volt_criterion:
                print(f'Voltage already optimised. No steps taken.')
                print(f'Total number of steps for phase and volt: {tot_steps}')
                if render_opt:
                    plt.figure('Voltage optimisation')
                    render((-20,20), (-20,20), 
                            rel_phase_volt_set, 
                            self.profile, 
                            volt_loss, 
                            volt_loss_memory, 
                            self.tomo_values, 
                            self.bunches, 
                            self.fwhms, 
                            self.intensities, 
                            criterion=volt_criterion, 
                            step=volt_step,
                            done=True)
                volt_done=True
        
        while not volt_done:
            if tot_steps > max_total_opt_steps:
                print(f'Optimisation not finished in less than {max_total_opt_steps}, exiting...')
                phase_done=True
                volt_done=True

            print(f'Beginning volt step {volt_step}')

            self.wait_for_acquisition()
            # Extract values of acquisition
            phase_obs, phase_loss, volt_obs, volt_loss = self.get_and_process_acquisition()
            volt_obs_memory.append([volt_obs])
            volt_loss_memory.append([volt_loss])
            dm_memory.append(self.tomo_values) # Append tomoscope values to allow for saving.
            profile_memory.append(self.profile)
            fwhms_memory.append([self.fwhms])
            intensities_memory.append([self.intensities])

            if volt_step>=1: # Save second state after taking first step.
                s2_volt = volt_obs.copy()
                r1_volt = -volt_loss
                volt_step_tuple = (s1_volt, a1_volt, s2_volt, r1_volt)
                volt_agent_tuples.append(volt_step_tuple)

            if render_opt:
                plt.figure('Voltage optimisation')
                render((-20,20), (-20,20), 
                        rel_phase_volt_set, 
                        self.profile, 
                        volt_loss, 
                        volt_loss_memory, 
                        self.tomo_values, 
                        self.bunches, 
                        self.fwhms, 
                        self.intensities, 
                        criterion=volt_criterion, 
                        step=volt_step)

            #### Inner loop of phase optimisation. If phase loss deteriorates, return to phase optimisation. ###
            
            if phase_loss > phase_criterion*1.2: # monitor phase loss to make sure it remains acceptable.
                print(f'Phase deteriorated to loss of 1.2*criterion, re-optimising')
                while phase_loss > phase_criterion:
                    
                    #converted_action = self.predict_phase_opt_step(phase_obs)
                    inputs_dict = {INPUTS: phase_obs}
                    outputs_dict = self.phase_agent_tri.predict(inputs_dict)
                    converted_action = outputs_dict[OUTPUTS]
                    converted_action = np.append(converted_action,0)
                    corr=converted_action
                    # h14_offset += converted_action[0]
                    # h21_offset += converted_action[1]
                    # v14_offset += 0
                    rel_phase_volt_set.append(rel_phase_volt_set[-1]+corr)
                    h14_input = converted_action[0]
                    h21_input = converted_action[1]
                    v14_input = converted_action[2]

                    s1 = phase_obs.copy()
                    a1 = converted_action[:2].copy()

                    print(f'Current relative phase from start: {rel_phase_volt_set[0]}, {rel_phase_volt_set[-1]}')
                    print(f'Taking Action: {converted_action})')
                    print(f'Profile-based loss: {phase_loss}')
                    if self.beam_type == '48b_BCMS':
                        offset_GSRPC_BCMS(self.japc, phase_offset=h14_input)
                        offset_GSRPB_BCMS(self.japc, phase_offset=h21_input)
                    elif self.beam_type == '72b_25ns':
                        offset_GSRPC(self.japc, phase_offset=h14_input)
                        offset_GSRPB(self.japc, phase_offset=h21_input)
                    action_memory.append([converted_action])
                    phase_step += 1
                    tot_steps += 1
                    
                    self.wait_for_acquisition()
                    phase_obs, phase_loss, volt_obs, volt_loss = self.get_and_process_acquisition()

                    s2 = phase_obs.copy()
                    r1 = -phase_loss
                    step_tuple = (s1,a1,s2,r1)
                    phase_agent_tuples.append(step_tuple)

                    ### Store obervations for plotting/saving
                    phase_obs_memory.append([phase_obs])
                    phase_loss_memory.append([phase_loss])
                    dm_memory.append(self.tomo_values) # Append tomoscope values to allow for saving.
                    profile_memory.append(self.profile)
                    fwhms_memory.append([self.fwhms])
                    intensities_memory.append([self.intensities])

                    if render_opt:
                        plt.figure('Phase optimisation')
                        render((-20,20), (-20,20), rel_phase_volt_set, self.profile, 
                                phase_loss, phase_loss_memory, self.tomo_values, 
                                self.bunches, self.fwhms, self.intensities, 
                                criterion=phase_criterion, step=phase_step)

            print(f'Phase loss: {phase_loss}, still ok.')
            if volt_loss < volt_criterion:
                print(f'Voltage optimised. Took {volt_step} steps to optimise.')
                print(f'Total number of steps for phase and volt: {tot_steps}')
                if render_opt:
                    plt.figure('Voltage optimisation')
                    render((-20,20), (-20,20), 
                            rel_phase_volt_set, 
                            self.profile, 
                            volt_loss, 
                            volt_loss_memory, 
                            self.tomo_values, 
                            self.bunches, 
                            self.fwhms, 
                            self.intensities, 
                            criterion=volt_criterion, 
                            step=volt_step,
                            done=True)
                volt_done=True
            #values = cv2.resize(values, dsize=(400,150))
            else:
                #action, _ = self.agent_volt_tri.predict(volt_obs, deterministic=True)
                inputs_dict = {INPUTS: volt_obs}
                outputs_dict = self.agent_volt_tri.predict(inputs_dict)
                action = outputs_dict[OUTPUTS]
                volt_action = action * self.voltage_max_step # volt max step
                print(f'Taking volt step: {action*max_step_volt})')
                converted_action = np.array([0.0,0.0,0.0], dtype=object)
                converted_action[2] = action[0]*max_step_volt

                #voltage_factor += action[0]*max_step_volt
                #np.save('/eos/home-j/jwulff/workspaces/RL-007-RFinPS/optimising-rf-manipulations-in-the-ps-through-rl/dataset/tri/new_live_data/p14_{}_p21_{}_v14_{}_datamatrix'.format(int(h14_offset*100), int(h21_offset*100), int(voltage_factor*1000)), values_centered_norm)

                new_phase = rel_phase_volt_set[-1].copy()
                new_phase[2] = new_phase[2]+action[0]*max_step_volt
                rel_phase_volt_set.append(new_phase)

                if self.beam_type == '48b_BCMS':
                    update_voltage_program_GSVMOD7_BCMS(self.japc, volt_action)
                elif self.beam_type == '72b_25ns':
                    update_voltage_program_GSVMOD7(self.japc, volt_action)

                s1_volt = volt_obs.copy()
                a1_volt = converted_action[2]
            volt_step += 1
            tot_steps += 1
            action_memory.append([converted_action])
            dm_memory.append(self.tomo_values)
        print('Exiting optimisation..')
        plt.show(block=block)
        
        if save_dir != None:
            
            print('Saving episode data...')
            now = datetime.datetime.now()
            date_time = now.strftime("%b_%d_%Y_%H-%M-%S")
            episode_save_dir = save_dir + '/Episode_{}'.format(date_time)

            # Create saving directory for this episode with timestamp.
            if not os.path.exists(episode_save_dir):
                os.makedirs(episode_save_dir)
            
            #np.save('{}/phase_set_memory'.format(episode_save_dir), phase_set_memory)
            np.save('{}/action_memory'.format(episode_save_dir), action_memory)
            np.save('{}/dm_memory'.format(episode_save_dir), dm_memory)
            np.save('{}/profile_memory'.format(episode_save_dir), profile_memory)
            np.save('{}/profile_criterion_memory'.format(episode_save_dir), phase_loss_memory)
            np.save('{}/volt_criterion_memory'.format(episode_save_dir), volt_loss_memory)
            np.save('{}/rel_phase_volt_set'.format(episode_save_dir), rel_phase_volt_set)
            np.save('{}/fwhms_memory'.format(episode_save_dir), fwhms_memory)
            np.save('{}/intensities_memory'.format(episode_save_dir), intensities_memory)
            np.save('{}/phase_agent_tuples'.format(episode_save_dir), phase_agent_tuples)
            np.save('{}/volt_agent_tuples'.format(episode_save_dir), volt_agent_tuples)
            
            # Testing data as dictionary
            saving_dict = {}
            saving_dict['steps'] = tot_steps
            saving_dict['phase_step'] = phase_step
            saving_dict['volt_step'] = volt_step
            saving_dict['actions'] = action_memory
            saving_dict['dms'] = dm_memory
            saving_dict['profile_memory'] = profile_memory
            saving_dict['phase_loss_memory'] = phase_loss_memory
            saving_dict['volt_loss_memory'] = volt_loss_memory
            saving_dict['rel_phase_volt_set'] = rel_phase_volt_set
            saving_dict['fwhms'] = fwhms_memory
            saving_dict['intensities'] = intensities_memory
            saving_dict['phase_agent_tuples'] = phase_agent_tuples
            saving_dict['volt_agent_tuples'] = volt_agent_tuples

            with open(episode_save_dir + '/episode_data.pkl', "wb") as file:
                pickle.dump(saving_dict, file, protocol=pickle.HIGHEST_PROTOCOL)

            ### Save optimisation plots
            plt.figure('Voltage optimisation')
            plt.savefig('{}/volt_optimisation_plot.png'.format(episode_save_dir))
            plt.figure('Phase optimisation')
            plt.savefig('{}/phase_optimisation_plot.png'.format(episode_save_dir))


    def predict_phase_opt_step(self, phase_obs):
        max_step_size = 10
        action, _states = self.agent_tri.predict(phase_obs, deterministic=True)
        converted_action = np.array([0.0,0.0,0.0], dtype=object)
        converted_action[0] = action[0]*max_step_size
        converted_action[1] = action[1]*max_step_size
        return converted_action

    def start_automatic_optimisation_quad(self, 
                                max_steps=30, 
                                render_opt=False,
                                phase_42_criterion=0.0006,
                                phase_84_criterion = 0.0008,
                                save_dir = None):

        """ 
        Main function to run to run a complete episode of the quad split optimisation. Currently using separate agents for 42/84 harmonics.

        9th NOVEMBER: Testing rolling average under criterion to consider episode completed (rather than reaching objective once).
        """
        p42_done = False
        p84_done = False
        last_three_p42_loss = []
        last_three_p84_loss = []

        s1,a1,s2,r1 = None, None, None, None

        phase_42_step = 0
        phase_84_step = 0

        tot_steps = 0
        action_memory = []
        phase_set_memory = [] # Doubles as labels for dm:s in dm_memory. May need to convert them to resemble labels in original dataset.
        dm_memory = []
        phase_42_loss_memory = []
        phase_84_loss_memory = []
        profile_42_memory = []
        profile_84_memory = []

        fwhms_memory = []
        intensities_memory = []
        rel_phase_set = [[0,0]]

        ### Phase and volt criterion - decides what loss they will accept and terminate with.
        phase_42_criterion = phase_42_criterion
        phase_84_criterion = phase_84_criterion

        ### Max opt steps allowed - How many steps are the phase/voltage agents allowed to take before terminating run.
        max_total_opt_steps = max_steps

        max_step_size = 20

        ### Trying to save things to make future online RL possible.
        phase_42_obs_memory = []
        phase_agent_tuples = []

        phase_84_obs_memory = []

        ### Save directory. If not input, will not save anything.
        save_dir=save_dir
        
        print(f'Starting automatic optimisation of quadruple split...')
        

        phase_done = False
        #######################################################
        ### Start of phase optimisation using SAC-agents. ######
        
        while not phase_done:
            if tot_steps > max_total_opt_steps:
                print(f'Optimisation not finished in less than {max_total_opt_steps}, exiting...')
                sys.exit()
            self.wait_for_acquisition()
            p42_obs, p42_loss, p84_obs, p84_loss = self.get_and_process_acquisition(mode='quad')
            ### Store obervations for plotting/saving
            phase_42_obs_memory.append([p42_obs])
            phase_42_loss_memory.append([p42_loss])
            phase_84_obs_memory.append([p84_obs])
            phase_84_loss_memory.append([p84_loss])
            dm_memory.append(self.tomo_values) # Append tomoscope values to allow for saving.
            profile_84_memory.append(self.profile_84)
            profile_42_memory.append(self.profile_42)

            # Collect last three p42/p84 losses and compute average.
            avg_last_three_p42_loss = np.sum(phase_42_loss_memory[-3:])/3
            avg_last_three_p84_loss = np.sum(phase_84_loss_memory[-3:])/3

            fwhms_memory.append([self.fwhms])
            intensities_memory.append([self.intensities])

            if render_opt:
                plt.figure('Optimisation')
                render_quad((-20,20), (-20,20), 
                            rel_phase_set, 
                            self.profile_84, 
                            p42_loss, 
                            phase_42_loss_memory, 
                            phase_84_loss_memory, 
                            self.tomo_values, 
                            self.bunches, 
                            self.fwhms, 
                            self.intensities, 
                            criterion=phase_42_criterion, 
                            criterion_84=phase_84_criterion, 
                            step=tot_steps)

                # if s1 != None:
                #     s2 = phase_obs
                #     r1 = -phase_loss
                #     step_tuple = (s1,a1,s2,r1)
                #     phase_agent_tuples.append(step_tuple)

            #if p42_loss < phase_42_criterion and not p42_done:
            if avg_last_three_p42_loss < phase_42_criterion and not p42_done:
                print(f'Phase 42 criterion reached in {phase_42_step} steps.')
                plt.figure('p42 Optimisation')
                render_quad((-20,20), (-20,20), 
                            rel_phase_set, 
                            self.profile_84, 
                            p42_loss, 
                            phase_42_loss_memory, 
                            phase_84_loss_memory, 
                            self.tomo_values, 
                            self.bunches, 
                            self.fwhms, 
                            self.intensities, 
                            criterion=phase_42_criterion, 
                            criterion_84=phase_84_criterion, 
                            step=tot_steps)
                p42_done=True
            #if p84_loss < phase_84_criterion and not p84_done:
            if avg_last_three_p84_loss < phase_84_criterion and not p84_done:
                plt.figure('p84 Optimisation')
                print(f'Phase 84 criterion reached in {phase_84_step} steps.')
                render_quad((-20,20), (-20,20), 
                            rel_phase_set, 
                            self.profile_84, 
                            p42_loss, 
                            phase_42_loss_memory, 
                            phase_84_loss_memory, 
                            self.tomo_values, 
                            self.bunches, 
                            self.fwhms, 
                            self.intensities, 
                            criterion=phase_42_criterion, 
                            criterion_84=phase_84_criterion, 
                            step=tot_steps)
                p84_done=True

            # Monitor losses after reaching criterion, to make sure they don't deteriorate

            if p42_done:
                #if p42_loss > 2*phase_42_criterion:
                if avg_last_three_p42_loss > 1.5*phase_42_criterion:
                    print(f'Phase 42 loss deteriorated after initial sucess to more than 2*limit. Optimising further...')
                    p42_done = False
            if p84_done:
                #if p84_loss > 1.5*phase_84_criterion:
                if avg_last_three_p84_loss > 1.5*phase_84_criterion:
                    print(f'Phase 84 loss deteriorated after initial sucess to more than 2*limit. Optimising further...')
                    p84_done = False
            if p42_done and p84_done:
                print(f'Optimisation finished. Took {tot_steps} steps.')
                phase_done = True
                break

            # Predict
            converted_action_42 = 0.0
            converted_action_84 = 0.0
            if not p42_done:
                inputs_dict = {INPUTS: p42_obs}
                outputs_dict = self.agent_42.predict(inputs_dict)
                action_42 = outputs_dict[OUTPUTS]
                #action_42, _states = self.agent_42.predict(p42_obs, deterministic=True)
                converted_action_42 = action_42 # *max_step_size # If mlp model, already converted.
                phase_42_step += 1

            if not p84_done:
                inputs_dict = {INPUTS: p84_obs}
                outputs_dict = self.agent_84.predict(inputs_dict)
                action_84 = outputs_dict[OUTPUTS]
                #action, _ = self.agent_84.predict(p84_obs, deterministic=True)
                converted_action_84 = action_84 # *max_step_size # If mlp model, already converted.
                phase_84_step += 1

            tot_steps += 1
            # Take step
            corr = np.append(converted_action_42,converted_action_84)
            #dpc20_offset += converted_action_42
            #dpc40_offset += converted_action_84

            rel_phase_set.append(rel_phase_set[-1]+corr)

            #new_h42 = h42_setting+converted_action_42#.astype(np.float32)
            #new_h84 = h84_setting+converted_action_84#.astype(np.float32)

            # Calculate manual input for splitting adjustment.
            h42_input = converted_action_42
            h84_input = converted_action_84

            #print(f'Current phase set: {h42_setting}, {h84_setting}')
            print(f'Taking Action: {corr})')
            print(f'Profile-based loss42: {p42_loss}, loss84: {p84_loss}')
            offset_C20(self.japc, phase_offset=h42_input)
            offset_C40(self.japc, phase_offset=h84_input)
            action_memory.append([corr])
        print('Exiting optimisation..')
        plt.close('Optimisation')
        plt.figure('p42 Optimisation')
        render_quad((-20,20), (-20,20), 
                    rel_phase_set, 
                    self.profile_84, 
                    p42_loss, 
                    phase_42_loss_memory, 
                    phase_84_loss_memory, 
                    self.tomo_values, 
                    self.bunches, 
                    self.fwhms, 
                    self.intensities, 
                    criterion=phase_42_criterion, 
                    criterion_84=phase_84_criterion, 
                    step=tot_steps,
                    done=True)
        plt.figure('p84 Optimisation')
        render_quad((-20,20), (-20,20), 
                    rel_phase_set, 
                    self.profile_84, 
                    p42_loss, 
                    phase_42_loss_memory, 
                    phase_84_loss_memory, 
                    self.tomo_values, 
                    self.bunches, 
                    self.fwhms, 
                    self.intensities, 
                    criterion=phase_42_criterion, 
                    criterion_84=phase_84_criterion, 
                    step=tot_steps,
                    done=True)
        plt.show(block=True)
        
        if save_dir != None:
            
            print('Saving episode data...')
            now = datetime.datetime.now()
            date_time = now.strftime("%b_%d_%Y_%H-%M-%S")
            episode_save_dir = save_dir + '/Episode_{}'.format(date_time)

            # Create saving directory for this episode with timestamp.
            if not os.path.exists(episode_save_dir):
                os.makedirs(episode_save_dir)
            
            #np.save('{}/phase_set_memory'.format(episode_save_dir), phase_set_memory)
            np.save('{}/action_memory'.format(episode_save_dir), action_memory)
            np.save('{}/dm_memory'.format(episode_save_dir), dm_memory)
            np.save('{}/profile_42_memory'.format(episode_save_dir), profile_42_memory)
            np.save('{}/profile_84_memory'.format(episode_save_dir), profile_84_memory)
            np.save('{}/phase_42_loss_memory'.format(episode_save_dir), phase_42_loss_memory)
            np.save('{}/phase_84_loss_memory'.format(episode_save_dir), phase_84_loss_memory)
            np.save('{}/rel_phase_set'.format(episode_save_dir), rel_phase_set)
            np.save('{}/fwhms_memory'.format(episode_save_dir), fwhms_memory)
            np.save('{}/intensities_memory'.format(episode_save_dir), intensities_memory)
            # TO BE IMPLEMENTED...
            #np.save('{}/phase_agent_tuples'.format(episode_save_dir), phase_agent_tuples)
            #np.save('{}/volt_agent_tuples'.format(episode_save_dir), volt_agent_tuples)

            # Testing data as dictionary
            saving_dict = {}
            saving_dict['steps_p42'] = phase_42_step
            saving_dict['steps_p84'] = phase_84_step
            saving_dict['actions'] = action_memory
            saving_dict['dms'] = dm_memory
            saving_dict['profile_42_memory'] = profile_42_memory
            saving_dict['profile_84_memory'] = profile_84_memory
            saving_dict['p42_loss_memory'] = phase_42_loss_memory
            saving_dict['p84_loss_memory'] = phase_84_loss_memory
            saving_dict['rel_phase_set'] = rel_phase_set
            saving_dict['fwhms'] = fwhms_memory
            saving_dict['intensities'] = intensities_memory
            #saving_dict['agent_tuples'] = agent_tuples
            with open(episode_save_dir + '/episode_data.pkl', "wb") as file:
                pickle.dump(saving_dict, file, protocol=pickle.HIGHEST_PROTOCOL)
            ### Save optimisation plots
            plt.figure('Optimisation')
            plt.savefig('{}/optimisation_plot.png'.format(episode_save_dir))

    def start_automatic_optimisation_tri_all_params_agent(self, 
                                    initial_feat_guess=True, 
                                    max_steps=30, 
                                    render_opt=False,
                                    phase_volt_criterion=0.0008,
                                    save_dir = None,
                                    block=True):

        """ 
        Main function to run to run a complete episode of triple split optimisation.
        """
        # Tri
        model_name = r"SAC-RelBLInt-allparams-sprof-step10-002-59521-ref-NoiseMoveInj-diff-00008_900k"#r"SAC-RelBLInt-allparams-sprof-step10-01-59521-ref-NoiseMoveInj-diff-00008_770k"
        self.agent_tri_all_params = SAC.load(parent_path + "/RL_models/tri/{}".format(model_name))
        done = False

        s1,a1,s2,r1 = None, None, None, None

        step = 0
        #volt_step = 0
        #tot_steps = 0
        phase_volt_set_memory = []
        action_memory = []
        dm_memory = []
        phase_loss_memory = []
        loss_memory = []
        profile_memory = []
        fwhms_memory = []
        intensities_memory = []
        rel_phase_volt_set = [[0,0,0]]
        agent_tuples = []

        ### Phase and volt criterion - decides what loss they will accept and terminate with.
        phase_volt_criterion = phase_volt_criterion

        ### Max opt steps allowed - How many steps are the phase/voltage agents allowed to take before terminating run.
        max_total_opt_steps = max_steps

        ### Trying to save things to make future online RL possible.
        obs_memory = []
        agent_tuples = []

        ### Save directory. If not input, will not save anything.
        save_dir=save_dir
        
        print(f'Starting automatic optimisation of triple split...')
        self.wait_for_acquisition()
        phase_obs, phase_loss, volt_obs, volt_loss = self.get_and_process_acquisition() # Use volt obs and loss for general optimisation.
        ### Store obervations for plotting/saving
        obs_memory.append([volt_obs])
        phase_loss_memory.append([phase_loss]) # Store for potential interest
        loss_memory.append([volt_loss])
        dm_memory.append(self.tomo_values) # Append tomoscope values to allow for saving.
        profile_memory.append(self.tomo_values[-1,:])
        fwhms_memory.append([self.fwhms])
        intensities_memory.append([self.intensities])

        if render_opt:
            plt.figure('Optimisation')
            render((-20,20), (-20,20), 
                    rel_phase_volt_set, 
                    self.profile, 
                    volt_loss, 
                    loss_memory, 
                    self.tomo_values, 
                    self.bunches, 
                    self.fwhms, 
                    self.intensities, 
                    criterion=phase_volt_criterion, 
                    step=step)

        if initial_feat_guess:
            # Initial feat extractor phase step
            initial_guess = self.feature_extractor.predict(self.tomo_values)
            converted_action = -initial_guess
            print(f'Taking initial guess from feat extractor: Moving {converted_action}')
            # Track correction
            corr=converted_action
            # h14_offset += converted_action[0]
            # h21_offset += converted_action[1]
            # v14_offset += converted_action[2]
            rel_phase_volt_set.append(rel_phase_volt_set[-1]+corr)
            h14_input = converted_action[0]
            h21_input = converted_action[1]
            #v14_input = converted_action[2]
            print(f'Current relative phase from start: {rel_phase_volt_set[0]}, {rel_phase_volt_set[-1]}')
            print(f'Taking Action: {converted_action})')
            print(f'Profile-based loss: {phase_loss}')
            if self.beam_type == '48b_BCMS':
                #print('Not doing anything, exiting before init guess')
                #sys.exit()
                offset_GSRPC_BCMS(self.japc, phase_offset=h14_input)
                offset_GSRPB_BCMS(self.japc, phase_offset=h21_input)
            elif self.beam_type == '72b_25ns':
                offset_GSRPC(self.japc, phase_offset=h14_input)
                offset_GSRPB(self.japc, phase_offset=h21_input)
            action_memory.append([converted_action])
            step += 1
        
        #######################################################
        ### Start of optimisation using SAC-agent (acting on phase AND voltage). ######
        
        while not done:
            if step > max_total_opt_steps:
                print(f'Optimisation not finished in less than {max_total_opt_steps}, exiting...')
                done=True
            self.wait_for_acquisition()
            phase_obs, phase_loss, volt_obs, volt_loss = self.get_and_process_acquisition()

            if s1 != None:
                s2 = phase_obs
                r1 = -phase_loss
                step_tuple = (s1,a1,s2,r1)
                agent_tuples.append(step_tuple)

            ### Store obervations for plotting/saving
            obs_memory.append([volt_obs])
            loss_memory.append([volt_loss])
            dm_memory.append(self.tomo_values) # Append tomoscope values to allow for saving.
            profile_memory.append(self.profile)
            fwhms_memory.append([self.fwhms])
            intensities_memory.append([self.intensities])

            if render_opt:
                plt.figure('Optimisation')
                render((-20,20), (-20,20), 
                    rel_phase_volt_set, 
                    self.profile, 
                    volt_loss, 
                    loss_memory, 
                    self.tomo_values, 
                    self.bunches, 
                    self.fwhms, 
                    self.intensities, 
                    criterion=phase_volt_criterion, 
                    step=step)

            if volt_loss < phase_volt_criterion:
                print(f'Criterion reached in {step} steps. Optimisation finished.')
                plt.figure('Optimisation')
                render((-20,20), (-20,20), 
                        rel_phase_volt_set, 
                        self.profile, 
                        volt_loss, 
                        loss_memory, 
                        self.tomo_values, 
                        self.bunches, 
                        self.fwhms, 
                        self.intensities, 
                        criterion=phase_volt_criterion, 
                        done = True, 
                        step=step)
                #plt.savefig('{}Ep{}_optimisation.png'.format(SAVE_DIR, episode))
                done=True
                break
            else: # Not yet optimised, take opt. step
                print(f'Loss not optimal, taking step {step}...')
                converted_action = self.predict_opt_step_all_params(volt_obs)
                corr=converted_action.copy()
                # h14_offset += converted_action[0]
                # h21_offset += converted_action[1]
                # v14_offset += 0
                max_step_volt = 0.02
                corr[2] = corr[2]*max_step_volt
                rel_phase_volt_set.append(rel_phase_volt_set[-1]+corr)
                h14_input = converted_action[0]
                h21_input = converted_action[1]
                v14_input = converted_action[2]
                
                volt_action = v14_input * self.voltage_max_step*0.2 # volt max step calculated as 0.1 of reference, we want 0.02 of reference
                print(f'Taking volt step: {v14_input*max_step_volt})')
                converted_action[2] = v14_input*max_step_volt
                #new_phase = rel_phase_volt_set[-1].copy()
                #new_phase[2] = new_phase[2]+v14_input*max_step_volt
                #rel_phase_volt_set.append(new_phase)
                ### Saving tuples of s1,a1,s2,r1

                s1 = phase_obs.copy()
                a1 = converted_action[:2].copy()


                print(f'Current relative phase from start: {rel_phase_volt_set[0]}, {rel_phase_volt_set[-1]}')
                print(f'Taking Action: {converted_action})')
                print(f'Profile-based loss: {volt_loss}')
                if self.beam_type == '48b_BCMS':
                    offset_GSRPC_BCMS(self.japc, phase_offset=h14_input)
                    offset_GSRPB_BCMS(self.japc, phase_offset=h21_input)
                    update_voltage_program_GSVMOD7_BCMS(self.japc, volt_action)
                elif self.beam_type == '72b_25ns':
                    offset_GSRPC(self.japc, phase_offset=h14_input)
                    offset_GSRPB(self.japc, phase_offset=h21_input)
                    update_voltage_program_GSVMOD7(self.japc, volt_action)
                
                #voltage_factor += action[0]*max_step_volt
                #np.save('/eos/home-j/jwulff/workspaces/RL-007-RFinPS/optimising-rf-manipulations-in-the-ps-through-rl/dataset/tri/new_live_data/p14_{}_p21_{}_v14_{}_datamatrix'.format(int(h14_offset*100), int(h21_offset*100), int(voltage_factor*1000)), values_centered_norm)

                action_memory.append([converted_action])
                step += 1
        print('Exiting optimisation..')
        plt.show(block=False)
        
        if save_dir != None:
            
            print('Saving episode data...')
            now = datetime.datetime.now()
            date_time = now.strftime("%b_%d_%Y_%H-%M-%S")
            episode_save_dir = save_dir + '/Episode_{}'.format(date_time)

            # Create saving directory for this episode with timestamp.
            if not os.path.exists(episode_save_dir):
                os.makedirs(episode_save_dir)
            
            #np.save('{}/phase_set_memory'.format(episode_save_dir), phase_set_memory)
            np.save('{}/action_memory'.format(episode_save_dir), action_memory)
            np.save('{}/dm_memory'.format(episode_save_dir), dm_memory)
            np.save('{}/profile_memory'.format(episode_save_dir), profile_memory)
            np.save('{}/profile_criterion_memory'.format(episode_save_dir), loss_memory)
            np.save('{}/volt_criterion_memory'.format(episode_save_dir), loss_memory)
            np.save('{}/rel_phase_volt_set'.format(episode_save_dir), rel_phase_volt_set)
            np.save('{}/fwhms_memory'.format(episode_save_dir), fwhms_memory)
            np.save('{}/intensities_memory'.format(episode_save_dir), intensities_memory)
            np.save('{}/phase_agent_tuples'.format(episode_save_dir), agent_tuples)
            
            # Testing data as dictionary
            saving_dict = {}
            saving_dict['steps'] = step
            saving_dict['actions'] = action_memory
            saving_dict['dms'] = dm_memory
            saving_dict['profile_memory'] = profile_memory
            saving_dict['loss_memory'] = loss_memory
            saving_dict['rel_phase_volt_set'] = rel_phase_volt_set
            saving_dict['fwhms'] = fwhms_memory
            saving_dict['intensities'] = intensities_memory
            saving_dict['agent_tuples'] = agent_tuples

            with open(episode_save_dir + '/episode_data.pkl', "wb") as file:
                pickle.dump(saving_dict, file, protocol=pickle.HIGHEST_PROTOCOL)
            ### Save optimisation plots
            plt.figure('Optimisation')
            plt.savefig('{}/optimisation_plot.png'.format(episode_save_dir))
        

    def predict_opt_step_all_params(self, obs):
        max_step_size = 10
        action, _states = self.agent_tri_all_params.predict(obs, deterministic=True)
        converted_action = np.array([0.0,0.0,0.0], dtype=object)
        converted_action[0] = action[0]*max_step_size
        converted_action[1] = action[1]*max_step_size
        converted_action[2] = action[2]

        return converted_action