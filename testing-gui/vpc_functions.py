from operator import index
import numpy as np
import matplotlib.pyplot as plt
import scipy.interpolate as interp
from scipy.signal import find_peaks
import torch

# Calculate criterion as a loss
def loss_function_two(b1,b2, verbose=False): 
    # Single out each bunch. 400 bins and three bunches, initial bucket 308ns/3 ~ 102.6667

    mse12 = np.mean((b1-b2)**2)

    # Sum all MSE:s to get a single loss
    loss = mse12
    if verbose:
        print(f"loss: {loss}")
    #reward = -loss
    return loss  

def load_model(path, model_base):
    checkpoint = torch.load(path)
    model_base.load_state_dict(checkpoint['model_state_dict'])
    #optimizer.load_state_dict(checkpoint['optimizer_state_dict'])
    #criterion = checkpoint['loss']
    print(f'Weights loaded...')
    return model_base

def process_tomoscope_acquisition(datamatrix, mode='quad', remove_tails = False, correct_dm=False):

    # Convert and normalize to look similar as simulated data. 
    values = datamatrix # Matrix if using tomoscope
    values = values.astype(np.float64)
    values = values-np.min(values)

    # Normalize
    values = values/np.max(values)

    # Test noise area and compute mean to center noise around zero
    if mode=='quad':
        values_noise = np.vstack((values[0:50,150:180], values[0:50,20:50])) # Works for quad
    elif mode=='tri':
        values_noise = np.vstack((values[0:25,350:380], values[0:25,20:50])) # testing for tri
        std = np.std(values_noise)

    values_centered = values - np.mean(values_noise)
    # print(np.mean(values_centered[0:50,,150:180]))
    if correct_dm:
        values_centered = correct_dm_tri(values_centered)
    if remove_tails:
        #######
        # Remove tails that of other bunches that may be present in trisplit 
        # tomscope acquisition. Replace with noise sampled from acquisition itself. 
        #######
        rows, _ = values_centered.shape
        values_centered[:,:40] = np.random.default_rng().normal(0,std,(rows,40)) #+ 0.025
        values_centered[:,360:] = np.random.default_rng().normal(0,std,(rows,40)) # + 0.025
    # values_centered = values_centered - 0.025
    # print('using specific values for normalizing, KEEP IN MIND')

    # Normalize again to make max value 1.
    values_centered_norm = values_centered/np.max(values_centered)
    values_centered_norm[-1,:] = values_centered_norm[-2,:] # Remove final trace as it shifts to one direction, and just copy second to last
    return values_centered_norm



def correct_profile(profile):
    # Linear approximation of shift over the bunches
    profile = profile/np.max(profile) # normalize
    
    index_of_peaks, peak_heights = find_peaks(profile, height=0.15, distance=100)
    #if len(index_of_peaks) == 72:
    #    print("Did not find 72 peaks")
    #    return 
    x0 = index_of_peaks[0] - 150
    y0 = profile[x0]
    x1 = index_of_peaks[-1] + 150 # End index a bit after the final bunch
    y1=np.mean(profile[x1-10:x1+10])

    # point 1 is (x0,y0), point 2 is (x1,y1)
    m, b =np.polyfit([x0,x1],[y0,y1],1)
    #line_approx = interp.interp1d([x0,x1],[y0,y1])
    corrected_profile = np.zeros(len(profile[:x1]))
    for i, value in enumerate(profile[:x1]):
        corrected_profile[i] = value - (m*i+b) #line_approx(i)
    return corrected_profile

def correct_dm_tri(dm):
    # Linear approximation of shift over the bunches
    profile = dm[1,:]
    #if len(index_of_peaks) == 72:
    #    print("Did not find 72 peaks")
    #    return 
    x0 = 40
    y0 = np.mean(profile[x0:x0+10])
    x1 = len(profile)-41# End index a bit after the final bunch
    y1=np.mean(profile[x1-10:x1])

    # point 1 is (x0,y0), point 2 is (x1,y1)
    m, b =np.polyfit([x0,x1],[y0,y1],1)
    #line_approx = interp.interp1d([x0,x1],[y0,y1])
    correction = np.zeros(len(profile))
    for i, value in enumerate(profile):
        correction[i] = -(m*i+b) #line_approx(i)
    rows,cols = np.shape(dm)
    for row in range(rows):
        dm[row,:] = dm[row,:] + correction

    return dm

def tri_phase_loss(b1,b3, verbose=False):
    mse13 = np.mean((b1-b3)**2)

    # Sum all MSE:s to get a single loss
    loss = mse13
    if verbose:
        print(f"loss: {loss}")
    #reward = -loss
    return loss 

def peaks(profile, height = 0.2, distance=100):
    profile = profile#/np.max(profile) # normalize
    index_of_peaks, peak_heights = find_peaks(profile, height=height, distance=distance)
    return index_of_peaks, peak_heights['peak_heights']

def FWHM(profile, index_of_peaks, peak_heights):
    FWHMs = np.array([0]*len(index_of_peaks))
    l_edges = []
    r_edges = []
    i = 0
    for peak, peak_height in zip(index_of_peaks, peak_heights):
        maximum = peak_height
        minimum = 0 # Something else??
        half_max = maximum/2
        #locate left edge
        l_edge_found = False
        search_idx = peak
        while not l_edge_found:
            value = profile[search_idx]
            #print(value)
            if value <= half_max:
                left_edge = search_idx
                l_edges.append(left_edge)
                l_edge_found=True
            search_idx -= 1
        #locate right edge
        r_edge_found = False
        search_idx = peak
        while not r_edge_found:
            value = profile[search_idx]
            if value <= half_max:
                right_edge = search_idx
                r_edges.append(right_edge)
                r_edge_found=True
            search_idx += 1
        FWHMs[i] = right_edge-left_edge # Given in bins
        i+=1
    return FWHMs, l_edges, r_edges

# Define loss function

# Calculate criterion as a loss
def loss_function(b1,b2,b3,b4, verbose=False): 
    # Single out each bunch. 400 bins and three bunches, initial bucket 308ns/3 ~ 102.6667

    #MSE between bunch 1 and bunch 2 profiles
    mse12 = np.mean((b1-b2)**2)
    mse13 = np.mean((b1-b3)**2)
    mse14 = np.mean((b1-b4)**2)

    mse23 = np.mean((b2-b3)**2)
    mse24 = np.mean((b2-b4)**2)

    mse34 = np.mean((b3-b4)**2)

    


    # Sum all MSE:s to get a single loss
    loss = (mse12 + mse13 + mse14 + mse23 + mse24 + mse34)/6
    if verbose:
        print(f"loss: {loss}")
    #reward = -loss
    return loss

# Calculate criterion as a loss
def loss_function_tri(b1,b2,b3, verbose=False): 
    # Single out each bunch. 400 bins and three bunches, initial bucket 308ns/3 ~ 102.6667

    mse12 = np.mean((b1-b2)**2)
    mse13 = np.mean((b1-b3)**2)
    mse23 = np.mean((b2-b3)**2)

    # Sum all MSE:s to get a single loss
    loss = (mse12 + mse13+ mse23)/3
    if verbose:
        print(f"loss: {loss}")
    #reward = -loss
    return loss  

def isolate_bunches_from_dm_profile(profile, plot_found_bunches=False, bunch_width = 15, intensities=False, rel=False, distance=50, height=0.2):
    index_of_peaks, peak_heights = peaks(profile, height=height, distance=distance)
    # print(index_of_peaks)
    #plt.figure()
    #plt.plot(profile)
    #plt.plot(index_of_peaks,peak_heights,'x')

    # Find l,r edges
    centers = np.array([0]*len(index_of_peaks))
    fwhms = np.array([0]*len(index_of_peaks))

    l_edges = []
    r_edges = []
    i = 0
    for peak, peak_height in zip(index_of_peaks, peak_heights):
        maximum = peak_height
        minimum = 0 # Something else??
        half_max = maximum/2
        #locate left edge
        l_edge_found = False
        search_idx = peak
        while not l_edge_found:
            value = profile[search_idx]
            #print(value)
            if value <= half_max:
                left_edge = search_idx
                l_edges.append(left_edge)
                l_edge_found=True
            search_idx -= 1
        #locate right edge
        r_edge_found = False
        search_idx = peak
        while not r_edge_found:
            value = profile[search_idx]
            if value <= half_max:
                right_edge = search_idx
                r_edges.append(right_edge)
                r_edge_found=True
            search_idx += 1
        centers[i] = int((right_edge-left_edge)/2)+left_edge # Given in bins
        fwhms[i] = right_edge-left_edge # FWHM in bins
        i += 1
    if plot_found_bunches:

        plt.figure()
        plt.plot(profile)
        plt.plot(index_of_peaks,peak_heights,'x')
        for l_edge in l_edges:
            plt.axvline( x=l_edge,color='g')
        for r_edge in r_edges:
            plt.axvline( x=r_edge, color='r')
        for center in centers:
            plt.axvline( x=center, color='b')
    
    if rel:
        # Normalize relative to current fwhms
        fwhms = fwhms/np.max(fwhms)
    else:
        # Normalize with global constant
        fwhms =fwhms/52 # Fixed division to scale bunch lengths similarly as simulated data. At 0,0 offset, blengths of around 0.85. TESTING!!
    bunches = [] # list of individual bunches
    for center in centers:
        b = profile[center-bunch_width:center+bunch_width]
        bunches.append(b)
    if intensities:
        ints = np.zeros(len(index_of_peaks))
        for i, (l, r) in enumerate(zip(l_edges, r_edges)):
            ints[i] = np.sum(profile[l:r])
        return bunches, fwhms, ints
    return bunches, fwhms

def isolate_bunches_from_dm_profile_tri(profile, plot_found_bunches=False, bunch_width = 20, intensities=False, rel=False):
    index_of_peaks, peak_heights = peaks(profile, distance=50)
    # print(index_of_peaks)
    #plt.figure()
    #plt.plot(profile)
    #plt.plot(index_of_peaks,peak_heights,'x')

    # Find l,r edges
    centers = np.array([0]*len(index_of_peaks))
    fwhms = np.array([0]*len(index_of_peaks))

    l_edges = []
    r_edges = []
    i = 0
    for peak, peak_height in zip(index_of_peaks, peak_heights):
        maximum = peak_height
        minimum = 0 # Something else??
        half_max = maximum/2
        #locate left edge
        l_edge_found = False
        search_idx = peak
        while not l_edge_found:
            value = profile[search_idx]
            #print(value)
            if value <= half_max:
                left_edge = search_idx
                l_edges.append(left_edge)
                l_edge_found=True
            search_idx -= 1
        #locate right edge
        r_edge_found = False
        search_idx = peak
        while not r_edge_found:
            value = profile[search_idx]
            if value <= half_max:
                right_edge = search_idx
                r_edges.append(right_edge)
                r_edge_found=True
            search_idx += 1
        centers[i] = int((right_edge-left_edge)/2)+left_edge # Given in bins
        fwhms[i] = right_edge-left_edge # FWHM in bins
        i += 1
    if plot_found_bunches:

        plt.figure()
        plt.title('Find bunch centers')
        plt.plot(profile)
        plt.plot(index_of_peaks,peak_heights,'x')
        for l_edge in l_edges:
            plt.axvline( x=l_edge,color='g')
        for r_edge in r_edges:
            plt.axvline( x=r_edge, color='r')
        for center in centers:
            plt.axvline( x=center, color='b', label='Bunch centers')
        plt.legend()
    
    if rel:
        # Normalize relative to current fwhms
        fwhms = fwhms/np.max(fwhms)
    else:
        # Normalize with global constant
        fwhms =fwhms/52 # Fixed division to scale bunch lengths similarly as simulated data. At 0,0 offset, blengths of around 0.85. TESTING!!
    bunches = [] # list of individual bunches
    for center in centers:
        b = profile[center-bunch_width:center+bunch_width]
        bunches.append(b)
    if intensities:
        ints = np.zeros(3)
        for i, (l, r) in enumerate(zip(l_edges, r_edges)):
            ints[i] = np.sum(profile[l:r])
        return bunches, fwhms, ints
    return bunches, fwhms

def savitzky_golay(y, window_size, order, deriv=0, rate=1):
    r"""Smooth (and optionally differentiate) data with a Savitzky-Golay filter.
    The Savitzky-Golay filter removes high frequency noise from data.
    It has the advantage of preserving the original shape and
    features of the signal better than other types of filtering
    approaches, such as moving averages techniques.
    Parameters
    ----------
    y : array_like, shape (N,)
        the values of the time history of the signal.
    window_size : int
        the length of the window. Must be an odd integer number.
    order : int
        the order of the polynomial used in the filtering.
        Must be less then `window_size` - 1.
    deriv: int
        the order of the derivative to compute (default = 0 means only smoothing)
    Returns
    -------
    ys : ndarray, shape (N)
        the smoothed signal (or it's n-th derivative).
    Notes
    -----
    The Savitzky-Golay is a type of low-pass filter, particularly
    suited for smoothing noisy data. The main idea behind this
    approach is to make for each point a least-square fit with a
    polynomial of high order over a odd-sized window centered at
    the point.
    Examples
    --------
    t = np.linspace(-4, 4, 500)
    y = np.exp( -t**2 ) + np.random.normal(0, 0.05, t.shape)
    ysg = savitzky_golay(y, window_size=31, order=4)
    import matplotlib.pyplot as plt
    plt.plot(t, y, label='Noisy signal')
    plt.plot(t, np.exp(-t**2), 'k', lw=1.5, label='Original signal')
    plt.plot(t, ysg, 'r', label='Filtered signal')
    plt.legend()
    plt.show()
    References
    ----------
    .. [1] A. Savitzky, M. J. E. Golay, Smoothing and Differentiation of
       Data by Simplified Least Squares Procedures. Analytical
       Chemistry, 1964, 36 (8), pp 1627-1639.
    .. [2] Numerical Recipes 3rd Edition: The Art of Scientific Computing
       W.H. Press, S.A. Teukolsky, W.T. Vetterling, B.P. Flannery
       Cambridge University Press ISBN-13: 9780521880688
    """
    import numpy as np
    from math import factorial
    
    try:
        window_size = np.abs(np.int(window_size))
        order = np.abs(np.int(order))
    except ValueError as msg:
        raise ValueError("window_size and order have to be of type int")
    if window_size % 2 != 1 or window_size < 1:
        raise TypeError("window_size size must be a positive odd number")
    if window_size < order + 2:
        raise TypeError("window_size is too small for the polynomials order")
    order_range = range(order+1)
    half_window = (window_size -1) // 2
    # precompute coefficients
    b = np.mat([[k**i for i in order_range] for k in range(-half_window, half_window+1)])
    m = np.linalg.pinv(b).A[deriv] * rate**deriv * factorial(deriv)
    # pad the signal at the extremes with
    # values taken from the signal itself
    firstvals = y[0] - np.abs( y[1:half_window+1][::-1] - y[0] )
    lastvals = y[-1] + np.abs(y[-half_window-1:-1][::-1] - y[-1])
    y = np.concatenate((firstvals, y, lastvals))
    return np.convolve( m[::-1], y, mode='valid')

def smooth_profile(profile, window=11, order=3):
    return savitzky_golay(profile, window, order)

def render(phase_1_range, phase_2_range, setting_memory, profile, loss, loss_memory, datamatrix, bunches, fwhms, intensities, criterion = 0.0010, done=False, step=0):
        
    """
    Render walking through phase space with voltage on the side and current profile/loss alongside.
    """


    x=[]
    y=[]
    z=[]
    plt.clf()
    plt.suptitle(f'Step: {step}')

    for phase_set in setting_memory:
        x.append(phase_set[0])
        y.append(phase_set[1])
        z.append(phase_set[2])
    plt.subplot(331)
    plt.title('Voltage factor changes')
    plt.plot(z, 'yo-')
    
    plt.subplot(332)
    plt.ylim((phase_1_range[0]-5,phase_1_range[1]+5))
    plt.xlim((phase_2_range[0]-5,phase_2_range[1]+5))
    plt.xlabel('p14 changes')
    plt.ylabel('p21 changes')
    vf = setting_memory[-1][2]
    plt.title(f'vf = {vf:4f}')
    plt.plot(x, y, 'go-')
    plt.plot(x[-1],y[-1],'ro')
    plt.subplot(333)
    plt.title(f'Loss: {loss:4f}')
    plt.plot(profile)

    plt.subplot(334)
    plt.title("Loss")
    plt.plot(loss_memory, 'o-')
    plt.plot(np.linspace(0,len(loss_memory),len(loss_memory)+1),np.ones(len(loss_memory)+1)*criterion, 'k--', label='Stop Criterion')
    plt.show()
    plt.subplot(335)
    plt.title("tomo acq.")
    plt.imshow(datamatrix, aspect='auto')
    plt.show()
    print(f'Current loss: {loss}')
    plt.subplot(336)
    plt.plot(bunches[0], label='b1')
    plt.plot(bunches[1], label='b2')
    plt.plot(bunches[2], label='b3')
    plt.subplot(337)
    plt.ylim((-0.3,0.3))
    plt.plot(fwhms, 'go-', label='fwhms')
    plt.plot(intensities, 'bo-', label='Rel. intensity')
    plt.legend()
    fig = plt.gcf()
    axes = plt.gca()

    #plot_finish(fig=fig, axes=axes, xlabel='Setting', ylabel='Observable')
    plt.pause(0.2)

def render_quad(phase_1_range, phase_2_range, setting_memory, profile, loss, loss_memory_42, loss_memory_84, datamatrix, bunches, fwhms, intensities, criterion = 0.0010, done=False, step=0):
        
    """
    Render walking through phase space with voltage on the side and current profile/loss alongside.
    """


    x=[]
    y=[]
    z=[]
    plt.clf()
    plt.suptitle(f'Step: {step}')

    for phase_set in setting_memory:
        x.append(phase_set[0])
        y.append(phase_set[1])
    
    plt.subplot(331)
    plt.ylim((phase_1_range[0]-5,phase_1_range[1]+5))
    plt.xlim((phase_2_range[0]-5,phase_2_range[1]+5))
    plt.xlabel('p14 changes')
    plt.ylabel('p21 changes')
    plt.plot(x, y, 'go-')
    plt.plot(x[-1],y[-1],'ro')
    plt.subplot(332)
    plt.title(f'Loss: {loss:4f}')
    plt.plot(profile)

    plt.subplot(333)
    plt.title("Loss p42")
    plt.plot(loss_memory_42, 'o-')
    plt.plot(np.linspace(0,len(loss_memory_42),len(loss_memory_42)+1),np.ones(len(loss_memory_42)+1)*criterion, 'k--', label='Stop Criterion')


    plt.subplot(334)
    plt.title("Loss p84")
    plt.plot(loss_memory_84, 'o-')
    plt.plot(np.linspace(0,len(loss_memory_84),len(loss_memory_84)+1),np.ones(len(loss_memory_84)+1)*criterion, 'k--', label='Stop Criterion')
    plt.show()
    plt.subplot(335)
    plt.title("tomo acq.")
    plt.imshow(datamatrix, aspect='auto')
    plt.show()
    print(f'Current loss: {loss}')
    # plt.subplot(336)
    # plt.plot(bunches[0], label='b1')
    # plt.plot(bunches[1], label='b2')
    # plt.plot(bunches[2], label='b3')
    # plt.plot(bunches[3], label='b4')

    plt.subplot(336)
    plt.ylim((-0.3,0.3))
    plt.plot(fwhms, 'go-', label='fwhms')
    plt.plot(intensities, 'bo-', label='Rel. intensity')
    plt.legend()
    fig = plt.gcf()
    axes = plt.gca()

    #plot_finish(fig=fig, axes=axes, xlabel='Setting', ylabel='Observable')
    plt.pause(0.2)


def render_voltage(setting_memory, profile, loss, loss_memory, datamatrix, bunches, fwhms, intensities, criterion = 0.0008, done=False, fig_ax = None):
        
    """
    Render walking through phase space with voltage on the side and current profile/loss alongside.
    """
    plt.clf()

    x=[]
    y=[]
    z=[]
    for phase_set in setting_memory:
        x.append(phase_set[0])
        y.append(phase_set[1])
        z.append(phase_set[2])
    
    
    if done:
        plt.suptitle("DONE! Success!!")
    plt.subplot(231)
    plt.title('Voltage factor changes')
    plt.plot(z, 'yo-')
    plt.xlabel('step')
    plt.ylabel('v14 factor')
    
    # plt.subplot(232)
    # plt.ylim((-20,20))
    # plt.xlim((-20,20))
    # plt.xlabel('p14')
    # plt.ylabel('p21') 
    # vf = setting_memory[-1][2]
    # plt.title(f'vf = {vf:4f}')
    # plt.plot(x, y, 'go-')
    # plt.scatter(x[-1],y[-1], 'rx')
    #plt.colorbar()
    #plt.clim(0, 0.02)

    plt.subplot(232)
    plt.title(f'Loss: {loss:4f}')
    plt.plot(profile)
    plt.ylim((0,1))

    plt.subplot(233)
    plt.title("Loss")
    plt.plot(loss_memory, 'o-')
    plt.plot(np.linspace(0,len(loss_memory),len(loss_memory)+1),np.ones(len(loss_memory)+1)*criterion, 'k--', label='Stop Criterion')
    plt.show()
    plt.subplot(234)
    plt.title("tomo acq.")
    plt.imshow(datamatrix, aspect='auto')
    plt.show()
    print(f'Current loss: {loss}')
    plt.subplot(235)
    plt.ylim(top=1)
    plt.plot(bunches[0], label='b1')
    plt.plot(bunches[1], label='b2')
    plt.plot(bunches[2], label='b3')
    plt.subplot(236)
    plt.ylim((-0.3,0.3))
    plt.plot(fwhms, 'go-', label='Rel. fwhms')
    plt.plot(intensities, 'bo-', label='Rel. intensity')
    plt.legend()
    #fig.canvas.draw()
    #fig.canvas.flush_events()
    axes = plt.gca()

    #plot_finish(fig=fig, axes=axes, xlabel='Setting', ylabel='Observable')
    plt.pause(0.2)
# corrected_profile = corrected_profile/np.max(corrected_profile) # normalize
# index_of_peaks = find_peaks(corrected_profile, height=0.25, distance=200)
# plt.figure()
# plt.plot(corrected_profile)
# plt.plot(index_of_peaks,peak_heights,'x')
# plt.show()

# plt.figure()
# plt.subplot(121)
# plt.title('Before correction')
# plt.plot(profile[:x1])
# plt.subplot(122)
# plt.title('After correction')
# plt.plot(corrected_profile)

   # return corrected_profile
