from unicodedata import name
import torch
import torch.nn as nn
import torch.nn.functional as F
#from pytorch_fitmodule import FitModule
from torch.autograd import Variable
import numpy as np
#from utils import count_parameters
#import torchviz
#from utils import count_parameters
#from neural_networks.dataloader import QuadsplitDataset, ToTensor, Normalize
#from dataloader import QuadsplitDataset, ToTensor, Normalize

from torchvision import transforms#, utils
from torch.utils.data import Dataset, DataLoader
import os
os.environ["PATH"] += os.pathsep + 'C:/Program Files/Graphviz/bin/'


class CNN_tri(torch.nn.Module):
    def __init__(self, num_features=3):
        super(CNN_tri, self).__init__()
        

        self.encoder = nn.Sequential(# Input is 1x150x400 
            torch.nn.Conv2d(1,64,3,stride=2), # 
            torch.nn.BatchNorm2d(64),
            torch.nn.LeakyReLU(0.2, True),
            torch.nn.Conv2d(64,32,3,2), # 
            torch.nn.BatchNorm2d(32),
            torch.nn.LeakyReLU(0.2, True),
            torch.nn.Conv2d(32,32,3,2), # 
            torch.nn.BatchNorm2d(32),
            torch.nn.LeakyReLU(0.2, True),
            torch.nn.Conv2d(32,32,3,2), # 
            torch.nn.BatchNorm2d(32),
            torch.nn.LeakyReLU(0.2, True),
            torch.nn.Conv2d(32,32,3,2), # 
            torch.nn.BatchNorm2d(32),
            torch.nn.LeakyReLU(0.2, True),
            torch.nn.Flatten(),
            torch.nn.Linear(1056,256), # When using original sim sizes
            torch.nn.LeakyReLU(0.2, True),
            torch.nn.Linear(256, num_features)
        )

    def forward(self, x):    # add additional layers here?                                       
        x = x.float()                                              
        out = self.encoder(x)
        return out

if __name__ == "__main__":
    cnn = CNN_tri()
    nbr_params = count_parameters(cnn)
    make_graph = False
    if make_graph:
        dataset = QuadsplitDataset(root_dir =r"C:\Users\jwulff\cernbox\workspaces\RL-007-RFinPS\optimising-rf-manipulations-in-the-ps-through-rl\dataset",
        transform=transforms.Compose([
            Normalize(),
            ToTensor(),
        ]))
        dataloader = DataLoader(dataset, batch_size=5, shuffle=False, num_workers=0)
        data = next(iter(dataloader))
        inputs, labels = data['image'].float(), data['labels'].float()
        outputs = cnn(inputs)
        #torchviz.make_dot(outputs, params=dict(list(cnn.named_parameters()))).render("cnn_torchviz", format="png")
    print(cnn)
    print(nbr_params)