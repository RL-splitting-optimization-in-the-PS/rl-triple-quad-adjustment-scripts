from RL_splitting_adjustment_scripts.splitting_agent_base import SplittingAgentBase

"""
Instructions to run automatic optimisation tri/quad functions:

1. Open a tomoscope on your beam.
2. Open the splitting reference of your interest (triple or quad, 72bs/48b_bcms)
3. Make sure the acquisition is centered on the splitting of one bunch.
4. Check which tomoscope is being used (57/58) and use the correct name in the SplittingAgentBase init.
5. Make sure the correct settings are given to SplittingAgentBase:
    user, splitting, tomoscope, and beam type (72b_25ns or 48b_BCMS).
6. Use the approproate function for you:
    splitting_class.start_automatic_optimisation_tri
    splitting_class.start_automatic_optimisation_quad

    with your settings of choice.
"""


if __name__ == "__main__":

    ########## General settings ##########

    user = 'CPS.USER.MD4'
    tomoscope = 'PR.SCOPE58.CH01/Acquisition'
    beam_type = '72b_25ns' # or 48b_BCMS

    ###############################################
    
    ########## Triple split optimisation ##########


    # splitting_class = SplittingAgentBase(user=user, 
    #                                     splitting='tri', 
    #                                     tomoscope=tomoscope, 
    #                                     beam_type=beam_type)

    # splitting_class.start_automatic_optimisation_tri(initial_feat_guess=True, 
    #                                     max_steps=30, 
    #                                     render_opt=True,
    #                                     phase_criterion=0.0008,
    #                                     volt_criterion = 0.0005,
    #                                     )#save_dir = '/eos/home-j/jwulff/workspaces/RL_splitting_adjustment_scripts/local_saved_episodes')
    
    ###############################################
    
    ########## Quad split optimisation ############

    # splitting_class = SplittingAgentBase(user=user, 
    #                                     splitting='quad', 
    #                                     tomoscope=tomoscope, 
    #                                     beam_type=beam_type)

    # splitting_class.start_automatic_optimisation_quad(max_steps=30, 
    #                                     render_opt=True,
    #                                     phase_42_criterion=0.0008,
    #                                     phase_84_criterion = 0.0008,
    #                                     save_dir = '/eos/home-j/jwulff/workspaces/RL_splitting_adjustment_scripts/local_saved_episodes')

        
###############################################
    
    ########## Tri split optimisation ############

    splitting_class = SplittingAgentBase(user=user, 
                                        splitting='tri', 
                                        tomoscope=tomoscope, 
                                        beam_type=beam_type)

    splitting_class.start_automatic_optimisation_tri_all_params_agent(
                                        max_steps=30, 
                                        render_opt=True,
                                        phase_volt_criterion=0.0008,
                                        save_dir = '/eos/home-j/jwulff/workspaces/RL_splitting_adjustment_scripts/local_saved_episodes/testing_all_params_agent')