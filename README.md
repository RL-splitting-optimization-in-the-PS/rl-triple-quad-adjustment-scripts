# RL-splitting-adjustment-scripts

Gitlab repository for a collection of scripts/saved models to use in automatic triple/quad splitting adjustments. Currently requires tomoscope (or tomoscope-like) acquisitions to run.

## Purpose of this project

Provide an installable package that can then be imported into a script and run through simple use of built-in functions to deploy automatic
optimising RL-agents for triple/quad splittings in the PS (for nominal 72b and 48b BCMS beams).

## Getting started

Clone gitlab repository to your local machine.

Setup
-----

Creating new venv.
Activate acc-py Base in your terminal.

```bash
# Activate Acc-Py Base.
# This is the "base" distribution containing only the bare minimum of
# pre-installed packages.
source /acc/local/share/python/acc-py/base/pro/setup.sh

```

Create a new venv from an acc-py base and activate it.
```bash
acc-py venv /path/to/your/new/venv
source /path/to/your/new/venv/bin/activate
which python  # Where does our Python interpreter come from?
/path/to/your/new/venv/bin/python
# deactivate  # Leave the venv again.
```

CD into the repository of this repo, and run 

```bash
pip install --editable .
```

to install the requirements from the setup.py file in your ne virtual environment. Using the --editable tag may be a good idea, as the code is a work in progress and you may need to make changes.

Use inside the Tomoscope custom scripts app
-----
Instructions in how to use the optimizing scripts through the tomoscope custom scripts function in the following codimd: https://codimd.web.cern.ch/s/rtMQQ-IVV


Run an episode
-----

There is an example script importing and initialising the package and agent before running the function starting the optimisation in main.py. It can be used to try to run an episode given that you have opened a tomoscope and are taking acquisitions similar to reference quad/triple splittings.

Original files
-----
The files 

```
vpc_script_agent_automated_rel.py
vpc_script_agent_automated_rel_quad.py
vpc_script_agent_automated_rel_BCMS.py
```
are the old, original scripts I used to run the models from my personal working repository. They will not run now due to errors in paths. The code to be used in this repo is mainly located in the ```splitting_agent_base.py``` file and class.


Using the GUI
-----

There is currently a prototype GUI available in the testing-gui folder. By running the UI.py file you will start a GUI, where different settings can be adjusted.
After inputing your desired settings, an automatic optimisation can be started by pressing the 'start automatic optimisation' button.


Improvements that can be made
-----

Some simple ideas for improvements of the current usage of the tomscope custom scripts splittin optimisation is as follows:

- **Add a check for whether beam is present or not (for example checking DCBCT).** Currently, the code will crash if the tomoscope triggers data without correct format (like no beam).
- **Add an improved way of determining the currently active tomoscope.** Currently, both scope 57 and 58 are queried for data and then a check is made to see which of the two has the correct shape, e.g. which tomoscope is looking at a triple splitting reference. However, if both scopes are viewing the same shape of data, there is no way for the script to know which one to use. Was discussed whether this could be included in the input of the tomoscope custom scripts, but TBD.
- Integrate the functionality into some other, more generalized optimisation application for ease of operator use!


##
