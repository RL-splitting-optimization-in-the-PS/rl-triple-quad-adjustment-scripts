import pyjapc
import matplotlib.pyplot as plt
import numpy as np
import sys
sys.path.append(r'C:\Users\jwulff\cernbox\workspaces\RL-007-RFinPS\optimising-rf-manipulations-in-the-ps-through-rl')
import torch
import torch.nn as nn
import torch.nn.functional as F
#from pytorch_fitmodule import FitModule
from torch.autograd import Variable
import numpy as np
from neural_networks.dataloader import QuadsplitDataset, ToTensor, Normalize
from torchvision import transforms, utils
from torch.utils.data import Dataset, DataLoader
from neural_networks.CNN import CNN
from neural_networks.CNN_tri import CNN_tri
from neural_networks.CNN_tri_new import CNN_tri_new
import torch.optim as optim
import matplotlib.pyplot as plt
from vpc_functions import load_model, isolate_bunches_from_dm_profile_tri, loss_function_tri, process_tomoscope_acquisition, render, tri_phase_loss#, correct_dm_tri #, FWHM
from vpc_ajdust_phases import offset_GSRPB, offset_GSRPC, update_voltage_program_GSVMOD7
import time
from stable_baselines3 import SAC
import os
from pathlib import Path

japc = pyjapc.PyJapc(selector='CPS.USER.MD4')

#print(japc.getUsers('CPS'))
devices = []
#devices.append('PR.BCT/Acquisition')  # DC BCT
#devices.append('PR.SCOPE58.CH01/Acquisition')  # Tomoscope 57-58
devices.append('PR.SCOPE57.CH01/Acquisition')  # Tomoscope 57-58
# devices.append('PR.SCOPE97.CH01/Acquisition')  # WCM03 OASIS
#devices.append('PR.SCOPE59.CH01/Acquisition')  # BSM 59-87
# devices.append('PR.SCOPE60.CH01/Acquisition')  # Last turn logging 60
# devices.append('PR.BQL72/ContinuousAcquisition')  # BBQ
# devices.append('PR.SCOPE31.CH01/Acquisition')  # Wideband transverse
# devices.append('PR.SCOPE31.CH02/Acquisition')  # Wideband transverse
# devices.append('PR.SCOPE31.CH03/Acquisition')  # Wideband transverse
# devices+=['F16.BCT126/Acquisition',
#           'F16.BCT203/Acquisition',
#           'F16.BCT212/Acquisition',
#           'F16.BCT372/Acquisition']  # TT2 BCTs
# devices+=['PA.VD20-80-SA/Samples',
#           'PA.VD20-92-SA/Samples',
#           'PA.VD40-77-SA/Samples',
#           'PA.VD40-78-SA/Samples',
#           'PA.VD80-08-SA/Samples',
#           'PA.VD80-88-SA/Samples',
#           'PA.VD80-89-SA/Samples']  # Samplers
# devices+=['PA.VDC11-SA/Samples',
#           'PA.VDC36-SA/Samples',
#           'PA.VDC46-SA/Samples',
#           'PA.VDC51-SA/Samples',
#           'PA.VDC56-SA/Samples',
#           'PA.VDC66-SA/Samples',
#           'PA.VDC76-SA/Samples',
#           'PA.VDC81-SA/Samples',
#           'PA.VDC86-SA/Samples',
#           'PA.VDC81-SA/Samples',
#           'PA.VDC96-SA/Samples']  # Samplers

parent_path = str(Path(__file__).parent.parent) # Save parent dir of this script to allow loading of CNN/RL models.

##########
# Load REFERENCE VOLTAGE USED IN SIMULATION
##########
ref_voltage_h14 = np.load(parent_path + '/v14_settings_max_step_01.npy') # Load v14 max step. Calculated as 0.1*reference setting of the six points to change in v14 program.
max_step_volt = 0.1 # Hardcoded through training of agent.
voltage_max_step = ref_voltage_h14
# plt.figure()
# plt.plot(time_h14,voltage_h14)
# plt.show()
###############################################################
# LOAD FEATURE EXTRACTOR(s)
###############################################################

model = CNN_tri() # NEW move_injection version (35k dataset)
model2 = CNN_tri() # New MoveInj version (59k dataset)
model3 = CNN_tri_new(trim_edges=True) # 

#optimizer = optim.Adam(model.parameters(), lr=1e-3)


feat_model_name3 = 'cnn_tri_Trim_Move_Injection_PV_59521_ref_voltageJul_11_2022_09-22-53_e43' # Trim edges version

print(f'Fetching model {feat_model_name3}...')
path = r"/eos/home-j/jwulff/workspaces/RL-007-RFinPS/optimising-rf-manipulations-in-the-ps-through-rl/saved_models/{}.pth".format(feat_model_name3)
#path = r"C:\Users\jwulff\cernbox\workspaces\RL-007-RFinPS\optimising-rf-manipulations-in-the-ps-through-rl\saved_models\{}.pth".format(model_name)
model3 = load_model(path,model)
#optimizer.load_state_dict(checkpoint['optimizer_state_dict'])
print(f'Weights loaded...')


model.eval() ### IMPORTANT! If not in eval mode, batchnorm layers will not work correctly.

h14_phase_setting_init = japc.getParam('PA.GSRPC/Amplitude#amplitude')
h21_phase_setting_init = japc.getParam('PA.GSRPB/Amplitude#amplitude')

# GSVMOD7 (v14)
V14_INIT = japc.getParam('PA.GSV10MOD7/Setting#amplitudes') # ??
NEW_v14 = V14_INIT.copy()
#set voltage to reference:

init_voltage_settings = NEW_v14[3][1]

#japc.setParam('PA.GSV10MOD7/Setting#amplitudes', NEW_v14, checkDims=False)
print('Not setting voltage to reference, keeping as it was (should be optimal-ish!!)')
# Set voltage max step size  as 0.01*reference
# This is then added/subtracted from the current voltage setting to take steps.
max_step_volt = 0.1

v14 = 1.0
p14 = 10
p21 = -5

def checkIfNewDataReceivedCallback(parameterName, newValue): # Checks when tomoscope receives new information
    global new_values_received
    new_values_received=True
    print(f"New value for {parameterName}")

japc.subscribeParam(devices[0], checkIfNewDataReceivedCallback)
japc.startSubscriptions()

#v14 =-1 # -1 v14 means unknown voltage. For saving purposes.



# Load agent
#SAC-BLIntCnn-Simple-profile-59521-ref-NoiseMoveInjPV-diff-00008
#SAC-CNN-only-step001-Simple-profile-VPC-rew-59521-ref-NoiseMoveInjPV-diff-0001
# SAC-RelBLIntPhase-phase-loss-profile-step10-59521-ref-NoiseMoveInjPV-diff-0001
# SAC-RelBLIntPhase-phase-loss-profile-step10-59521-ref-NoiseMoveInjPV-diff-00012
# SAC-RelBLIntPhase-phase-loss-profile-step10-59521-ref-NoiseMoveInjPV-diff-00009
# SAC-RelBLIntCnnPhase-simple-profile-step10-59521-ref-NoiseMoveInjPV-diff-00015

model_name = r"SAC-RelBLIntPhase-phase-loss-profile-step10-59521-ref-NoiseMoveInjPV-diff-00009"
agent = SAC.load("./RL_logs/tri/{}".format(model_name))

volt_model_name = r"SAC-RelBLIntVolt-voltloss-step01-profile-59521-ref-NoiseMoveInjPV-diff-00010"
volt_model = SAC.load("./RL_logs/tri/{}".format(volt_model_name))
# volt_model_name = r"SAC-RelBLIntVolt-voltloss-profile-59521-ref-NoiseMoveInjPV-diff-00010"
# volt_model = SAC.load("./RL_logs/tri/{}".format(volt_model_name))


SAVE_DIR = r"/eos/home-j/jwulff/workspaces/RL-007-RFinPS/optimising-rf-manipulations-in-the-ps-through-rl/MD_results_11_11_2022/tri/{}/".format(model_name)

# Create saving directory
if not os.path.exists(SAVE_DIR):
    os.makedirs(SAVE_DIR)

# Init settings

# For now it is required to initially optimize settings manually before starting scan. Periodically one may need to check that the optimum setting has not drifted!
### Triple splitting

starting_points = [
    [20,20,0.05],
    [-20,-20,0.05],
    [10,-10, -0.05],
    [-10,10,0.05],
    [15,5,0.10],
    [5,-15,-0.10]
]
episode = 1
for settings in starting_points:
    #init episode and step counter
    

    step = 0
    phase_set_memory = []
    action_memory = []
    phase_set_memory = [] # Doubles as labels for dm:s in dm_memory. May need to convert them to resemble labels in original dataset.
    dm_memory = []
    profile_criterion_memory = []
    profile_memory = []
    fwhms_memory = []
    intensities_memory = []


    ### Trying to save things to make future online RL possible.
    phase_obs_memory = []
    phase_agent_action_memory = []
    phase_agent_reward_memory = [] # Simply using -loss for now

    volt_obs_memory = []
    volt_agent_action_memory = []
    volt_agent_reward_memory = []

    phase_volt_set = [0,0,1.0] # init setting at at 0,0,0 phase/volt set!
    START_OFFSET = settings#[40,-15, -0.05]
    h14_offset = START_OFFSET[0] # 
    h21_offset = START_OFFSET[1] #
    voltage_step_offset = START_OFFSET[2]
    voltage_factor = 1.0 + voltage_step_offset
    offset_GSRPC(japc, phase_offset=round(h14_offset,4))
    offset_GSRPB(japc, phase_offset=round(h21_offset,4))
    voltage_offset = init_voltage_settings*voltage_step_offset
    update_voltage_program_GSVMOD7(japc, voltage_offset)
    phase_volt_set = phase_volt_set + START_OFFSET
    phase_set_memory.append(phase_volt_set)
    skipped_settings = []


    #### CHOOSE IF YOU WANT INITIAL GUESS FROM FEAT EXTRACTOR ####

    initial_feat_guess = True

    new_h14 = japc.getParam('PA.GSRPC/Amplitude#amplitude')
    new_h21 = japc.getParam('PA.GSRPB/Amplitude#amplitude')
    new_v14f = 0
    # BEFORE BEGINNING EPISODE, move to desired starting offset

    current_offset = START_OFFSET

    rel_phase_volt_set = [[0,0,0]]
    plt.figure('Phase optimisation')
    for step in range(20):
        print(f"Beginning step {step}...")

        h14_setting = new_h14 #japc.getParam('PA.DPC20/Setting#value')
        h21_setting = new_h21 #japc.getParam('PA.DPC40/Setting#value')

        # Wait for next acquisition
        # Save acquisition with phase setting
        #print('Set p14,p21,v14 now to what you want and store in p14,p21,v14 variables!')
        #print(f'Current settings: p14: {p14}, p21 {p21}, v14 {v14}.')

        new_values_received = False
        # Wait for next acquisition

        while not new_values_received:
            time.sleep(1)
        # Extract values of acquisition
        data_dictionary = japc.getParam(devices[0])
        values = data_dictionary['value'] # Matrix if using tomoscope

        values_centered_norm = process_tomoscope_acquisition(values, mode='tri', remove_tails=True, correct_dm=True)
        values_centered_norm_trimmed = values_centered_norm[:,40:360]
        values_centered_norm_tails = process_tomoscope_acquisition(values, mode='tri', remove_tails=False)
        profile = values_centered_norm[-1,:]
        profile_memory.append([profile])
        #corr = correct_profile_tri(profile)
        #################
        # CALCULATE LOSS
        #################
        bunches=[]
        bunches, fwhms, intensities = isolate_bunches_from_dm_profile_tri(profile, intensities=True, rel=True)
        # Norm intensities: Try only relative intensities/fwhms
        intensities = intensities / np.max(intensities) # Same as in sim. #TODO
        intensities = intensities - np.mean(intensities)

        fwhms = fwhms - np.mean(fwhms)

        fwhms_memory.append([fwhms])
        intensities_memory.append([intensities])

        b1 = bunches[0]
        b2 = bunches[1]
        b3 = bunches[2]
        #loss = loss_function_tri(b1,b2,b3)
        loss = tri_phase_loss(b1, b3)
        profile_criterion_memory.append(loss)

        #values = cv2.resize(values, dsize=(400,150))
        #np.save('/eos/home-j/jwulff/workspaces/RL-007-RFinPS/optimising-rf-manipulations-in-the-ps-through-rl/dataset/tri/new_live_data/p14_{}_p21_{}_v14_{}_datamatrix'.format(int(h14_offset*100), int(h21_offset*100), int(voltage_factor*1000)), values_centered_norm)

        sample = {'image': values_centered_norm, 'labels': np.ndarray([0])}
        sample_trim = {'image': values_centered_norm_trimmed, 'labels': np.ndarray([0])}
        sample_w_tails = {'image': values_centered_norm_tails, 'labels': np.ndarray([0])}
        transform = transforms.Compose([
            #Normalize(),
            ToTensor(),
        ])



        inputs_dict = transform(sample) # Convert to tensor
        inputs_dict_trim = transform(sample_trim) # Convert to tensor
        inputs_dict_tails = transform(sample_w_tails) # Convert to tensor

        image = inputs_dict['image'].unsqueeze(dim=0)
        transformed_profile = image[0,0,-1,:].numpy()
        image_trim = inputs_dict_trim['image'].unsqueeze(dim=0)
        transformed_trim_profile = image_trim[0,0,-1,:].numpy()
        image_tails = inputs_dict_tails['image'].unsqueeze(dim=0)
        transformed_profile_tails = image_tails[0,0,-1,:].numpy()

        criterion = 0.0008

        # print(f'Current loss: {loss}')
        # plt.figure()
        # plt.plot(b1, label='b1')
        # plt.plot(b2, label='b2')
        # plt.plot(b3, label='b3')
        # plt.figure()
        # plt.title(f'Loss: {loss}')
        # plt.plot(transformed_profile)
        # plt.figure()
        # plt.imshow(values_centered_norm, aspect='auto')
        # plt.show()
        # plt.figure()
        # plt.title("Loss")
        # plt.plot(profile_criterion_memory, 'o-')
        # plt.plot(np.linspace(0,len(profile_criterion_memory),len(profile_criterion_memory)+1),np.ones(len(profile_criterion_memory)+1)*criterion, 'k--', label='Stop Criterion')
        # plt.show()
        plt.figure('Phase optimisation')
        render((-20,20), (-20,20), rel_phase_volt_set, profile, loss, profile_criterion_memory, values_centered_norm, bunches, fwhms, intensities, criterion=criterion, step=step)

        outputs3 = model3(image_trim).detach().numpy()/100
        outputs3[0][2] = outputs3[0][2]/10


        print(f'True offset: {[h14_offset, h21_offset]}')
        print(f' Feat extractor REF trimmed: {outputs3}')
        #Outputs are estimated offset from 0,0 sim setting in dpc20,dpc40
        #new_h14 = h14_phase_setting-outputs[0].astype(np.float32)
        #new_h21 = h21_phase_setting-outputs[1].astype(np.float32)
        # Send outputs of CNN and BLs to Agent to predict action

        initial_guess = outputs3[0].copy()
        initial_guess[2] = 0

        # Construct observation: [bls, CNN_output] or only [bls]
        max_phase_setting = 20
        max_volt = 1.05
        
        # outputs2[:2] = outputs2[:2]/max_phase_setting
        # outputs2[2] = (outputs2[2]-1)/(max_volt-1)
        outputs3 = outputs3[0]
        outputs3[:2] = outputs3[:2]/max_phase_setting
        outputs3[2] = (outputs3[2]-1)/(max_volt-1)

        
        #outputs = np.array([0.01,-0.50])
        bls_and_intensities = np.append(fwhms, intensities)
        obs = bls_and_intensities
        # If loss is below criterion, end episode
        if loss < criterion and step > 1:
            print(f'Phase criterion reached in {step} steps, ending episode early. Optimisation finished...')
            plt.figure('Phase optimisation')
            render((-20,20), (-20,20), rel_phase_volt_set, profile, loss, profile_criterion_memory, 
                    values_centered_norm, 
                    bunches,
                    fwhms, 
                    intensities, 
                    criterion=criterion, 
                    done=True, 
                    step=step)
            dm_memory.append(values_centered_norm)
            plt.savefig('{}Ep{}_optimisation.png'.format(SAVE_DIR, episode))
            break

        max_step_volt = 0.1
        max_step_size = 10
        obs = [obs[0], obs[2], obs[3], obs[5]] # bl1,bl3,int1,int3
        obs_2 = [obs_2[0], obs_2[2], obs_2[3], obs_2[5], obs_2[6], obs_2[7]] # bl1,bl3,int1,int3,p14,p21
        # Predict
        action, _states = agent.predict(obs, deterministic=True)#, deterministic=True)


        #action = action*10 # Convert action into degrees by max step size, currently 10
        converted_action = np.array([0.0,0.0,0.0], dtype=object)
        converted_action[0] = action[0]*max_step_size
        converted_action[1] = action[1]*max_step_size

        #converted_action[2] = action[2]*voltage_max_step # max_step_volt
        
        #corr = np.append(corr, 0)#action[2])
        

        if step == 0:
            dm_memory.append(values_centered_norm)
            print(f'Step 0, no action taken')
            action_memory.append([0,0])
            continue
        

        # Take step
        if initial_feat_guess:
            print(f'Agent initial step, not taken: {converted_action}')
            converted_action = -initial_guess
            print(f'Taking initial guess from feat extractor instead: Moving {converted_action}')
            initial_feat_guess=False
        # Track correction
        corr=converted_action[:3]
        h14_offset += converted_action[0]
        h21_offset += converted_action[1]
        

        rel_phase_volt_set.append(rel_phase_volt_set[-1]+corr)

        new_h14 = h14_setting+converted_action[0].astype(np.float32)
        new_h21 = h21_setting+converted_action[1].astype(np.float32)

        # Calculate manual input for splitting adjustment.
        h14_input = converted_action[0]
        h21_input = converted_action[1]
        v14_input = converted_action[2]
        print(f'Current phase set: {h14_setting}, {h21_setting}')
        print(f'Taking Action: {converted_action})')
        print(f'Model 2 suggested action: {converted_action2}')
        print(f'Profile-based loss: {loss}')
        offset_GSRPC(japc, phase_offset=h14_input)
        offset_GSRPB(japc, phase_offset=h21_input)

        #voltage_change = converted_action[2]#(v14_input/0.01)*voltage_max_step
        #update_voltage_program_GSVMOD7(japc, voltage_change)
        
        # h14
        GSRPC = japc.getParam('PA.DPC20/Setting#value')

        # h21
        GSRPB = japc.getParam('PA.MHFBC40-77/MhsChannelCTRLPPM#phaseCycle')
        #DPC_40_78 = japc.getParam('PA.MHFBC40-78/MhsChannelCTRLPPM#phaseCycle')

        print(f'Settings after assigning relative offset {converted_action[0]},{converted_action[1]}')#: h14 {GSRPC}, h21: {GSRPB}')
        new_values_received = False
        # Set phases
        #print(f"Taking action {action}, new settings: dpc20 {new_dpc20}, dpc40 {new_dpc40}")
        #japc.setParam('PA.DPC20/Setting#value', new_dpc20)
        #japc.setParam('PA.DPC40/Setting#value', new_dpc40)
        action_memory.append([converted_action])
        dm_memory.append(values_centered_norm)
        
    # Check params have been set
    #assert(japc.getParam('PA.DPC20/Setting#value')==new_dpc20)
    #assert(japc.getParam('PA.DPC40/Setting#value')==new_dpc40)

    # Voltage factor 1.0 means v14 flat top at 16422.83058167




    #########################
    # VOLTAGE OPTIMISATION  #
    #########################
    if loss > criterion:
        print(f'Phase criterion NOT reached within {step} steps, not performing voltage optimisation.')
        plt.figure('Phase optimisation')
        render((-20,20), (-20,20), rel_phase_volt_set, profile, loss, profile_criterion_memory, values_centered_norm, bunches, fwhms, intensities, criterion=criterion, done=False)
    else:
        volt_done = False
        curr_step = 0
        profile_mem = []
        volt_loss_memory = []
        volt_criterion = 0.0005
        # Wait for next acquisition
        plt.figure('Voltage optimisation')
        while not volt_done:
            print(f'Beginning volt step {curr_step}')
            volt_loss_memory.append([])

            new_values_received = False
            while not new_values_received:
                time.sleep(1)
            # Extract values of acquisition
            data_dictionary = japc.getParam(devices[0])
            values = data_dictionary['value'] # Matrix if using tomoscope

            values_centered_norm = process_tomoscope_acquisition(values, mode='tri', remove_tails=True, correct_dm=True)
            values_centered_norm_trimmed = values_centered_norm[:,40:360]
            values_centered_norm_tails = process_tomoscope_acquisition(values, mode='tri', remove_tails=False)
            profile = values_centered_norm[-1,:].copy()
            profile_memory.append([profile])
            #corr = correct_profile_tri(profile)
            #################
            # CALCULATE LOSS
            #################
            bunches=[]
            bunches, fwhms, intensities = isolate_bunches_from_dm_profile_tri(profile, intensities=True, rel=True)
            # Norm intensities: Try only relative intensities/fwhms
            intensities = intensities / np.max(intensities) # Same as in sim. #TODO
            intensities = intensities - np.mean(intensities)

            fwhms = fwhms - np.mean(fwhms)
            observation = np.append(fwhms, intensities)

            fwhms_memory.append([fwhms])
            intensities_memory.append([intensities])

            b1 = bunches[0]
            b2 = bunches[1]
            b3 = bunches[2]
            loss = loss_function_tri(b1,b2,b3)
            phase_loss = tri_phase_loss(b1, b3)
            volt_loss_memory[curr_step] = loss
            #render((-20,20), (-20,20), rel_phase_volt_set, profile, loss, volt_loss_memory, values_centered_norm, bunches, fwhms, intensities, criterion=volt_criterion, done=False)
            plt.figure('Voltage optimisation')
            render((-20,20), (-20,20), rel_phase_volt_set, profile, loss, volt_loss_memory, values_centered_norm, bunches, fwhms, intensities, criterion=volt_criterion, done=False, step=curr_step)
            if phase_loss > criterion*1.2:
                print(f'phase deteriorated to loss of 1.2*criterion, re-optimising')

                while phase_loss > criterion:
                    bls_and_intensities = np.append(fwhms, intensities)
                    phase_obs = [bls_and_intensities[0], bls_and_intensities[2], bls_and_intensities[3], bls_and_intensities[5]]
                    action, _states = agent.predict(phase_obs, deterministic=True)#, deterministic=True)
                    
                    #action = action*10 # Convert action into degrees by max step size, currently 10
                    converted_action = np.array([0.0,0.0,0.0], dtype=object)
                    converted_action[0] = action[0]*max_step_size
                    converted_action[1] = action[1]*max_step_size

                    corr=converted_action[:3]
                    h14_offset += converted_action[0]
                    h21_offset += converted_action[1]
                    

                    rel_phase_volt_set.append(rel_phase_volt_set[-1]+corr)

                    new_h14 = h14_setting+converted_action[0].astype(np.float32)
                    new_h21 = h21_setting+converted_action[1].astype(np.float32)

                    # Calculate manual input for splitting adjustment.
                    h14_input = converted_action[0]
                    h21_input = converted_action[1]
                    v14_input = converted_action[2]
                    print(f'Current phase set: {h14_setting}, {h21_setting}')
                    print(f'Taking Action: {converted_action})')
                    print(f'Model 2 suggested action: {converted_action2}')
                    print(f'Profile-based loss: {loss}')
                    offset_GSRPC(japc, phase_offset=h14_input)
                    offset_GSRPB(japc, phase_offset=h21_input)
                    step += 1

                    # h14
                    GSRPC = japc.getParam('PA.DPC20/Setting#value')
                    # h21
                    GSRPB = japc.getParam('PA.MHFBC40-77/MhsChannelCTRLPPM#phaseCycle')

                    print(f'Settings after assigning relative offset {converted_action[0]},{converted_action[1]}')#: h14 {GSRPC}, h21: {GSRPB}')
                    new_values_received = False
                    action_memory.append([converted_action])
                    dm_memory.append(values_centered_norm)
                    while not new_values_received:
                        time.sleep(1)
                    # Extract values of acquisition
                    data_dictionary = japc.getParam(devices[0])
                    values = data_dictionary['value'] # Matrix if using tomoscope

                    values_centered_norm = process_tomoscope_acquisition(values, mode='tri', remove_tails=True, correct_dm=True)
                    values_centered_norm_trimmed = values_centered_norm[:,40:360]
                    values_centered_norm_tails = process_tomoscope_acquisition(values, mode='tri', remove_tails=False)
                    profile = values_centered_norm[-1,:].copy()
                    profile_memory.append([profile])
                    #corr = correct_profile_tri(profile)
                    #################
                    # CALCULATE LOSS
                    #################
                    bunches=[]
                    bunches, fwhms, intensities = isolate_bunches_from_dm_profile_tri(profile, intensities=True, rel=True)
                    # Norm intensities: Try only relative intensities/fwhms
                    intensities = intensities / np.max(intensities) # Same as in sim. #TODO
                    intensities = intensities - np.mean(intensities)

                    fwhms = fwhms - np.mean(fwhms)
                    observation = np.append(fwhms, intensities)

                    fwhms_memory.append([fwhms])
                    intensities_memory.append([intensities])

                    b1 = bunches[0]
                    b2 = bunches[1]
                    b3 = bunches[2]
                    loss = loss_function_tri(b1,b2,b3)
                    phase_loss = tri_phase_loss(b1, b3)
                    profile_criterion_memory.append(phase_loss)
                    plt.figure('Phase optimisation')
                    render((-20,20), (-20,20), rel_phase_volt_set, profile, phase_loss, profile_criterion_memory, values_centered_norm, bunches, fwhms, intensities, criterion=criterion, step=step)
            print(f'phase loss: {phase_loss}, still ok.')
            if loss < volt_criterion:
                print(f'Voltage optimised. Took {curr_step} steps to optimise.')
                tot = step + curr_step
                print(f'Total number of steps for phase and volt: {tot}')
                plt.figure('Voltage optimisation')
                render((-20,20), (-20,20), rel_phase_volt_set, profile, loss, volt_loss_memory, values_centered_norm, bunches, fwhms, intensities, criterion=volt_criterion, done=True, step=curr_step)
                volt_done=True
            #values = cv2.resize(values, dsize=(400,150))
            else:
                action, _ = volt_model.predict(observation, deterministic=True)
                volt_step = action * voltage_max_step # volt max step
                print(f'Taking volt step: {action*max_step_volt})')
                converted_action = np.array([0.0,0.0,0.0], dtype=object)
                converted_action[2] = volt_step

                voltage_factor += action[0]*max_step_volt
                np.save('/eos/home-j/jwulff/workspaces/RL-007-RFinPS/optimising-rf-manipulations-in-the-ps-through-rl/dataset/tri/new_live_data/p14_{}_p21_{}_v14_{}_datamatrix'.format(int(h14_offset*100), int(h21_offset*100), int(voltage_factor*1000)), values_centered_norm)

                new_phase = rel_phase_volt_set[-1].copy()
                new_phase[2] = new_phase[2]+action[0]*max_step_volt
                rel_phase_volt_set.append(new_phase)
                converted_action = np.array([0.0,0.0,0.0], dtype=object)
                update_voltage_program_GSVMOD7(japc, volt_step)
            curr_step += 1
            action_memory.append([converted_action])
            dm_memory.append(values_centered_norm)
    # Convert to data that can be put into the feature extractor
    plt.figure('Voltage optimisation')
    plt.savefig('{}Ep{}_volt_optimisation.png'.format(SAVE_DIR, episode))

    # RESET VOLTAGE
    japc.setParam('PA.GSV10MOD7/Setting#amplitudes', V14_INIT, checkDims=False) # ??
    japc.setParam('PA.GSRPC/Amplitude#amplitude', h14_phase_setting_init)
    japc.setParam('PA.GSRPB/Amplitude#amplitude', h21_phase_setting_init)
    # Saving metadata


    np.save('{}Ep{}_phase_set_memory'.format(SAVE_DIR, episode), phase_set_memory)
    #np.save('{}Ep{}_action_memory'.format(SAVE_DIR, episode), action_memory)
    np.save('{}Ep{}_dm_memory'.format(SAVE_DIR, episode), dm_memory)
    np.save('{}Ep{}_profile_memory'.format(SAVE_DIR, episode), profile_memory)
    np.save('{}Ep{}_profile_criterion_memory'.format(SAVE_DIR, episode), profile_criterion_memory)
    np.save('{}Ep{}_volt_criterion_memory'.format(SAVE_DIR, episode), volt_loss_memory)
    np.save('{}Ep{}_rel_phase_volt_set'.format(SAVE_DIR, episode), rel_phase_volt_set)
    np.save('{}Ep{}_fwhms_memory'.format(SAVE_DIR, episode), fwhms_memory)
    np.save('{}Ep{}_intensities_memory'.format(SAVE_DIR, episode), intensities_memory)



#Plot some of the data
plt.figure(dpi=200)
plt.title('Profile based criterion')
plt.plot(profile_criterion_memory,'bo-')
plt.plot(np.linspace(0,len(profile_criterion_memory),len(profile_criterion_memory)+1),np.ones(len(profile_criterion_memory)+1)*criterion, 'k--', label='Stop Criterion')

plt.xlabel('Step')
plt.ylabel('Agent criterion [arb. units]')
plt.savefig('{}Ep{}_profile_based_criterion.png'.format(SAVE_DIR, episode))
plt.figure(dpi=250)
plt.title('First five steps')
plt.subplot(251)
plt.imshow(dm_memory[1])
plt.subplot(252)
plt.imshow(dm_memory[2])
plt.subplot(253)
plt.imshow(dm_memory[3])
plt.subplot(254)
plt.imshow(dm_memory[4])
plt.subplot(255)
plt.imshow(dm_memory[5])
plt.subplot(256)
plt.plot(dm_memory[1][-1,:])
plt.subplot(257)
plt.plot(dm_memory[2][-1,:])
plt.subplot(258)
plt.plot(dm_memory[3][-1,:])
plt.subplot(259)
plt.plot(dm_memory[4][-1,:])
plt.subplot(2,5,10)
plt.plot(dm_memory[5][-1,:])
plt.show()
plt.savefig('{}Ep{}_first_five_steps.png'.format(SAVE_DIR, episode))

#Plot some of the data

plt.figure(dpi=200)
plt.title('Initial and final state')
plt.subplot(221)
plt.imshow(dm_memory[0])
plt.subplot(222)
plt.imshow(dm_memory[-1])
plt.subplot(223)
plt.plot(dm_memory[0][-1,:])
plt.subplot(224)
plt.plot(dm_memory[-1][-1,:])
plt.show()
plt.savefig('{}Ep{}_init_final_state.png'.format(SAVE_DIR, episode))