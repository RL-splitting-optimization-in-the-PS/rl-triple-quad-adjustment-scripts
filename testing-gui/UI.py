from PyQt5 import QtWidgets, uic

import numpy as np
import logging
import sys
from accwidgets.app_frame import ApplicationFrame
from accwidgets.qt import exec_app_interruptable
from RL_splitting_adjustment_scripts.splitting_agent_base_logging import SplittingAgentBase

# class Ui(ApplicationFrame):
#     def __init__(self):
#         super(Ui, self).__init__() # Call the inherited classes __init__ method
#         uic.loadUi('/eos/home-j/jwulff/workspaces/RL-007-RFinPS/optimising-rf-manipulations-in-the-ps-through-rl/testing-gui/agent_gui_test.ui', self) # Load the .ui file
#         self.show() # Show the GUI

#from fbs_runtime.application_context.PyQt5 import ApplicationContext
from PyQt5.QtCore import QDateTime, Qt, QTimer
from PyQt5.QtWidgets import (QApplication, QCheckBox, QComboBox, QDateTimeEdit,
        QDial, QDialog, QGridLayout, QGroupBox, QHBoxLayout, QLabel, QLineEdit,
        QProgressBar, QPushButton, QRadioButton, QScrollBar, QSizePolicy,
        QSlider, QSpinBox, QStyleFactory, QTableWidget, QTabWidget, QTextEdit,
        QVBoxLayout, QWidget)
#from model import splittingAgentBase
import sys

from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg, NavigationToolbar2QT as NavigationToolbar
from matplotlib.figure import Figure

from RL_splitting_adjustment_scripts.vpc_functions import render


class MplCanvas(FigureCanvasQTAgg):

    def __init__(self, parent=None, width=5, height=4, dpi=100):
        fig = Figure(figsize=(width, height), dpi=dpi)
        self.axes = fig.add_subplot(111)
        super(MplCanvas, self).__init__(fig)

logging.getLogger().setLevel(logging.INFO)
class AgentGui(ApplicationFrame):
    def __init__(self, model, timing_bar = True, use_log_console=True, parent=None):
        super(AgentGui, self).__init__(parent, use_timing_bar = timing_bar, use_log_console=use_log_console)
        self.timing_bar._set_domain(2)
        self.timing_bar._set_highlighted_user('MD1')
        self.setCentralWidget(mainWidget(model=model, timing_bar=self.timing_bar))
    

class mainWidget(QWidget):
    def __init__(self, model, timing_bar = None):
        super(mainWidget, self).__init__()

        self.render_data = []

        self.model = model
        self.timing_bar = timing_bar
        self.save_path = None
        self.render_plot = True # Default is true

        #self.createTopRightGroupBox()
        self.createBottomLeftGroupBox()
        #self.createBottomLeftTabWidget()

        mainLayout = QGridLayout()
        #mainLayout.addWidget(self.topLeftGroupBox, 1, 0)
        #mainLayout.addWidget(self.topRightGroupBox, 0, 1)
        mainLayout.addWidget(self.bottomLeftGroupBox, 0, 0)
        mainLayout.setRowStretch(1, 1)
        mainLayout.setRowStretch(2, 1)
        mainLayout.setColumnStretch(0, 1)
        mainLayout.setColumnStretch(1, 1)
        self.setLayout(mainLayout)

        self.setWindowTitle("RL Agent")

    def createTopRightGroupBox(self):
        self.topRightGroupBox = QGroupBox("Plotting")

        sc = MplCanvas(self, width=100, height=200, dpi=100)
        sc.axes.plot([0,1,2,3,4], [10,1,20,3,40])
        toolbar = NavigationToolbar(sc, self)

        layout = QVBoxLayout()
        layout.addWidget(toolbar)
        layout.addWidget(sc)
        layout.addStretch(1)
        self.topRightGroupBox.setLayout(layout)    

    def createBottomLeftGroupBox(self):

        self.bottomLeftGroupBox = QGroupBox("Settings")
        agentComboBox = QComboBox()
        agentComboBox.addItems(['Tri: SAC-Phase/volt-Sim2real', 'Quad: SAC-p42/84-Sim2Real'])

        agentLabel = QLabel("&Agent:")
        agentLabel.setBuddy(agentComboBox)

        renderCheckBox = QCheckBox()
        renderCheckBox.setChecked(True)
        renderCheckBox.setText("Render optimisation")
        renderCheckBox.stateChanged.connect(lambda:self._render_changed(renderCheckBox))
        

        splittingComboBox = QComboBox()
        splittingComboBox.addItems(['tri', 'quad'])
        splittingComboBox.activated[str].connect(self._on_splitting_change)

        splittingLabel = QLabel("&Splitting:")
        splittingLabel.setBuddy(splittingComboBox)
        

        userComboBox = QComboBox()
        userComboBox.addItems(['LHC2', 'MD1','MD2','MD3','MD4','MD5','MD6','MD7','MD8','MD9'])
        userComboBox.setCurrentIndex(1)
        userComboBox.activated[str].connect(self._on_user_change)

        userLabel = QLabel("&User:")
        userLabel.setBuddy(userComboBox)

        beamComboBox = QComboBox()
        beamComboBox.addItems(['72b_25ns', '48b_BCMS'])
        beamComboBox.setCurrentIndex(0)
        beamComboBox.activated[str].connect(self._on_beam_change)

        beamLabel = QLabel("&Beam:")
        beamLabel.setBuddy(beamComboBox)

        tomoComboBox = QComboBox()
        tomoComboBox.addItems(['PR.SCOPE57.CH01/Acquisition', 'PR.SCOPE58.CH01/Acquisition'])
        tomoComboBox.activated[str].connect(self._on_tomo_change)

        tomoLabel = QLabel("&Tomoscope:")
        tomoLabel.setBuddy(userComboBox)

        phaseOrP42CriterionLineEdit = QLineEdit()
        phaseOrP42CriterionLineEdit.setText('0.0008')
        phaseOrP42CriterionLineEdit.textChanged.connect(self._on_crit_change)

        phaseOrP42CriterionLabel = QLabel("&(Tri: phase, Quad: p42) criterion:")
        phaseOrP42CriterionLabel.setBuddy(phaseOrP42CriterionLineEdit)

        voltOrP84CriterionLineEdit = QLineEdit()
        voltOrP84CriterionLineEdit.setText('0.0008')
        voltOrP84CriterionLineEdit.textChanged.connect(self._on_crit_change)

        voltOrP84CriterionLabel = QLabel("&(Tri: volt, Quad: p84) criterion:")
        voltOrP84CriterionLabel.setBuddy(voltOrP84CriterionLineEdit)

        savePathLineEdit = QLineEdit()
        savePathLineEdit.setText('None')
        savePathLineEdit.textChanged.connect(self._on_save_path_change)

        savePathLabel = QLabel("&Abs. save dir.:")
        savePathLabel.setBuddy(savePathLineEdit)

        suggestButton = QPushButton('Suggest action (Agent)')
        suggestButton.clicked.connect(self._suggest_action)
        featButton = QPushButton('Use Feature extractor')
        featButton.clicked.connect(self._use_feat_extractor)
        automaticOptButton = QPushButton('Start Auto optimisation')
        automaticOptButton.clicked.connect(self.start_automatic_optimisation)

        layout = QVBoxLayout()
        layout.addWidget(splittingLabel)
        layout.addWidget(splittingComboBox)
        layout.addWidget(beamLabel)
        layout.addWidget(beamComboBox)
        # layout.addWidget(agentLabel)
        # layout.addWidget(agentComboBox)
        layout.addWidget(userLabel)
        layout.addWidget(userComboBox)
        layout.addWidget(tomoLabel)
        layout.addWidget(tomoComboBox)
        # TODO  # Add a way to adjust criterions for tri/quad splittings. If set to quad,
                # change p42/p84 criterions, if set to tri change phase/volt criterions.
                # Should automatically switch to default values when changing from splittings.
        # layout.addWidget(phaseOrP42CriterionLabel)
        # layout.addWidget(phaseOrP42CriterionLineEdit)
        # layout.addWidget(voltOrP84CriterionLabel)
        # layout.addWidget(voltOrP84CriterionLineEdit)
        layout.addWidget(savePathLabel)
        layout.addWidget(savePathLineEdit)
        layout.addWidget(suggestButton)
        layout.addWidget(featButton)
        layout.addWidget(renderCheckBox)
        layout.addWidget(automaticOptButton)
        layout.addStretch(1)
        self.bottomLeftGroupBox.setLayout(layout)

    def _suggest_action(self):
        self.model.get_and_process_acquisition()
        self.model.suggest_step()

    def _on_user_change(self, value):
        logging.info(f'Changing user to {value}')
        self.timing_bar._set_highlighted_user('CPS.USER.'+value)
        self.model.change_user('CPS.USER.'+value)
    
    def _on_beam_change(self, value):
        logging.info(f'Changing beam setting to {value}.')
        self.model.change_beam_type(value)
    
    def _on_tomo_change(self, value):
        logging.info(f'Changing tomo to {value}')
        self.tomo = value
        self.model.change_tomo(value)

    def _on_splitting_change(self, value):
        logging.info(f'Changing splitting to {value}')
        self.splitting = value
        self.model.change_splitting(value)
    
    def _render_changed(self, checkbox):
        if checkbox.isChecked():
            self.render_plot = True
            logging.info(f'Render opt plot {self.render_plot}')
        elif not checkbox.isChecked():
            self.render_plot = False
            logging.info(f'Render opt plot {self.render_plot}.')

    def _on_crit_change(self, value):
        logging.info(f'Changing criterion to {value}')
        self.crit = value
        self.model.change_criterion(float(value))

    def _on_save_path_change(self, value):
        logging.info(f'Changing criterion to {value}')
        self.save_path = value

    def _use_feat_extractor(self, value):
        self.model.use_feature_extractor()


    def start_automatic_optimisation(self):
        if self.model.splitting == 'quad':
            logging.info(f'Running auto opt. for quad...')
            self.model.start_automatic_optimisation_quad(max_steps=30, 
                                render_opt=self.render_plot,
                                phase_42_criterion=0.0006,
                                phase_84_criterion = 0.0008,
                                save_dir = self.save_path)
            # self.tri_optimisation_48b_BCMS()
        if self.model.splitting == 'tri':
            logging.info(f'Running auto opt. for tri...')
            self.model.start_automatic_optimisation_tri(initial_feat_guess=True, 
                                        max_steps=30, 
                                        render_opt=self.render_plot,
                                        phase_criterion=0.0008,
                                        volt_criterion = 0.0005,
                                        save_dir = self.save_path)



if __name__ == '__main__':
    #appctxt = ApplicationContext()
    app = QtWidgets.QApplication(sys.argv) # Create an instance of QtWidgets.QApplication
    model = SplittingAgentBase()

    gallery = AgentGui(model)
    gallery.show()

    sys.exit(app.exec())

# app = QtWidgets.QApplication(sys.argv) # Create an instance of QtWidgets.QApplication
# window = Ui() # Create an instance of our class
# sys.exit(exec_app_interruptable(app)) # Start the application