"""
High-level tests for the  package.

"""

import RL_splitting_adjustment_scripts


def test_version():
    assert RL_splitting_adjustment_scripts.__version__ is not None
