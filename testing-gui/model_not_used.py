from lib2to3.pytree import convert
from random import sample
import pyjapc
from vpc_functions import load_model, isolate_bunches_from_dm_profile_tri, loss_function_tri, process_tomoscope_acquisition, render, tri_phase_loss#, correct_dm_tri #, FWHM
from vpc_ajdust_phases import offset_GSRPB, offset_GSRPB_BCMS, offset_GSRPC, offset_GSRPC_BCMS, update_voltage_program_GSVMOD7, update_voltage_program_GSVMOD7_BCMS
from stable_baselines3 import SAC
import logging
import time
import numpy as np
from torchvision import transforms, utils
import os
import matplotlib.pyplot as plt
dir_path = os.path.dirname(os.path.realpath(__file__))

"""
Class with all meaningful actions taken in gui take effect.
"""



class splittingAgentBase():

    def __init__(self, user='CPS.USER.MD1', 
                splitting='Tri', 
                agent='Tri: SAC-Phase/volt-Sim2real', 
                tomoscope='PR.SCOPE57.CH01/Acquisition', 
                criterion=0.0008):

        #print(japc.getUsers('CPS'))

        logging.info('Initializing agents and CNNs...')
        self.tomo = tomoscope
        self.criterion = criterion
        self.volt_criterion = 0.0005
        self.user = user
        self.japc = pyjapc.PyJapc(selector=self.user)
        self.tomo_values = None
        self.fwhms = None
        self.intensities=None
        self.splitting = splitting
        self.beam_type = '72b_25ns' # Default is 72b.

        self.phase_criterion = 0.0008
        self.volt_criterion = 0.0005


        ### Load feat extr.
        self.model = CNN_tri_new(trim_edges=True)
        feat_model_name = 'cnn_tri_Trim_Move_Injection_PV_59521_ref_voltageJul_11_2022_09-22-53_e43' # Trim edges version
        self.model_quad = CNN() 
        feat_model_name_quad = 'cnn_quad_ms_ADD_NOISE_Move_injectione_46_FINETUNE_LIVEMay_05_2022_15-41-33_e11'# 


        print(f'Fetching model {feat_model_name}...')
        path = r"/eos/home-j/jwulff/workspaces/RL-007-RFinPS/optimising-rf-manipulations-in-the-ps-through-rl/saved_models/{}.pth".format(feat_model_name)
        #path = r"C:\Users\jwulff\cernbox\workspaces\RL-007-RFinPS\optimising-rf-manipulations-in-the-ps-through-rl\saved_models\{}.pth".format(model_name)
        self.model = load_model(path,self.model)
        #optimizer.load_state_dict(checkpoint['optimizer_state_dict'])
        print(f'Weights loaded...')
        self.model.eval() ### IMPORTANT! If not in eval mode, batchnorm layers will not work correctly.
        print(f'Fetching model {feat_model_name_quad}...')
        path = r"/eos/home-j/jwulff/workspaces/RL-007-RFinPS/optimising-rf-manipulations-in-the-ps-through-rl/saved_models/{}.pth".format(feat_model_name_quad)
        self.model_quad = load_model(path,self.model_quad)
        print(f'Weights loaded...')
        self.model_quad.eval() ### IMPORTANT! If not in eval mode, batchnorm layers will not work correctly.

        ### Load RL agents
        # Tri
        model_name_phase = r"SAC-RelBLIntPhase-phase-loss-profile-step10-59521-ref-NoiseMoveInjPV-diff-00009"
        self.agent_tri = SAC.load("./RL_logs/tri/{}".format(model_name_phase))

        volt_model_name = r"SAC-RelBLIntVolt-voltloss-step01-profile-59521-ref-NoiseMoveInjPV-diff-00010"
        self.agent_volt_tri = SAC.load("./RL_logs/tri/{}".format(volt_model_name))

        ref_voltage_h14 = np.load(dir_path + '/v14_settings_max_step_01.npy')
        self.max_step_volt = 0.1
        self.voltage_max_step = ref_voltage_h14*self.max_step_volt 
        self.max_step_size=20

        # Load agents quad
        model_name_42 = r"SAC-Simple-Profile-relBLInt42-step20-diff-00003"
        self.agent_42 = SAC.load("./RL_logs/quad/{}".format(model_name_42))
        model_name_84 = r"SAC-Simple-Profile-dist8-relBLInt84-step20-diff-00003"
        self.agent_84 = SAC.load("./RL_logs/quad/{}".format(model_name_84))

        

        self.japc.subscribeParam(self.tomo, self.checkIfNewDataReceivedCallback)
        self.japc.startSubscriptions()

    def checkIfNewDataReceivedCallback(self, parameterName, newValue):
            self.new_values_received=True
            logging.info(f"New value for {parameterName}.")

    def suggest_step_tri(self):
        if self.tomo_values is None:
            logging.info(f'No tomo data available {self.tomo_values}. Wait for next acquisition.')
            
            return
        else:
            phase_obs, phase_loss, volt_obs, volt_loss = self.get_and_process_acquisition()
            logging.info(f'Phase loss: {phase_loss}, volt loss: {volt_loss}')
            if phase_loss < self.criterion:
                logging.info('Phase loss below criteria, checking voltage.')
                if volt_loss < self.volt_criterion:
                    logging.info('Volt loss below criteria, splitting optimised. Suggestion anyway below.')
                action, _ = self.agent_volt_tri.predict(volt_obs, deterministic=True)
                #volt_step = action * self.voltage_max_step # volt max step
                logging.info(f'Suggested volt step: {action*self.max_step_volt})')
                #return
            phase_action, _ = self.agent_tri.predict(phase_obs, deterministic=True)
            converted_action = np.array([0.0,0.0,0.0], dtype=object)
            converted_action[0] = phase_action[0]*self.max_step_size
            converted_action[1] = phase_action[1]*self.max_step_size
            logging.info(f'Suggested phase action: p14: {converted_action[0]}, p21: {converted_action[1]}.')


    def wait_for_acquisition(self):
        logging.info('Waiting for new acquisition...')
        self.new_values_received = False
        while not self.new_values_received:
            time.sleep(1)
        return
    

    def get_and_process_acquisition(self):
        mode = self.splitting
        data_dictionary = self.japc.getParam(self.tomo)
        values = data_dictionary['value'] # Matrix if using tomoscope
        if mode=='Tri':
            try:
                assert np.shape(values) == (150,400)
            except AssertionError as e:
                logging.error(f"Unexpected tomoscope acq. shape {np.shape(values)}. Exiting...")
                return None
        if mode=='Quad':
            try:
                assert np.shape(values) == (150,200)
            except AssertionError as e:
                logging.error(f"Unexpected tomoscope acq. shape {np.shape(values)}. Exiting...")
                return None
        values_centered_norm = process_tomoscope_acquisition(values, mode='tri', remove_tails=True, correct_dm=True)
        values_centered_norm_trimmed = values_centered_norm[:,40:360]
        self.tomo_values = values_centered_norm_trimmed
        profile = values_centered_norm_trimmed[-1,:]
        #corr = correct_profile_tri(profile)
        #################
        # CALCULATE LOSS
        #################
        bunches=[]
        bunches, fwhms, intensities = isolate_bunches_from_dm_profile_tri(profile, intensities=True, rel=True)
        # Norm intensities: Try only relative intensities/fwhms
        intensities = intensities / np.max(intensities) # Same as in sim. #TODO
        intensities = intensities - np.mean(intensities)

        fwhms = fwhms - np.mean(fwhms)
        self.fwhms = fwhms
        self.intensities=intensities
        #fwhms_memory.append([fwhms])
        #intensities_memory.append([intensities])

        b1 = bunches[0]
        b2 = bunches[1]
        b3 = bunches[2]
        #loss = loss_function_tri(b1,b2,b3)
        phase_loss = tri_phase_loss(b1, b3)
        volt_loss = loss_function_tri(b1,b2,b3)
        #self.loss_memory.append(loss)
        phase_obs = [fwhms[0], fwhms[2], intensities[0], intensities[2]]
        volt_obs = np.append(fwhms,intensities)
        return phase_obs, phase_loss, volt_obs, volt_loss

    def change_p21(self, value):
        offset_GSRPC(self.japc, phase_offset=value)

    def change_p14(self,value):
        offset_GSRPB(self.japc, phase_offset=value)
    
    def update_voltage_program(self,value):
        update_voltage_program_GSVMOD7(self.japc, value)

    def use_feature_extractor(self):
        if self.tomo_values is None:
            logging.info(f'No tomo data available {self.tomo_values}. Wait for next acquisition.')
            
            return
        sample_trim = {'image': self.tomo_values, 'labels': np.ndarray([0])}
        transform = transforms.Compose([
            #Normalize(),
            ToTensor(),
            ])
        inputs_dict_trim = transform(sample_trim)
        image_trim = inputs_dict_trim['image'].unsqueeze(dim=0)
        outputs = self.model(image_trim).detach().numpy()/100
        outputs[0][2] = outputs[0][2]/10
        logging.info(f'Feat extractor predicted phase offsets: {outputs[0][:2]}')
        outputs[0][2] = 0
        return outputs[0]

    def change_user(self, new_user):
        self.japc.setSelector(new_user)
        return

    def change_beam_type(self, new_type):
        self.beam_type=new_type
        return

    def change_tomo(self, new_tomo):
        self.tomo = new_tomo
        self.japc.subscribeParam(self.tomo, self.checkIfNewDataReceivedCallback)
        self.japc.startSubscriptions()
        return

    def change_criterion(self, new_criterion):
        self.criterion = new_criterion
    
    def start_automatic_optimisation(self,):
        if self.beam_type is '48b_BCMS':
            logging.info(f'Not yet implemented for BCMS...')
            # self.tri_optimisation_48b_BCMS()
        if self.beam_type is '72b_25ns':

            self.tri_optimisation_72b()

    
    def tri_optimisation_72b(self):

        phase_step = 0
        volt_step = 0
        tot_steps = 0
        phase_set_memory = []
        action_memory = []
        phase_set_memory = [] # Doubles as labels for dm:s in dm_memory. May need to convert them to resemble labels in original dataset.
        dm_memory = []
        phase_loss_memory = []
        volt_loss_memory = []
        profile_memory = []
        fwhms_memory = []
        intensities_memory = []
        rel_phase_volt_set = [[0,0,0]]

        ### Phase and volt criterion - decides what loss they will accept and terminate with.
        phase_criterion = self.phase_criterion
        volt_criterion = self.volt_criterion

        ### Trying to save things to make future online RL possible.
        phase_obs_memory = []
        volt_obs_memory = []

        ### Max opt steps allowed - How many steps are the phase/voltage agents allowed to take before terminating run.
        max_total_opt_steps = 30
        
        logging.info(f'Starting automatic optimisation of {self.splitting} split...')
        self.wait_for_acquisition()
        phase_obs, phase_loss, volt_obs, volt_loss = self.get_and_process_acquisition()
        ### Store obervations for plotting/saving
        phase_obs_memory.append([phase_obs])
        phase_loss_memory.append([phase_loss])
        dm_memory.append(self.tomo_values) # Append tomoscope values to allow for saving.
        profile_memory.append(self.tomo_values[-1,:])
        fwhms_memory.append([self.fwhms])
        intensities_memory.append([self.intensities])

        plt.figure('Phase optimisation')
        render((-20,20), (-20,20), rel_phase_volt_set, self.profile, 
                phase_loss, phase_loss_memory, self.tomo_values, 
                self.bunches, self.fwhms, self.intensities, 
                criterion=phase_criterion, step=phase_step)
        # Initial feat extractor step
        initial_guess = self.use_feature_extractor()
        converted_action = -initial_guess
        logging.info(f'Taking initial guess from feat extractor: Moving {converted_action}')
        initial_feat_guess=False
        # Track correction
        corr=converted_action
        h14_offset += converted_action[0]
        h21_offset += converted_action[1]
        v14_offset += 0
        rel_phase_volt_set.append(rel_phase_volt_set[-1]+corr)
        h14_input = converted_action[0]
        h21_input = converted_action[1]
        v14_input = converted_action[2]
        logging.info(f'Current relative phase from start: {rel_phase_volt_set[0]}, {rel_phase_volt_set[1]}')
        logging.info(f'Taking Action: {converted_action})')
        logging.info(f'Profile-based loss: {phase_loss}')
        if self.beam_type is '48b_BCMS':
            offset_GSRPC_BCMS(self.japc, phase_offset=h14_input)
            offset_GSRPB_BCMS(self.japc, phase_offset=h14_input)
        elif self.beam_type is '72b_25ns':
            offset_GSRPC(self.japc, phase_offset=h14_input)
            offset_GSRPB(self.japc, phase_offset=h21_input)
        phase_step += 1
        tot_steps += 1
        action_memory.append([converted_action])
        

        while not phase_done:
            self.wait_for_acquisition()
            phase_obs, phase_loss, volt_obs, volt_loss = self.get_and_process_acquisition()
            
            ### Store obervations for plotting/saving
            phase_obs_memory.append([phase_obs])
            phase_loss_memory.append([phase_loss])
            dm_memory.append(self.tomo_values) # Append tomoscope values to allow for saving.
            profile_memory.append(self.profile)
            fwhms_memory.append([self.fwhms])
            intensities_memory.append([self.intensities])
            
            plt.figure('Phase optimisation')
            render((-20,20), (-20,20), rel_phase_volt_set, self.profile, 
                        phase_loss, phase_loss_memory, self.tomo_values, 
                        self.bunches, self.fwhms, self.intensities, 
                        criterion=phase_criterion, step=phase_step)
            plt.show()

            if phase_loss < self.criterion:
                logging.info(f'Phase criterion reached in {phase_step} steps, ending episode early. Optimisation finished...')
                plt.figure('Phase optimisation')
                render((-20,20), (-20,20), 
                        rel_phase_volt_set, 
                        self.profile, 
                        phase_loss, 
                        phase_loss_memory, 
                        self.tomo_values, 
                        self.bunches, 
                        self.fwhms, 
                        self.intensities, 
                        criterion=phase_criterion, 
                        done = True, 
                        step=phase_step)
                plt.show()
                #plt.savefig('{}Ep{}_optimisation.png'.format(SAVE_DIR, episode))
                phase_done=True
                break
            else: #phase not yet optimised, take opt. step
                logging.info(f'Phase loss not optimal, taking step {phase_step}...')
                converted_action = self.predict_phase_opt_step(phase_obs)
                corr=converted_action
                h14_offset += converted_action[0]
                h21_offset += converted_action[1]
                v14_offset += 0
                rel_phase_volt_set.append(rel_phase_volt_set[-1]+corr)
                h14_input = converted_action[0]
                h21_input = converted_action[1]
                v14_input = converted_action[2]
                logging.info(f'Current relative phase from start: {rel_phase_volt_set[0]}, {rel_phase_volt_set[1]}')
                logging.info(f'Taking Action: {converted_action})')
                logging.info(f'Profile-based loss: {phase_loss}')
                if self.beam_type is '48b_BCMS':
                    offset_GSRPC_BCMS(self.japc, phase_offset=h14_input)
                    offset_GSRPB_BCMS(self.japc, phase_offset=h14_input)
                elif self.beam_type is '72b_25ns':
                    offset_GSRPC(self.japc, phase_offset=h14_input)
                    offset_GSRPB(self.japc, phase_offset=h21_input)
                phase_step += 1
                tot_steps += 1
        
        volt_done = False
        profile_mem = []
        volt_loss_memory = []
        max_step_volt = 0.1
        # Wait for next acquisition
        #plt.figure('Voltage optimisation')
        while not volt_done:
            logging.info(f'Beginning volt step {volt_step}')

            self.wait_for_acquisition()
            # Extract values of acquisition
            phase_obs, phase_loss, volt_obs, volt_loss = self.get_and_process_acquisition()
            
            volt_obs_memory.append([volt_obs])
            volt_loss_memory.append([volt_loss])
            dm_memory.append(self.tomo_values) # Append tomoscope values to allow for saving.
            profile_memory.append(self.profile)
            fwhms_memory.append([self.fwhms])
            intensities_memory.append([self.intensities])

            plt.figure('Voltage optimisation')
            render((-20,20), (-20,20), 
                    rel_phase_volt_set, 
                    self.profile, 
                    volt_loss, 
                    volt_loss_memory, 
                    self.tomo_values, 
                    self.bunches, 
                    self.fwhms, 
                    self.intensities, 
                    criterion=volt_criterion, 
                    step=volt_step)
            plt.show()
            if phase_loss > self.criterion*1.2:
                print(f'Phase deteriorated to loss of 1.2*criterion, re-optimising')
                while phase_loss > self.criterion:
                    converted_action = self.predict_phase_opt_step(phase_obs)
                    corr=converted_action
                    h14_offset += converted_action[0]
                    h21_offset += converted_action[1]
                    v14_offset += 0
                    rel_phase_volt_set.append(rel_phase_volt_set[-1]+corr)
                    h14_input = converted_action[0]
                    h21_input = converted_action[1]
                    v14_input = converted_action[2]
                    logging.info(f'Current relative phase from start: {rel_phase_volt_set[0]}, {rel_phase_volt_set[1]}')
                    logging.info(f'Taking Action: {converted_action})')
                    logging.info(f'Profile-based loss: {phase_loss}')
                    offset_GSRPC(self.japc, phase_offset=h14_input)
                    offset_GSRPB(self.japc, phase_offset=h21_input)
                    phase_step += 1
                    tot_steps += 1

                    self.wait_for_acquisition()
                    phase_obs, phase_loss, volt_obs, volt_loss = self.get_and_process_acquisition()

                    ### Store obervations for plotting/saving
                    phase_obs_memory.append([phase_obs])
                    phase_loss_memory.append([phase_loss])
                    dm_memory.append(self.tomo_values) # Append tomoscope values to allow for saving.
                    profile_memory.append(self.profile)
                    fwhms_memory.append([self.fwhms])
                    intensities_memory.append([self.intensities])
                    
                    plt.figure('Phase optimisation')
                    render((-20,20), (-20,20), rel_phase_volt_set, self.profile, 
                            phase_loss, phase_loss_memory, self.tomo_values, 
                            self.bunches, self.fwhms, self.intensities, 
                            criterion=phase_criterion, step=phase_step)
                    plt.show()

            logging.info(f'Phase loss: {phase_loss}, still ok.')
            if volt_loss < volt_criterion:
                logging.info(f'Voltage optimised. Took {volt_step} steps to optimise.')
                tot = phase_step + volt_step
                logging.info(f'Total number of steps for phase and volt: {tot}')
                plt.figure('Voltage optimisation')
                render((-20,20), (-20,20), 
                        rel_phase_volt_set, 
                        self.profile, 
                        volt_loss, 
                        volt_loss_memory, 
                        self.tomo_values, 
                        self.bunches, 
                        self.fwhms, 
                        self.intensities, 
                        criterion=volt_criterion, 
                        step=volt_step,
                        done=True)
                volt_done=True
            #values = cv2.resize(values, dsize=(400,150))
            else:
                action, _ = self.agent_volt_tri.predict(volt_obs, deterministic=True)
                volt_action = action * self.voltage_max_step # volt max step
                logging.info(f'Taking volt step: {action*max_step_volt})')
                converted_action = np.array([0.0,0.0,0.0], dtype=object)
                converted_action[2] = volt_step

                voltage_factor += action[0]*max_step_volt
                #np.save('/eos/home-j/jwulff/workspaces/RL-007-RFinPS/optimising-rf-manipulations-in-the-ps-through-rl/dataset/tri/new_live_data/p14_{}_p21_{}_v14_{}_datamatrix'.format(int(h14_offset*100), int(h21_offset*100), int(voltage_factor*1000)), values_centered_norm)

                new_phase = rel_phase_volt_set[-1].copy()
                new_phase[2] = new_phase[2]+action[0]*max_step_volt
                rel_phase_volt_set.append(new_phase)
                converted_action = np.array([0.0,0.0,0.0], dtype=object)
                if self.beam_type is '48b_BCMS':
                    update_voltage_program_GSVMOD7_BCMS(self.japc, volt_action)
                elif self.beam_type is '72b_25ns':
                    update_voltage_program_GSVMOD7(self.japc, volt_action)
            volt_step += 1
            tot_steps += 1
            action_memory.append([converted_action])
            dm_memory.append(self.tomo_values)
        print('Exiting optimisation..')
        plt.show(block=True)
    def predict_phase_opt_step(self, phase_obs):
        max_step_size = 10
        action, _states = self.agent_tri.predict(phase_obs, deterministic=True)
        converted_action = np.array([0.0,0.0,0.0], dtype=object)
        converted_action[0] = action[0]*max_step_size
        converted_action[1] = action[1]*max_step_size
        return converted_action

