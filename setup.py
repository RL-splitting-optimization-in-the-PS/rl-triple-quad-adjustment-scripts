"""
setup.py for RL-splitting-adjustment-scripts.

For reference see
https://packaging.python.org/guides/distributing-packages-using-setuptools/

"""
from pathlib import Path
from setuptools import setup, find_packages


HERE = Path(__file__).parent.absolute()
with (HERE / 'README.md').open('rt', encoding='utf-8') as fh:
    LONG_DESCRIPTION = fh.read().strip()


REQUIREMENTS: dict = {
    'core': [
        "gym >= 0.21.0",
        "matplotlib ~= 3.0",
        "numpy >= 1.0",
        "scipy >= 1.5",
        "pyjapc >= 2.0",
        "torch >= 1.11.0",
        "torchvision >= 0.12.0",
        #"stable_baselines3 >= 1.5.0", # Included in mlp-rf-splittings
        "tensorboard >= 2.9.1",
        #"cloudpickle", # Included in mlp-rf-splittings
        #"pickle5", # Included in mlp-rf-splittings
        "longitudinal-tomography",
        "accwidgets[all-widgets]",
        "mlp-rf-splittings"
    ],
    'test': [
        'pytest',
    ],
    'dev': [
        # 'requirement-for-development-purposes-only',
    ],
    'doc': [
        'sphinx',
        'acc-py-sphinx',
    ],
}

setup(
    name='RL-splitting-adjustment-scripts',
    version="0.0.1.dev0",

    author='Joel Wulff',
    author_email='joel.wulff@cern.ch',
    description='SHORT DESCRIPTION OF PROJECT',
    long_description=LONG_DESCRIPTION,
    long_description_content_type='text/markdown',
    url='',

    packages=find_packages(),
    python_requires='~=3.7',
    classifiers=[
        "Programming Language :: Python :: 3",
        "Operating System :: OS Independent",
    ],

    install_requires=REQUIREMENTS['core'],
    extras_require={
        **REQUIREMENTS,
        # The 'dev' extra is the union of 'test' and 'doc', with an option
        # to have explicit development dependencies listed.
        'dev': [req
                for extra in ['dev', 'test', 'doc']
                for req in REQUIREMENTS.get(extra, [])],
        # The 'all' extra is the union of all requirements.
        'all': [req for reqs in REQUIREMENTS.values() for req in reqs],
    },
)
