import sys
sys.path.append(r'C:\Users\jwulff\cernbox\workspaces\RL-007-RFinPS\optimising-rf-manipulations-in-the-ps-through-rl') # Old solution
# from pathlib import Path
# mod_path = str(Path(__file__).parent.parent) # Go up two steps to base of git repo.
# sys.path.append(mod_path)
import torch
import torch.nn as nn
import torch.nn.functional as F
#from pytorch_fitmodule import FitModule
from torch.autograd import Variable
import numpy as np
from dataloader import QuadsplitDataset, ToTensor, Normalize, NormalizeLabels, MoveInjectionCenter
from torchvision import transforms, utils
from torch.utils.data import Dataset, DataLoader
from CNN import CNN
from CNN_w_tanh import CNN_tanh
from CNN_w_tanh_maxpool import CNN_tanh_maxpool
from CNN_maxpool import CNN_maxpool
from MLP_Profile_feat_extractor import MLPProfile
from CNN_tri import CNN_tri
import torch.optim as optim
from utils import count_parameters
from torch.utils.tensorboard import SummaryWriter
from sklearn.model_selection import train_test_split
import datetime
import time
import os
import json
#r"C:\Users\jwulff\cernbox\workspaces\RL-007-RFinPS\optimising-rf-manipulations-in-the-ps-through-rl\dataset\quad\live_data\dataset.csv",
#r"C:\Users\jwulff\cernbox\workspaces\RL-007-RFinPS\optimising-rf-manipulations-in-the-ps-through-rl\dataset\quad\machine_settings_fixed\dataset.csv"
#r"C:\Users\jwulff\cernbox\workspaces\RL-007-RFinPS\optimising-rf-manipulations-in-the-ps-through-rl\dataset\tri\machine_settings_fixed\dataset_tri_xl_fixed_voltage.csv"

hparams = {
    'model_name': 'cnn_tri_Trim_Move_Injection_PV_59521_ref_voltage',
    'data_csv_path': r"C:\Users\jwulff\cernbox\workspaces\RL-007-RFinPS\optimising-rf-manipulations-in-the-ps-through-rl\dataset\tri\ref_voltage\dataset_tri_59521_ref.csv",
    'batch_size': 10,
    'log_dir': r"C:\Users\jwulff\cernbox\workspaces\RL-007-RFinPS\optimising-rf-manipulations-in-the-ps-through-rl\tensorboard_logs",
    'input_type': 'image', # profile, image
    'model': 'cnn_tri', # cnn, cnn_tanh, cnn_maxpool, mlpProfile ...
    'tri_fixed_voltage': False,
    'rf_manip': 'tri',
    'start_epoch': 0,
    'end_epoch': 50,
    'optimizer': 'adam',
    'add_noise': True,
    'move_injection': True,
    'opt_lr': 1e-3,
    'adam_b1': 0.9,
    'adam_b2': 0.999,
    'resize_data': False,
    'use_live_data': False,
    'load_model': False,
    'load_model_path': r"C:\Users\jwulff\cernbox\workspaces\RL-007-RFinPS\optimising-rf-manipulations-in-the-ps-through-rl\saved_models\cnn_quad_ms_ADD_NOISE_Move_injectionApr_28_2022_12-28-32_e29_continue__e46.pth",
    'test_logging': False,
    'norm_labels': False,
    'tanh': False, # Remember to also put norm labels to true when using tanh activation.
    'trim_edges': True,

}




###  Load and split dataset

DEBUG = True
batch_size = hparams['batch_size']
print('Loading dataset from {}...'.format(hparams['data_csv_path']))
if hparams['input_type'] == 'image':

    if hparams['resize_data']:
        # Deprecated, kept in case we want to resize in future.
        if hparams['rf_manip'] == 'tri':
            dataset = QuadsplitDataset(resize = hparams['rf_manip'], csv_file = hparams['data_csv_path'], tri_fixed_voltage=hparams['tri_fixed_voltage'])
        elif hparams['rf_manip'] == 'quad':
            dataset = QuadsplitDataset(resize = hparams['rf_manip'], csv_file = hparams['data_csv_path'])
    elif hparams['rf_manip'] == 'quad':
        dataset = QuadsplitDataset(csv_file = hparams['data_csv_path'], 
                                    noise=hparams['add_noise'], 
                                    live_data=hparams['use_live_data'], 
                                    norm_labels = hparams['norm_labels'],
                                    move_injection = hparams['move_injection'])
    elif hparams['rf_manip'] == 'tri':
        dataset = QuadsplitDataset(csv_file = hparams['data_csv_path'], 
                                    noise=hparams['add_noise'], 
                                    live_data=hparams['use_live_data'], 
                                    norm_labels = hparams['norm_labels'],
                                    move_injection = hparams['move_injection'],
                                    tri_fixed_voltage=hparams['tri_fixed_voltage'], # Added to allow use of tri split simulated dataset with only voltage factor=1.0
                                    trim_edges = hparams['trim_edges']) 
elif hparams['input_type'] == 'profile':
    # Point dataset to profiles instead of datamatrix images
    #data_csv_path = hparams['data_csv_path']
    #split = data_csv_path.split('\\')
    #split[-1] = 'dataset_profiles.csv'
    #ata_csv_path = '\\'.join(split)
    dataset = QuadsplitDataset(csv_file = hparams['data_csv_path'], 
                                    noise=hparams['add_noise'], 
                                    live_data=hparams['use_live_data'], 
                                    norm_labels = hparams['norm_labels'],
                                    move_injection = hparams['move_injection'],
                                    use_last_profile=True) # Use last profile of dm instead of saved profile, as we want it close to the output of tomoscope in the machine. 
length = dataset.__len__()
train_len = int(length * 0.9)
 
val_len = length - train_len
train, val = torch.utils.data.random_split(dataset, [train_len, val_len], generator=torch.Generator().manual_seed(37)) # Old seed 42
if hparams['test_logging']:
    print('Using val set for both train and val to test logging...')
    test_len = int(val_len/2)
    train, val = torch.utils.data.random_split(val, [test_len, val_len-test_len], generator=torch.Generator().manual_seed(42))
train_dataloader = DataLoader(train, batch_size=batch_size, shuffle=True, num_workers=0)
val_dataloader = DataLoader(val, batch_size=batch_size, shuffle=True, num_workers=0)

model_name = hparams['model_name']

### Initialize model. Load different model depending on chosen architechture.

if hparams['tanh']:

    if hparams['model'] == 'cnn':
        model = CNN_tanh()
        nbr_params = count_parameters(model)
    # elif hparams['model'] == 'cnn_tri':
    #     model = CNN_tri()
    #     nbr_params = count_parameters(model)
    elif hparams['model'] == 'cnn_maxpool':
        model = CNN_tanh_maxpool()
        nbr_params = count_parameters(model)
    elif hparams['model'] == 'mlpProfile':
        model = MLPProfile()
        nbr_params = count_parameters(model) 
else:
    if hparams['model'] == 'cnn':
        model = CNN()
        nbr_params = count_parameters(model)
    elif hparams['model'] == 'cnn_tri':
        if hparams['tri_fixed_voltage']:
            model = CNN_tri(num_features=2)
        else:
            if hparams['trim_edges']:
                model = CNN_tri(trim_edges=True)
            else:
                model = CNN_tri()
        nbr_params = count_parameters(model)
    elif hparams['model'] == 'cnn_maxpool':
        model = CNN_maxpool()
        nbr_params = count_parameters(model)
    
criterion = nn.MSELoss()
optimizer = optim.Adam(model.parameters(), 
                        betas=(hparams['adam_b1'], hparams['adam_b2']), 
                        lr=hparams['opt_lr'])
training_step = 0
eval_step=0
steps_per_epoch = int(train.__len__()/batch_size)
val_steps_per_epoch = int(val.__len__()/batch_size)

### Load model weights from previous run, if chosen.

now = datetime.datetime.now()
date_time = now.strftime("%b_%d_%Y_%H-%M-%S")
run_name = model_name + date_time

# TODO Fix bug: when the name of the model is too long, an error is triggered and training does not start. Unclear why.
# Current workaround is simply not using too long names.

if hparams['load_model']:
    path = hparams['load_model_path']
    loaded_model_name = os.path.splitext(os.path.basename(path))[0]
    print(f'Fetching model {path}...')
    checkpoint = torch.load(path)
    model.load_state_dict(checkpoint['model_state_dict'])
    optimizer.load_state_dict(checkpoint['optimizer_state_dict'])
    criterion = checkpoint['loss']
    run_name = loaded_model_name + '_continue'# + run_name
    if hparams['use_live_data']: # temporary solution when using live data
        run_name = model_name + date_time
    print('Weights loaded. Resuming training from epoch {}'.format(checkpoint['epoch']))
    training_step = steps_per_epoch * hparams['start_epoch']
    eval_step = val_steps_per_epoch * hparams['start_epoch']

# Tensorboard support

writer_val = SummaryWriter(log_dir=os.path.join(hparams['log_dir'], run_name, 'val'))
writer_train = SummaryWriter(log_dir=os.path.join(hparams['log_dir'], run_name, 'train'))
print('Logging training to {}...'.format(hparams['log_dir']))

# Training metadata
information = {'epoch_time': 0, 'time_per_batch': 0, 'batch_size': batch_size, 'steps_per_epoch': steps_per_epoch}

# Save hparams used for training for future checks
with open(r'C:\Users\jwulff\cernbox\workspaces\RL-007-RFinPS\optimising-rf-manipulations-in-the-ps-through-rl\neural_networks\training_metadata\{}.json'.format(run_name), 'w') as fp:
    json.dump(hparams, fp,  indent=4)


start_epoch = hparams['start_epoch']
epochs_to_run = hparams['end_epoch']# - hparams['start_epoch']


### START OF TRAINING LOOP

print('Starting training of {} feat extr for rf manip {}. Training name: {}...'.format(hparams['model'], hparams['rf_manip'], hparams['model_name']))
for epoch in range(hparams['start_epoch'], hparams['end_epoch']):
    model.train()
    steps_left = steps_per_epoch
    accum_loss = 0.0
    running_loss=0.0
    for i, data in enumerate(train_dataloader, 0):
        #if DEBUG:
        #    start = time.time()
        start = time.time()
        inputs, labels = data['image'].float(), data['labels'].float()
        #end = time.time()
        #print("Batch loaded in {}".format(end-start))
        # zero the parameter gradients
        optimizer.zero_grad()
        # forward + backward + optimize
        outputs = model(inputs).squeeze()
        #end = time.time()
        #print("Time of forward pass: {}".format(end-start))
        loss = criterion(outputs, labels)
        writer_train.add_scalar("Loss/train_step", loss, training_step)
        loss.backward()
        optimizer.step()
        end = time.time()
        training_step += 1
        steps_left -= 1
        secs_left = (end-start)*steps_left
        time_left = str(datetime.timedelta(seconds = secs_left))

        # print statistics
        running_loss += loss.item()
        accum_loss += loss.item()
        if i % 10 == 9:    # print every 10 mini-batches
            print(f'[{epoch + 1}/{epochs_to_run}, {i + 1:5d}/{steps_per_epoch}] loss: {running_loss / 10:.8f}, Est. epoch time left: {time_left} s')
            running_loss = 0.0
    mean_loss = accum_loss/i
    
    eval_loss = 0.0
    avg_loss = 0.0
    model.eval()
    print("Validation...")
    
    for i, data in enumerate(val_dataloader, 0):
        with torch.no_grad():
            inputs, labels = data['image'].float(), data['labels'].float()
            outputs = model(inputs).squeeze()
            loss = criterion(outputs,labels)
            writer_val.add_scalar("Loss/val_step", loss, eval_step)
            eval_step += 1
            eval_loss += loss.item()
            avg_loss += loss.item()
            if i % 10 == 9:    # print every 10 mini-batches
                print(f'[{epoch + 1}/{epochs_to_run}, {i + 1:5d}/{val_steps_per_epoch}] loss: {eval_loss / 10:.8f}')
                eval_loss = 0.0
    if i != 0:
        avg_loss = avg_loss/i
    writer_train.add_scalar("Train_val/Loss", mean_loss, epoch)
    writer_val.add_scalar("Train_val/Loss", avg_loss, epoch)
    torch.save({
                'epoch': epoch+1,
                'model_state_dict': model.state_dict(),
                'optimizer_state_dict': optimizer.state_dict(),
                'loss': criterion,
                }, 'saved_models/{}_e{}.pth'.format(run_name, epoch))
writer_train.flush()
writer_val.flush()
writer_train.close()
writer_val.close()
print('Finished Training')