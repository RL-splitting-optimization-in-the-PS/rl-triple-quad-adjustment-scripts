from tracemalloc import start
from RL_splitting_adjustment_scripts.splitting_agent_base import SplittingAgentBase
import pyjapc
from RL_splitting_adjustment_scripts.vpc_ajdust_phases import offset_GSRPB, offset_GSRPC, update_voltage_program_GSVMOD7
import numpy as np
"""
Instructions to run automatic optimisation tri/quad functions:

1. Open a tomoscope on your beam.
2. Open the splitting reference of your interest (triple or quad, 72bs/48b_bcms)
3. Make sure the acquisition is centered on the splitting of one bunch.
4. Check which tomoscope is being used (57/58) and use the correct name in the SplittingAgentBase init.
5. Make sure the correct settings are given to SplittingAgentBase:
    user, splitting, tomoscope, and beam type (72b_25ns or 48b_BCMS).
6. Use the approproate function for you:
    splitting_class.start_automatic_optimisation_tri
    splitting_class.start_automatic_optimisation_quad

    with your settings of choice.
"""


if __name__ == "__main__":
    
    ########## General settings ##########

    user = 'CPS.USER.MD2'
    tomoscope = 'PR.SCOPE57.CH01/Acquisition'
    beam_type = '72b_25ns' # or 48b_BCMS

    # Define starting points to visit with agents.

    japc = pyjapc.PyJapc(selector=user)

    # episodes 1-6
    # starting_points = [
    #     [20,20,0.05],
    #     [-20,-20,0.05],
    #     [10,-10, -0.05],
    #     [-10,10,0.05],
    #     [15,5,0.10],
    #     [5,-15,-0.10]
    # ]
    # Fixing plotting of voltage steps (hopefully) for eps 7+.
    # p42s = np.linspace(-20,20,2)
    # p84s = np.linspace(-20,20,2)
    # v14s = np.linspace(-0.08,0.08,2)
    # starting_points = []
    # for p42 in p42s:
    #     for p84 in p84s:
    #         for v14 in v14s:
    #             init_point = [p42,p84,v14]
    #             starting_points.append(init_point)

    starting_points = [
        [12.5,20,0.05],
        [-20,-10,0.05],
        [15,-10, -0.05],
        [-15,10,0.05],
        [5,15,0.10],
        [-10,-20,-0.10]
    ] #Episodes 7-12
    episode = 1
    h14_phase_setting_init = japc.getParam('PA.GSRPC/Amplitude#amplitude')
    h21_phase_setting_init = japc.getParam('PA.GSRPB/Amplitude#amplitude')

    # GSVMOD7 (v14)
    V14_INIT = japc.getParam('PA.GSV10MOD7/Setting#amplitudes') # ??
    NEW_v14 = V14_INIT.copy()
    init_voltage_settings = NEW_v14[3][1]
    
    splitting_class = SplittingAgentBase(user=user, 
                                            splitting='tri', 
                                            tomoscope=tomoscope, 
                                            beam_type=beam_type)

    splitting_class_2 = SplittingAgentBase(user=user, 
                                            splitting='tri', 
                                            tomoscope=tomoscope, 
                                            beam_type=beam_type)
    for settings in starting_points:
        nbr_of_ep = len(starting_points)
        print(f'Starting episode {episode}/{nbr_of_ep}...')
        START_OFFSET = settings#[40,-15, -0.05]
        h14_offset = START_OFFSET[0] # 
        h21_offset = START_OFFSET[1] #
        voltage_step_offset = START_OFFSET[2]
        voltage_factor = 1.0 + voltage_step_offset

        # offset_GSRPC(japc, phase_offset=h14_offset)
        # offset_GSRPB(japc, phase_offset=h21_offset)
        # voltage_offset = init_voltage_settings*voltage_step_offset
        # update_voltage_program_GSVMOD7(japc, voltage_offset)

        

        # splitting_class_2.start_automatic_optimisation_tri_all_params_agent(
        #                                 max_steps=30, 
        #                                 render_opt=True,
        #                                 phase_volt_criterion=0.0005,
        #                                 save_dir = '/eos/home-j/jwulff/workspaces/RL-007-RFinPS/optimising-rf-manipulations-in-the-ps-through-rl/MD_results_16_11_2022/tri/all_params/Episode_{}'.format(episode),
        #                                 block=False)
        


        # # RESET
        # japc.setParam('PA.GSV10MOD7/Setting#amplitudes', V14_INIT, checkDims=False) # ??
        # japc.setParam('PA.GSRPC/Amplitude#amplitude', h14_phase_setting_init)
        # japc.setParam('PA.GSRPB/Amplitude#amplitude', h21_phase_setting_init)

        offset_GSRPC(japc, phase_offset=round(h14_offset,4))
        offset_GSRPB(japc, phase_offset=round(h21_offset,4))
        voltage_offset = init_voltage_settings*voltage_step_offset
        update_voltage_program_GSVMOD7(japc, voltage_offset)

        
        splitting_class.start_automatic_optimisation_tri(initial_feat_guess=True, 
                                        max_steps=30, 
                                        render_opt=True,
                                        phase_criterion=0.0008,
                                        volt_criterion = 0.0005,
                                        save_dir = '/eos/home-j/jwulff/workspaces/RL-007-RFinPS/optimising-rf-manipulations-in-the-ps-through-rl/MD_results_16_11_2022/tri/phase_volt/mlp/Episode_{}'.format(episode),
                                        block=False)

        # RESET
        japc.setParam('PA.GSV10MOD7/Setting#amplitudes', V14_INIT, checkDims=False) # ??
        japc.setParam('PA.GSRPC/Amplitude#amplitude', h14_phase_setting_init)
        japc.setParam('PA.GSRPB/Amplitude#amplitude', h21_phase_setting_init)
        
        episode += 1
        print(f"Tested both agents on setting {settings}.")
    ###############################################
    
    ########## Triple split optimisation ##########


    # splitting_class = SplittingAgentBase(user=user, 
    #                                     splitting='tri', 
    #                                     tomoscope=tomoscope, 
    #                                     beam_type=beam_type)

    # splitting_class.start_automatic_optimisation_tri(initial_feat_guess=True, 
    #                                     max_steps=30, 
    #                                     render_opt=True,
    #                                     phase_criterion=0.0008,
    #                                     volt_criterion = 0.0005,
    #                                     )#save_dir = '/eos/home-j/jwulff/workspaces/RL_splitting_adjustment_scripts/local_saved_episodes')
    
    ###############################################
    
    ########## Quad split optimisation ############

    # splitting_class = SplittingAgentBase(user=user, 
    #                                     splitting='quad', 
    #                                     tomoscope=tomoscope, 
    #                                     beam_type=beam_type)

    # splitting_class.start_automatic_optimisation_quad(max_steps=30, 
    #                                     render_opt=True,
    #                                     phase_42_criterion=0.0008,
    #                                     phase_84_criterion = 0.0008,
    #                                     save_dir = '/eos/home-j/jwulff/workspaces/RL_splitting_adjustment_scripts/local_saved_episodes')

print('Script finished.')
###############################################
    
    ########## Tri split optimisation ############

    # splitting_class = SplittingAgentBase(user=user, 
    #                                     splitting='tri', 
    #                                     tomoscope=tomoscope, 
    #                                     beam_type=beam_type)

    # splitting_class.start_automatic_optimisation_tri_all_params_agent(
    #                                     max_steps=30, 
    #                                     render_opt=True,
    #                                     phase_volt_criterion=0.0008,
    #                                     save_dir = '/eos/home-j/jwulff/workspaces/RL_splitting_adjustment_scripts/local_saved_episodes/testing_all_params_agent')