import pyjapc
import matplotlib.pyplot as plt
import numpy as np
import sys
sys.path.append(r'C:\Users\jwulff\cernbox\workspaces\RL-007-RFinPS\optimising-rf-manipulations-in-the-ps-through-rl')
import torch
import torch.nn as nn
import torch.nn.functional as F
#from pytorch_fitmodule import FitModule
from torch.autograd import Variable
import numpy as np
from neural_networks.dataloader import QuadsplitDataset, ToTensor, Normalize
from torchvision import transforms, utils
from torch.utils.data import Dataset, DataLoader
from neural_networks.CNN import CNN
from neural_networks.CNN_tri import CNN_tri
from neural_networks.CNN_tri_new import CNN_tri_new
import torch.optim as optim
import matplotlib.pyplot as plt
import cv2
from vpc_functions import load_model, isolate_bunches_from_dm_profile, loss_function_two, process_tomoscope_acquisition, render_quad, tri_phase_loss#, correct_dm_tri #, FWHM
from vpc_ajdust_phases import offset_GSRPB, offset_GSRPC, update_voltage_program_GSVMOD7, offset_C20, offset_C40
import time
from stable_baselines3 import SAC
import os


japc = pyjapc.PyJapc(selector='CPS.USER.MD7')

#print(japc.getUsers('CPS'))
devices = []
#devices.append('PR.BCT/Acquisition')  # DC BCT
#devices.append('PR.SCOPE58.CH01/Acquisition')  # Tomoscope 57-58
devices.append('PR.SCOPE57.CH01/Acquisition')  # Tomoscope 57-58
# devices.append('PR.SCOPE97.CH01/Acquisition')  # WCM03 OASIS
#devices.append('PR.SCOPE59.CH01/Acquisition')  # BSM 59-87
# devices.append('PR.SCOPE60.CH01/Acquisition')  # Last turn logging 60
# devices.append('PR.BQL72/ContinuousAcquisition')  # BBQ
# devices.append('PR.SCOPE31.CH01/Acquisition')  # Wideband transverse
# devices.append('PR.SCOPE31.CH02/Acquisition')  # Wideband transverse
# devices.append('PR.SCOPE31.CH03/Acquisition')  # Wideband transverse
# devices+=['F16.BCT126/Acquisition',
#           'F16.BCT203/Acquisition',
#           'F16.BCT212/Acquisition',
#           'F16.BCT372/Acquisition']  # TT2 BCTs
# devices+=['PA.VD20-80-SA/Samples',
#           'PA.VD20-92-SA/Samples',
#           'PA.VD40-77-SA/Samples',
#           'PA.VD40-78-SA/Samples',
#           'PA.VD80-08-SA/Samples',
#           'PA.VD80-88-SA/Samples',
#           'PA.VD80-89-SA/Samples']  # Samplers
# devices+=['PA.VDC11-SA/Samples',
#           'PA.VDC36-SA/Samples',
#           'PA.VDC46-SA/Samples',
#           'PA.VDC51-SA/Samples',
#           'PA.VDC56-SA/Samples',
#           'PA.VDC66-SA/Samples',
#           'PA.VDC76-SA/Samples',
#           'PA.VDC81-SA/Samples',
#           'PA.VDC86-SA/Samples',
#           'PA.VDC81-SA/Samples',
#           'PA.VDC96-SA/Samples']  # Samplers

##########
# Load REFERENCE VOLTAGE USED IN SIMULATION
##########
# loaded_h14 = np.load('./input_files/LHC25#72b_22/C10_h-14-reference.npz')
# time_h14 = loaded_h14['time']*1e-3                          # Time [s]
# voltage_h14 = loaded_h14['amplitude']*1e3 * 0.989    # Voltage [V]

# ref_vf = 0.989
# ref_voltage_h14 = np.load('./input_files/LHC25#72b_22/ref_voltage_h14.npy')*ref_vf / 200 # Reference voltage values for the six points important in the LSA settings.


# time_sim_start = 1830e-3
# time_sim_end = 1891e-3
# voltage_h14 = voltage_h14[(time_h14>=time_sim_start)*(time_h14<=time_sim_end)]
# time_h14 = time_h14[(time_h14>=time_sim_start)*(time_h14<=time_sim_end)]

# plt.figure()
# plt.plot(time_h14,voltage_h14)
# plt.show()
###############################################################
# LOAD FEATURE EXTRACTOR(s)
###############################################################

model = CNN() # NEW move_injection version (35k dataset)
#model2 = CNN_tri() # New MoveInj version (59k dataset)
#model3 = CNN_tri_new(trim_edges=True) # Only predicting phases, 1681 samples

#optimizer = optim.Adam(model.parameters(), lr=1e-3)

feat_model_name = 'cnn_quad_ms_ADD_NOISE_Move_injectione_46_FINETUNE_LIVEMay_05_2022_15-41-33_e11'# 

print(f'Fetching model {feat_model_name}...')
path = r"/eos/home-j/jwulff/workspaces/RL-007-RFinPS/optimising-rf-manipulations-in-the-ps-through-rl/saved_models/{}.pth".format(feat_model_name)
#path = r"C:\Users\jwulff\cernbox\workspaces\RL-007-RFinPS\optimising-rf-manipulations-in-the-ps-through-rl\saved_models\{}.pth".format(model_name)
model = load_model(path,model)
#optimizer.load_state_dict(checkpoint['optimizer_state_dict'])
# print(f'Weights loaded...')
# print(f'Fetching model {feat_model_name2}...')
# path = r"/eos/home-j/jwulff/workspaces/RL-007-RFinPS/optimising-rf-manipulations-in-the-ps-through-rl/saved_models/{}.pth".format(feat_model_name2)
# #path = r"C:\Users\jwulff\cernbox\workspaces\RL-007-RFinPS\optimising-rf-manipulations-in-the-ps-through-rl\saved_models\{}.pth".format(model_name)

# model2 = load_model(path, model2)
# print(f'Weights loaded...')

# print(f'Fetching model {feat_model_name3}...')
# path = r"/eos/home-j/jwulff/workspaces/RL-007-RFinPS/optimising-rf-manipulations-in-the-ps-through-rl/saved_models/{}.pth".format(feat_model_name3)
# model3 = load_model(path,model3)

model.eval() ### IMPORTANT! If not in eval mode, batchnorm layers will not work correctly.

# preds=[]
# true=[]
# results = torch.empty(0)
# for i, data in enumerate(val_dataloader, 0):
#     with torch.no_grad():
#         inputs, labels = data['image'].float(), data['labels'].float()
#         outputs = model(inputs)
#         loss = criterion(outputs,labels)
#         preds.append(outputs)
#         true.append(labels)
#         preds_labels = torch.cat((outputs, labels), 1)
#         results = torch.cat((results, preds_labels),0)

#bsm_dictionary = japc.getParam(devices[1])
#init episode and step counter
episode = 11
step = 0
step_limit = 1 # Minimum number of steps needed. =1 means no action, as step 0 no action is taken.
phase_set_memory = []
action_memory = []
phase_set_memory = [] # Doubles as labels for dm:s in dm_memory. May need to convert them to resemble labels in original dataset.
dm_memory = []
bls_memory = []
criterion_memory = []
profile_criterion_memory_42 = []
profile_criterion_memory_84 = []

profile_memory = []
corrected_profile_memory = []
feat_extr_memory = []
feat_extr_no_move_memory =[]
model3_memory = []
model4_memory = []

# Load agent
model_name_42 = r"SAC-Simple-Profile-relBLInt42-step20-diff-00003"
agent_42 = SAC.load("./RL_logs/quad/{}".format(model_name_42))

# SAC-Simple-Profile-dist8-relBLInt84-step20-diff-00003
# SAC-Simple-Profile-dist8-relBLInt84-vary42-step20-diff-00003
model_name_84 = r"SAC-Simple-Profile-dist8-relBLInt84-step20-diff-00003"
agent_84 = SAC.load("./RL_logs/quad/{}".format(model_name_84))


# Init settings

# For now it is required to initially optimize settings manually before starting scan. Periodically one may need to check that the optimum setting has not drifted!
dpc20_setting_init = japc.getParam('PA.DPC20/Setting#value')
dpc40_setting_init = japc.getParam('PA.DPC40/Setting#value')

new_dpc20 = 0
new_dpc40 = 0

def checkIfNewDataReceivedCallback(parameterName, newValue):
    global new_values_received
    new_values_received=True
    print(f"New value for {parameterName}")

japc.subscribeParam(devices[0], checkIfNewDataReceivedCallback)
japc.startSubscriptions()


SAVE_DIR = r"/eos/home-j/jwulff/workspaces/RL-007-RFinPS/optimising-rf-manipulations-in-the-ps-through-rl/MD_results_1_08_2022/{}/{}/".format('quad',model_name_84)

# Create saving directory
if not os.path.exists(SAVE_DIR):
    os.makedirs(SAVE_DIR)

# Init settings

# For now it is required to initially optimize settings manually before starting scan. Periodically one may need to check that the optimum setting has not drifted!

### OPTIMIZE MANUALLY TO FIND 0,0 OFFSET

# C20
DPC_20_INIT = japc.getParam('PA.DPC20/Setting#value')

# C40
DPC_40_77_INIT = japc.getParam('PA.MHFBC40-77/MhsChannelCTRLPPM#phaseCycle')
DPC_40_78_INIT = japc.getParam('PA.MHFBC40-78/MhsChannelCTRLPPM#phaseCycle')


START_OFFSET = [-5, 5]
dpc20_offset = START_OFFSET[0] # propagated to dpc40
dpc40_offset = START_OFFSET[1] # 
offset_C20(japc, phase_offset=round(dpc20_offset,4))
offset_C40(japc, phase_offset=round(dpc40_offset,4))
phase_set = START_OFFSET
phase_set_memory.append(phase_set)
skipped_settings = []
step = 0

new_h42 = START_OFFSET[0]
new_h84 = START_OFFSET[1]

# BEFORE BEGINNING EPISODE, move to desired starting offset
p42_done = False
p84_done = False

current_offset = START_OFFSET

rel_phase_set = [[0,0]]
for step in range(15):
    print(f"Beginning step {step}...")

    h42_setting = new_h42 #japc.getParam('PA.DPC20/Setting#value')
    h84_setting = new_h84 #japc.getParam('PA.DPC40/Setting#value')

    # Wait for next acquisition
    # Save acquisition with phase setting
    #print('Set p14,p21,v14 now to what you want and store in p14,p21,v14 variables!')
    #print(f'Current settings: p14: {p14}, p21 {p21}, v14 {v14}.')

    new_values_received = False
    # Wait for next acquisition

    while not new_values_received:
        time.sleep(1)
    # Extract values of acquisition
    data_dictionary = japc.getParam(devices[0])
    values = data_dictionary['value'] # Matrix if using tomoscope

    values_centered_norm = process_tomoscope_acquisition(values, mode='quad')
    profile = values_centered_norm[-2,:]
    profile_42 = values_centered_norm[99,:]

    profile_memory.append([profile])
    #corr = correct_profile_tri(profile)
    #################
    # CALCULATE LOSS
    #################
    bunches=[]
    bunches, fwhms, intensities = isolate_bunches_from_dm_profile(profile, intensities=True, bunch_width=8, rel=True, plot_found_bunches=False, distance=20)
 

    ### Process for 84 agent
    # Four bunches found, take average of 1,3 and 2,4 respectively
    bunches = [(bunches[0] + bunches[2])/2, (bunches[1]+bunches[3])/2]
    fwhms = [(fwhms[0] + fwhms[2])/2, (fwhms[1]+fwhms[3])/2]
    intensities = [(intensities[0] + intensities[2])/2, (intensities[1]+intensities[3])/2]
    fwhms = fwhms -np.mean(fwhms)
    intensities = intensities / max(intensities) #
    intensities = intensities - np.mean(intensities)
    bls_and_intensities = np.append(fwhms, intensities)

    ### Process for 42 agent
    bunches_42, fwhms_42, intensities_42 = isolate_bunches_from_dm_profile(profile_42, intensities=True, rel=True, plot_found_bunches=False)
    fwhms_42 = fwhms_42 -np.mean(fwhms_42)
    # How to normalize intensities to be in range [0,1]? For now, divide by constant.
    intensities_42 = intensities_42 / max(intensities_42) # 
    intensities_42 = intensities_42 - np.mean(intensities_42)

    bls_and_intensities_42 = np.append(fwhms_42, intensities_42)

    fwhms = fwhms - np.mean(fwhms)



    b1 = bunches[0]
    b2 = bunches[1]
    #loss = loss_function_tri(b1,b2,b3)
    loss_84 = loss_function_two(b1, b2)

    loss_42 = loss_function_two(bunches_42[0], bunches_42[1])
    profile_criterion_memory_84.append(loss_84)
    profile_criterion_memory_42.append(loss_42)


    #values = cv2.resize(values, dsize=(400,150))
    np.save('/eos/home-j/jwulff/workspaces/RL-007-RFinPS/optimising-rf-manipulations-in-the-ps-through-rl/dataset/quad/new_live_data/p42_{}_p84_{}_datamatrix'.format(int(dpc20_offset*100), int(dpc40_offset*100)), values_centered_norm)

    sample = {'image': values_centered_norm, 'labels': np.ndarray([0])}

    transform = transforms.Compose([
        #Normalize(),
        ToTensor(),
    ])



    inputs_dict = transform(sample) # Convert to tensor


    image = inputs_dict['image'].unsqueeze(dim=0)
    transformed_profile = image[0,0,-1,:].numpy()


    criterion = 0.0006

    # print(f'Current loss: {loss}')
    # plt.figure()
    # plt.plot(b1, label='b1')
    # plt.plot(b2, label='b2')
    # plt.plot(b3, label='b3')
    # plt.figure()
    # plt.title(f'Loss: {loss}')
    # plt.plot(transformed_profile)
    # plt.figure()
    # plt.imshow(values_centered_norm, aspect='auto')
    # plt.show()
    # plt.figure()
    # plt.title("Loss")
    # plt.plot(profile_criterion_memory, 'o-')
    # plt.plot(np.linspace(0,len(profile_criterion_memory),len(profile_criterion_memory)+1),np.ones(len(profile_criterion_memory)+1)*criterion, 'k--', label='Stop Criterion')
    # plt.show()
    plt.figure('Optimisation')
    render_quad((-20,20), (-20,20), rel_phase_set, profile, loss_42, profile_criterion_memory_42, profile_criterion_memory_84, values_centered_norm, bunches, fwhms, intensities, criterion=criterion, step=step)

    outputs = model(image).detach().numpy()/100



    print(f'True offset: {[dpc20_offset, dpc40_offset]}')
    print(f' Feat extractor finetuned quad: {outputs} (non-propagated!!)' )

    #Outputs are estimated offset from 0,0 sim setting in dpc20,dpc40
    #new_h14 = h14_phase_setting-outputs[0].astype(np.float32)
    #new_h21 = h21_phase_setting-outputs[1].astype(np.float32)
    # Send outputs of CNN and BLs to Agent to predict action

    # Construct observation: [bls, CNN_output] or only [bls]
    max_phase_setting = 30
    
    outputs = outputs[0]
    outputs[:2] = outputs[:2]/max_phase_setting
    # outputs2[:2] = outputs2[:2]/max_phase_setting
    # outputs2[2] = (outputs2[2]-1)/(max_volt-1)


    
    #outputs = np.array([0.01,-0.50])

    obs_42 = bls_and_intensities_42
    obs_84 = bls_and_intensities
    # If loss is below criterion, end episode
    
    criterion_84 = 0.0008
    criterion = 0.0006
    if loss_42 < criterion and step > step_limit and not p42_done:
        print(f'Phase 42 criterion reached in {step} steps.')
        plt.figure('p42 Optimisation')
        render_quad((-20,20), (-20,20), rel_phase_set, profile_42, loss_42, profile_criterion_memory_42, profile_criterion_memory_84,
                values_centered_norm, 
                bunches,
                fwhms, 
                intensities, 
                criterion=criterion, 
                done=True, 
                step=step)
        dm_memory.append(values_centered_norm)
        plt.savefig('{}Ep{}_42optimisation.png'.format(SAVE_DIR, episode))
        p42_done=True
    if loss_84 < criterion_84 and step > step_limit and not p84_done:
        plt.figure('p84 Optimisation')
        print(f'Phase 84 criterion reached in {step} steps.')
        render_quad((-20,20), (-20,20), rel_phase_set, profile, loss_42, profile_criterion_memory_42, profile_criterion_memory_84, 
                values_centered_norm, 
                bunches,
                fwhms, 
                intensities, 
                criterion=criterion, 
                done=True, 
                step=step)
        dm_memory.append(values_centered_norm)
        plt.savefig('{}Ep{}_84optimisation.png'.format(SAVE_DIR, episode))
        p84_done=True
    
    if p42_done and p84_done and step > 5:
        print(f'Optimisation finished. Took {step} steps.')
        break

    max_step_size = 20

    # Predict
    converted_action_42 = 0.0
    converted_action_84 = 0.0
    if not p42_done:
        action_42, _states = agent_42.predict(obs_42, deterministic=True)
        converted_action_42 = action_42*max_step_size

    if not p84_done:
        action, _ = agent_84.predict(obs_84, deterministic=True)
        converted_action_84 = action*max_step_size

    if step == 0:
        dm_memory.append(values_centered_norm)
        print(f'Step 0, no action taken')
        action_memory.append([0,0])
        continue
    
    # Take step
    # if initial_feat_guess:
    #     converted_action = -initial_guess
    #     print(f'Taking initial guess from feat extractor: Moving {converted_action}')
    #     initial_feat_guess=False
    # Track correction
    corr = np.append(converted_action_42,converted_action_84)
    dpc20_offset += converted_action_42
    dpc40_offset += converted_action_84

    rel_phase_set.append(rel_phase_set[-1]+corr)

    new_h42 = h42_setting+converted_action_42#.astype(np.float32)
    new_h84 = h84_setting+converted_action_84#.astype(np.float32)

    # Calculate manual input for splitting adjustment.
    h42_input = converted_action_42
    h84_input = converted_action_84

    print(f'Current phase set: {h42_setting}, {h84_setting}')
    print(f'Taking Action: {corr})')
    print(f'Profile-based loss42: {loss_42}, loss84: {loss_84}')
    offset_C20(japc, phase_offset=h42_input)
    offset_C40(japc, phase_offset=h84_input)

    #voltage_change = converted_action[2]#(v14_input/0.01)*voltage_max_step
    #update_voltage_program_GSVMOD7(japc, voltage_change)
    
    new_values_received = False
    # Set phases
    #print(f"Taking action {action}, new settings: dpc20 {new_dpc20}, dpc40 {new_dpc40}")
    #japc.setParam('PA.DPC20/Setting#value', new_dpc20)
    #japc.setParam('PA.DPC40/Setting#value', new_dpc40)
    action_memory.append([corr])
    dm_memory.append(values_centered_norm)
    
# Check params have been set
#assert(japc.getParam('PA.DPC20/Setting#value')==new_dpc20)
#assert(japc.getParam('PA.DPC40/Setting#value')==new_dpc40)

# Voltage factor 1.0 means v14 flat top at 16422.83058167






# Reset
japc.setParam('PA.DPC20/Setting#value', DPC_20_INIT)
japc.setParam('PA.MHFBC40-77/MhsChannelCTRLPPM#phaseCycle', DPC_40_77_INIT)
japc.setParam('PA.MHFBC40-78/MhsChannelCTRLPPM#phaseCycle', DPC_40_78_INIT)
# Saving metadata


np.save('{}Ep{}_phase_set_memory'.format(SAVE_DIR, episode), phase_set_memory)
#np.save('{}Ep{}_action_memory'.format(SAVE_DIR, episode), action_memory)
np.save('{}Ep{}_dm_memory'.format(SAVE_DIR, episode), dm_memory)
np.save('{}Ep{}_profile_memory'.format(SAVE_DIR, episode), profile_memory)
np.save('{}Ep{}_profile_criterion_memory_42'.format(SAVE_DIR, episode), profile_criterion_memory_42)
np.save('{}Ep{}_profile_criterion_memory_84'.format(SAVE_DIR, episode), profile_criterion_memory_84)

np.save('{}Ep{}_rel_phase_volt_set'.format(SAVE_DIR, episode), rel_phase_set)


#Plot some of the data
plt.figure(dpi=200)
plt.title('Profile based criterion 42')
plt.plot(profile_criterion_memory_42,'bo-')
plt.plot(np.linspace(0,len(profile_criterion_memory_42),len(profile_criterion_memory_42)+1),np.ones(len(profile_criterion_memory_42)+1)*criterion, 'k--', label='Stop Criterion')

plt.figure(dpi=200)
plt.title('Profile based criterion 84')
plt.plot(profile_criterion_memory_84,'bo-')
plt.plot(np.linspace(0,len(profile_criterion_memory_84),len(profile_criterion_memory_84)+1),np.ones(len(profile_criterion_memory_84)+1)*criterion, 'k--', label='Stop Criterion')

plt.xlabel('Step')
plt.ylabel('Agent criterion [arb. units]')
plt.savefig('{}Ep{}_profile_based_criterion.png'.format(SAVE_DIR, episode))
plt.figure(dpi=250)
plt.title('First five steps')
plt.subplot(251)
plt.imshow(dm_memory[1])
plt.subplot(252)
plt.imshow(dm_memory[2])
plt.subplot(253)
plt.imshow(dm_memory[3])
plt.subplot(254)
plt.imshow(dm_memory[4])
plt.subplot(255)
plt.imshow(dm_memory[5])
plt.subplot(256)
plt.plot(dm_memory[1][-1,:])
plt.subplot(257)
plt.plot(dm_memory[2][-1,:])
plt.subplot(258)
plt.plot(dm_memory[3][-1,:])
plt.subplot(259)
plt.plot(dm_memory[4][-1,:])
plt.subplot(2,5,10)
plt.plot(dm_memory[5][-1,:])
plt.show()
plt.savefig('{}Ep{}_first_five_steps.png'.format(SAVE_DIR, episode))

#Plot some of the data

plt.figure(dpi=200)
plt.title('Initial and final state')
plt.subplot(221)
plt.imshow(dm_memory[0])
plt.subplot(222)
plt.imshow(dm_memory[-1])
plt.subplot(223)
plt.plot(dm_memory[0][-1,:])
plt.subplot(224)
plt.plot(dm_memory[-1][-1,:])
plt.show()
plt.savefig('{}Ep{}_init_final_state.png'.format(SAVE_DIR, episode))