import sys
from matplotlib.style import use
from pathlib import Path
mod_path = Path(__file__).parent.parent # Go up two steps to base of git repo.
sys.path.append(mod_path)
import torch
import torch.nn as nn
import torch.nn.functional as F
#from pytorch_fitmodule import FitModule
from torch.autograd import Variable
import numpy as np
from dataloader import QuadsplitDataset, ToTensor, Normalize
from torchvision import transforms, utils
from torch.utils.data import Dataset, DataLoader
from CNN import CNN
from CNN_w_tanh import CNN_tanh
from CNN_w_tanh_maxpool import CNN_tanh_maxpool
#from CNN_tri import CNN_tri
from CNN_tri_new import CNN_tri_new
from MLP_Profile_feat_extractor import MLPProfile
import torch.optim as optim
from utils import count_parameters
import matplotlib.pyplot as plt
import os
import json

# hparams = {
#     'model_name': 'cnn_quad_Noise_tanh',
#     'data_csv_path': r"C:\Users\jwulff\cernbox\workspaces\RL-007-RFinPS\optimising-rf-manipulations-in-the-ps-through-rl\dataset\quad\machine_settings_fixed\dataset.csv",
#     'batch_size': 20,
#     'log_dir': r"C:\Users\jwulff\cernbox\workspaces\RL-007-RFinPS\optimising-rf-manipulations-in-the-ps-through-rl\tensorboard_logs",
#     'model': 'cnn',
#     'rf_manip': 'quad',
#     'start_epoch': 0,
#     'end_epoch': 50,
#     'optimizer': 'adam',
#     'add_noise': True,
#     'move_injection': False,
#     'opt_lr': 1e-3,
#     'adam_b1': 0.9,
#     'adam_b2': 0.999,
#     'resize_data': False,
#     'use_live_data': False,
#     'load_model': False,
#     'load_model_path': r"C:\Users\jwulff\cernbox\workspaces\RL-007-RFinPS\optimising-rf-manipulations-in-the-ps-through-rl\saved_models\cnn_quad_ms_ADD_NOISEApr_22_2022_08-37-14_e27.pth",
#     'test_logging': False,
#     'tanh': True,

# }
# cnn_tri_Trim_Move_Injection_PV_59521_ref_voltageJul_11_2022_09-22-53_e27
# cnn_tri_Move_Injection4_PV_59521_ref_voltageJun_23_2022_13-23-23_e41
model_name = 'cnn_tri_Trim_Move_Injection_PV_59521_ref_voltageJul_11_2022_09-22-53_e27'
split = model_name.split('_')
hparams_meta = '_'.join(split[:-1])
epoch = split[-1]
train_metadata_path = 'neural_networks/training_metadata'
with open(train_metadata_path + '/' + hparams_meta + '.json') as json_file:
    hparams = json.load(json_file)
problem = 'tri'
use_last_profile = False
batch_size = 20
if hparams['input_type'] == 'profile':
    use_last_profile = True
dataset = QuadsplitDataset(resize = hparams['resize_data'],
                            noise = hparams['add_noise'],
                            csv_file = hparams['data_csv_path'],
                            live_data = hparams['use_live_data'],
                            move_injection = hparams['move_injection'],
                            norm_labels = hparams['tanh'],
                            use_last_profile=use_last_profile,
                            trim_edges=hparams['trim_edges']
                            
    )
length = dataset.__len__()
#small = int(length*0.1)
#big = length - small
#small_dataset, _ = torch.utils.data.random_split(dataset, [small, big], generator=torch.Generator().manual_seed(42))
#length = small_dataset.__len__()
train_len = int(length * 0.9)
 
val_len = length - train_len
train, val = torch.utils.data.random_split(dataset, [train_len, val_len], generator=torch.Generator().manual_seed(42))
train_dataloader = DataLoader(train, batch_size=batch_size, shuffle=True, num_workers=0)
val_dataloader = DataLoader(val, batch_size=batch_size, shuffle=True, num_workers=0)

# # Look at some examples of data to check injection move
# for data in val_dataloader:
#     with torch.no_grad():
#         inputs, labels = data['image'].float(), data['labels'].float()

#         plt.figure()
#         plt.imshow(inputs[0,0,:,:])
# Initialize model
if hparams['tanh']:
    if hparams['model'] == 'cnn':
        model = CNN_tanh()
        nbr_params = count_parameters(model)
    elif hparams['model'] == 'cnn_maxpool':
        model = CNN_tanh_maxpool()
        nbr_params = count_parameters(model)
    elif hparams['model'] == 'mlpProfile':
        model = MLPProfile()
        nbr_params = count_parameters(model)
    # elif hparams['model'] == 'cnn_tri':
    #     model = CNN_tri()
    #     nbr_params = count_parameters(model)
else:
    if hparams['model'] == 'cnn':
        model = CNN()
        nbr_params = count_parameters(model)
    elif hparams['model'] == 'cnn_tri':
        model = CNN_tri_new(num_features=3, trim_edges=True)
        nbr_params = count_parameters(model)
    

optimizer = optim.Adam(model.parameters(), lr=1e-3)

print(f'Fetching model {model_name}...')
#path = r"C:\Users\jwulff\cernbox\workspaces\RL-007-RFinPS\optimising-rf-manipulations-in-the-ps-through-rl\saved_models\{}.pth".format(model_name)
path = "saved_models/{}.pth".format(model_name) # Relative path works due to adding parent dir to sys path.

checkpoint = torch.load(path)
model.load_state_dict(checkpoint['model_state_dict'])
optimizer.load_state_dict(checkpoint['optimizer_state_dict'])
criterion = checkpoint['loss']
print(f'Weights loaded...')

model.eval()
preds=[]
true=[]
results = torch.empty(0)
for i, data in enumerate(val_dataloader, 0):
    with torch.no_grad():
        inputs, labels = data['image'].float(), data['labels'].float()
        outputs = model(inputs).squeeze()
        loss = criterion(outputs,labels)
        preds.append(outputs)
        true.append(labels)
        preds_labels = torch.cat((outputs, labels), 1)
        results = torch.cat((results, preds_labels),0)
        #if i % 3:
        #    break
        #eval_loss += loss.item()
        #avg_loss += loss.item()
        #print("Preds {}, True {}, Loss {}".format(outputs/100,labels/100,loss))
        #print("[Preds, True, {}]".format(preds_labels/100))

# Make some plots
print(results)
# Create save dir
if hparams['tanh']:
    results = results*3000 # Convert to degrees*100 like other labels
if not os.path.exists('plots/{}'.format(model_name)):
    os.makedirs('plots/{}'.format(model_name))

if problem=='quad':
    p42_out_array = results[:,0]/100
    p42_in_array = results[:,2]/100

    p84_out_array = results[:,1]/100
    p84_in_array = results[:,3]/100

    plt.figure()
    x=np.linspace(-25,25,100)
    plt.plot(x,x,'g')
    plt.plot(p42_in_array, p42_out_array, '.')
    plt.legend(['Reference', 'in vs out'])
    plt.xlabel('p42 phase input')
    plt.ylabel('p42 phase output')
    plt.savefig('plots/{}/p42_{}.png'.format(model_name, epoch))

    plt.figure()
    plt.plot(x,x,'g')
    plt.plot(p84_in_array, p84_out_array, '.')
    plt.legend(['Reference', 'in vs out'])
    plt.xlabel('p84 phase input')
    plt.ylabel('p84 phase output')
    plt.savefig('plots/{}/p84_{}.png'.format(model_name, epoch))

    plt.figure()
    zeros = np.zeros(len(x))
    x=np.linspace(-25,25,100)
    plt.plot(x,zeros,'g')
    plt.plot(p42_in_array, p42_out_array-p42_in_array, '.')
    plt.legend(['Reference', 'in vs out'])
    plt.xlabel('p42 phase input')
    plt.ylabel('p42 output - p42 input')
    plt.savefig('plots/{}/p42err_{}.png'.format(model_name, epoch))

    plt.figure()

    plt.plot(x,zeros,'g')
    plt.plot(p84_in_array, p84_out_array-p84_in_array, '.')
    plt.legend(['Reference', 'in vs out'])
    plt.xlabel('p84 phase input')
    plt.ylabel('p84 output - p84 input')
    plt.savefig('plots/{}/p84err_{}.png'.format(model_name, epoch))
    plt.show()

    # Save plots
if problem=='tri':
    p14_out_array = results[:,0]/100
    p14_in_array = results[:,3]/100

    p21_out_array = results[:,1]/100
    p21_in_array = results[:,4]/100

    v14_out_array = results[:,2]/1000
    v14_in_array = results[:,5]/1000

    plt.figure()
    x=np.linspace(-15,15,100)
    plt.plot(x,x,'g')
    plt.plot(p14_in_array, p14_out_array, '.')
    plt.legend(['Reference', 'in vs out'])
    plt.xlabel('p14 phase input')
    plt.ylabel('p14 phase output')
    plt.savefig('plots/{}_p14.png'.format(model_name))

    plt.figure()
    plt.plot(x,x,'g')
    plt.plot(p21_in_array, p21_out_array, '.')
    plt.legend(['Reference', 'in vs out'])
    plt.xlabel('p21 phase input')
    plt.ylabel('p21 phase output')
    plt.savefig('plots/{}_p21.png'.format(model_name))

    plt.figure()
    x_v14 = np.linspace(0.85,1.15,100)
    plt.plot(x_v14,x_v14,'g')
    plt.plot(v14_in_array, v14_out_array, '.')
    plt.legend(['Reference', 'in vs out'])
    plt.xlabel('v14 input')
    plt.ylabel('v14 output')
    plt.savefig('plots/{}_v14.png'.format(model_name))

    plt.figure()
    zeros = np.zeros(len(x))
    x=np.linspace(-15,15,100)
    plt.plot(x,zeros,'g')
    plt.plot(p14_in_array, p14_out_array-p14_in_array, '.')
    plt.legend(['Reference', 'in vs out'])
    plt.xlabel('p14 phase input')
    plt.ylabel('p14 output - p14 input')
    plt.savefig('plots/{}_p14err.png'.format(model_name))

    plt.figure()

    plt.plot(x,zeros,'g')
    plt.plot(p21_in_array, p21_out_array-p21_in_array, '.')
    plt.legend(['Reference', 'in vs out'])
    plt.xlabel('p21 phase input')
    plt.ylabel('p21 output - p21 input')
    plt.savefig('plots/{}_p21err.png'.format(model_name))

    plt.figure()

    plt.plot(x_v14,zeros,'g')
    plt.plot(v14_in_array, v14_out_array-v14_in_array, '.')
    plt.legend(['Reference', 'in vs out'])
    plt.xlabel('v14 phase input')
    plt.ylabel('v14 output - v14 input')
    plt.savefig('plots/{}_v14err.png'.format(model_name))
    plt.show()





print("Inference complete.")