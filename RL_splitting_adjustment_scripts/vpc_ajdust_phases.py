
# Imports
import numpy as np
from pyjapc import PyJapc

# japc instanciation
#japc = PyJapc()
#japc.setSelector("CPS.USER.LHC2")

# References and cavity names
cav_name_C40 = ['C40-77', 'C40-78']
cav_name_C80 = ['C80-08', 'C80-88', 'C80-89']

# Functions to adjust phase

def offset_C80(japc, phase_offset, failsafe=True):

    for cav_name in cav_name_C80:
        present_phase = japc.getParam(
            'PA.MHFB%s/MhsChannelCTRLPPM#phaseCycle' % (cav_name))
        new_phase = present_phase + phase_offset
        new_phase = new_phase % 360
        japc.setParam(
            'PA.MHFB%s/MhsChannelCTRLPPM#phaseCycle' %
            (cav_name), new_phase)

        if failsafe:
            check_phase = japc.getParam(
                'PA.MHFB%s/MhsChannelCTRLPPM#phaseCycle' %
                (cav_name))
            if check_phase == new_phase:
                print('%s' % (cav_name), ': %.2f - OK' % (new_phase))
            else:
                print('%s' % (cav_name), ': FAIL')


def offset_C40(japc, phase_offset, link_phase=True, failsafe=True):

    for cav_name in cav_name_C40:
        present_phase = japc.getParam(
            'PA.MHFB%s/MhsChannelCTRLPPM#phaseCycle' % (cav_name))
        new_phase = present_phase + phase_offset
        new_phase = new_phase % 360
        japc.setParam(
            'PA.MHFB%s/MhsChannelCTRLPPM#phaseCycle' %
            (cav_name), new_phase)

        if failsafe:
            check_phase = japc.getParam(
                'PA.MHFB%s/MhsChannelCTRLPPM#phaseCycle' %
                (cav_name))
            if check_phase == new_phase:
                print('%s' % (cav_name), ': %.2f - OK' % (new_phase))
            else:
                print('%s' % (cav_name), ': FAIL')

    if link_phase:
        offset_C80(japc, 2 * phase_offset, failsafe=failsafe)


def offset_C20(japc, phase_offset, link_phase=True, failsafe=True):

    present_phase = japc.getParam('PA.DPC20/Setting#value')
    new_phase = present_phase + phase_offset
    new_phase = new_phase % 360
    japc.setParam('PA.DPC20/Setting#value', new_phase)

    if failsafe:
        check_phase = japc.getParam('PA.DPC20/Setting#value')
        if np.isclose(check_phase, new_phase, atol=1e-1):
            print('C20', ': %.2f - OK' % (new_phase))
        else:
            print('C20', ': FAIL')

    if link_phase:
        # NB: C80 already included in C40
        offset_C40(japc, 2 * phase_offset, failsafe=failsafe)


def offset_GSRPC(japc, phase_offset, failsafe=True):
    present_phase = japc.getParam('PA.GSRPC/Amplitude#amplitude')
    
    new_phase = present_phase + phase_offset # % 180
    if new_phase > 180:
        new_phase = -180 + (new_phase % 180)
    elif new_phase < -180:
        new_phase = 180 + (new_phase % -180)
    japc.setParam('PA.GSRPC/Amplitude#amplitude', new_phase)
    if failsafe:
        check_phase = japc.getParam('PA.GSRPC/Amplitude#amplitude')
        if np.isclose(check_phase, new_phase, atol=1e-1):
            print('GSRPC', ': %.2f - OK' % (new_phase))
        else:
            print('GSRPC', ': FAIL')


def offset_GSRPB(japc, phase_offset, failsafe=True):
    present_phase = japc.getParam('PA.GSRPB/Amplitude#amplitude')
    new_phase = present_phase + phase_offset# % 180
    if new_phase > 180:
        new_phase = -180 + (new_phase % 180)
    elif new_phase < -180:
        new_phase = 180 + (new_phase % -180)
    japc.setParam('PA.GSRPB/Amplitude#amplitude', new_phase)
    if failsafe:
        check_phase = japc.getParam('PA.GSRPB/Amplitude#amplitude')
        if np.isclose(check_phase, new_phase, atol=1e-1):
            print('GSRPB', ': %.2f - OK' % (new_phase))
        else:
            print('GSRPB', ': FAIL')

def offset_GSRPC_BCMS(japc, phase_offset, failsafe=True):
    present_phase_settings = japc.getParam('PA.GSRPC/Setting#amplitudes')
    present_phase = present_phase_settings[4][1,1]
    new_phase = present_phase + phase_offset
    if new_phase > 180:
        new_phase = -180 + (new_phase % 180)
    elif new_phase < -180:
        new_phase = 180 + (new_phase % -180)
    present_phase_settings[4][1,1] = new_phase
    present_phase_settings[5][1,0] = new_phase

    japc.setParam('PA.GSRPC/Setting#amplitudes', present_phase_settings, checkDims=False)
    if failsafe:
        check_phase = japc.getParam('PA.GSRPC/Setting#amplitudes')[4][1,1]
        if np.isclose(check_phase, new_phase, atol=1e-1):
            print('GSRPC', ': %.2f - OK' % (new_phase))
        else:
            print('GSRPC', ': FAIL')


def offset_GSRPB_BCMS(japc, phase_offset, failsafe=True):
    present_phase_settings = japc.getParam('PA.GSRPB/Setting#amplitudes')
    present_phase = present_phase_settings[3][1,1]
    new_phase = present_phase + phase_offset# % 180
    if new_phase > 180:
        new_phase = -180 + (new_phase % 180)
    elif new_phase < -180:
        new_phase = 180 + (new_phase % -180)
    present_phase_settings[3][1,1] = new_phase
    present_phase_settings[4][1,0] = new_phase
    japc.setParam('PA.GSRPB/Setting#amplitudes', present_phase_settings, checkDims=False)
    if failsafe:
        check_phase = japc.getParam('PA.GSRPB/Setting#amplitudes')[3][1,1]
        if np.isclose(check_phase, new_phase, atol=1e-1):
            print('GSRPB', ': %.2f - OK' % (new_phase))
        else:
            print('GSRPB', ': FAIL')

def update_voltage_program_GSVMOD7(japc, new_points, failsafe=True):
    V14_INIT = japc.getParam('PA.GSV10MOD7/Setting#amplitudes') # ??
    NEW_v14 = V14_INIT.copy()
    #set voltage to reference: 
    NEW_v14[3][1] = NEW_v14[3][1] + new_points
    japc.setParam('PA.GSV10MOD7/Setting#amplitudes', NEW_v14, checkDims=False)
    print('Updated voltage program.')
    if failsafe:
        pass #TODO

def update_voltage_program_GSVMOD7_BCMS(japc, new_points, failsafe=True):
    V14_INIT = japc.getParam('PA.GSV10MOD7/Setting#amplitudes') # ??
    NEW_v14 = V14_INIT.copy()
    #set voltage to reference: 
    NEW_v14[7][1] = NEW_v14[7][1] + new_points
    japc.setParam('PA.GSV10MOD7/Setting#amplitudes', NEW_v14, checkDims=False)
    print('Updated voltage program.')
    if failsafe:
        pass #TODO
### Triple splitting
#GSRPC (h=14)
# GSRPC_INIT = japc.getParam('PA.GSRPC/Amplitude#amplitude')

# # Voltage, GSV10MOD7
# V14_INIT = japc.getParam('PA.GSV10MOD7/Setting#amplitudes')

# #GSRPB (h=21)
# GSRPB_INIT = japc.getParam('PA.GSRPB/Amplitude#amplitude')



# ### Quadruple splitting
# # C20
# DPC_20_INIT = japc.getParam('PA.DPC20/Setting#value')

# # C40
# DPC_40_77_INIT = japc.getParam('PA.MHFBC40-77/MhsChannelCTRLPPM#phaseCycle')
# DPC_40_78_INIT = japc.getParam('PA.MHFBC40-78/MhsChannelCTRLPPM#phaseCycle')

# # C80
# DPC_80_08_INIT = japc.getParam('PA.MHFBC80-08/MhsChannelCTRLPPM#phaseCycle')
# DPC_80_88_INIT = japc.getParam('PA.MHFBC80-88/MhsChannelCTRLPPM#phaseCycle')
# DPC_80_89_INIT = japc.getParam('PA.MHFBC80-89/MhsChannelCTRLPPM#phaseCycle')

# # Moving phases
# offset_C40(japc, 0)